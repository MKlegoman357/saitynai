export interface IUserInfo {
    id: string;
    username: string;
}

export interface IUserInfoDto {
    id: string;
    username: string;
}

export function mapUserInfoDtoToUserInfo(userInfoDto: IUserInfoDto): IUserInfo;
export function mapUserInfoDtoToUserInfo(userInfoDto: null): null;
export function mapUserInfoDtoToUserInfo(userInfoDto: IUserInfoDto | null): IUserInfo | null;
export function mapUserInfoDtoToUserInfo(userInfoDto: IUserInfoDto | null): IUserInfo | null {
    if (!userInfoDto) {
        return null;
    }

    return {
        id: userInfoDto.id,
        username: userInfoDto.username
    };
}
