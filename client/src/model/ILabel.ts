export interface ILabel {
    name: string;
}

export interface ILabelDto {
    name: string;
}

export function mapLabelDtoToLabel(labelDto: null): null;
export function mapLabelDtoToLabel(labelDto: ILabelDto): ILabel;
export function mapLabelDtoToLabel(labelDto: ILabelDto | null): ILabel | null;
export function mapLabelDtoToLabel(labelDto: ILabelDto | null): ILabel | null {
    if (!labelDto) {
        return null;
    }

    return {
        name: labelDto.name
    };
}
