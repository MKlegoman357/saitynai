export type IResponse<T = undefined, TErrorData = undefined> = {
    ok: true;
    data: T;
} | {
    ok: false;
    errorCode: string;
    errorData: TErrorData;
};
