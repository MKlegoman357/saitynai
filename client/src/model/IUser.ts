import { Role } from '@/model/Role';

export interface IUser {
    id: string;
    email: string;
    username: string;
    role: Role;
}

export interface IUserDto {
    id: string;
    email: string;
    username: string;
    role: Role;
}

export function mapUserDtoToUser(userDto: IUserDto): IUser;
export function mapUserDtoToUser(userDto: null): null;
export function mapUserDtoToUser(userDto: IUserDto | null): IUser | null;
export function mapUserDtoToUser(userDto: IUserDto | null): IUser | null {
    if (!userDto) {
        return null;
    }

    return {
        id: userDto.id,
        email: userDto.email,
        username: userDto.username,
        role: userDto.role
    };
}
