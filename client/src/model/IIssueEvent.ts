import { IssueStatus } from '@/model/IIssue';
import { IUserInfo, IUserInfoDto, mapUserInfoDtoToUserInfo } from '@/model/IUserInfo';
import { ILabel, ILabelDto, mapLabelDtoToLabel } from '@/model/ILabel';

export enum IssueEventType {
    Title = 'title',
    Body = 'body',
    Status = 'status',
    AssignedUsers = 'assigned-users',
    Labels = 'labels'
}

export type IssueEventUnion = ITitleIssueEvent | IBodyIssueEvent | IStatusIssueEvent | IAssignedUsersIssueEvent | ILabelsIssueEvent;

export interface IIssueEvent<TEvent extends IssueEventType = any> {
    type: TEvent;
    timestamp: Date;
    author?: IUserInfo;
}

export interface IIssueEventDto<TEvent extends IssueEventType = any> {
    type: TEvent;
    timestamp: string;
    author?: IUserInfoDto;
}

type TypeOfIssueEventDto<TEvent extends IssueEventType> =
    TEvent extends IssueEventType.Title ? ITitleIssueEventDto :
        TEvent extends IssueEventType.Body ? IBodyIssueEventDto :
            TEvent extends IssueEventType.Status ? IStatusIssueEventDto :
                TEvent extends IssueEventType.AssignedUsers ? IAssignedUsersIssueEventDto :
                    TEvent extends IssueEventType.Labels ? ILabelsIssueEventDto :
                        never;

type TypeOfIssueEvent<TEvent extends IssueEventType> =
    TEvent extends IssueEventType.Title ? ITitleIssueEvent :
        TEvent extends IssueEventType.Body ? IBodyIssueEvent :
            TEvent extends IssueEventType.Status ? IStatusIssueEvent :
                TEvent extends IssueEventType.AssignedUsers ? IAssignedUsersIssueEvent :
                    TEvent extends IssueEventType.Labels ? ILabelsIssueEvent :
                        never;

export function mapIssueEventDtoToIssueEvent(dto: null): null;
export function mapIssueEventDtoToIssueEvent<TEvent extends IssueEventType, TIssueEventDto extends TypeOfIssueEventDto<TEvent>, TIssueEvent extends TypeOfIssueEvent<TEvent>>(dto: TIssueEventDto): TIssueEvent;
export function mapIssueEventDtoToIssueEvent<TEvent extends IssueEventType, TIssueEventDto extends TypeOfIssueEventDto<TEvent>, TIssueEvent extends TypeOfIssueEvent<TEvent>>(dto: TIssueEventDto | null): TIssueEvent | null;
export function mapIssueEventDtoToIssueEvent<TEvent extends IssueEventType, TIssueEventDto extends TypeOfIssueEventDto<TEvent>, TIssueEvent extends TypeOfIssueEvent<TEvent>>(dto: TIssueEventDto | null): TIssueEvent | null {
    if (dto === null) {
        return null;
    } else if (dto.type === IssueEventType.Title) {
        return mapTitleIssueEventDtoToTitleIssueEvent(dto as any) as any;
    } else if (dto.type === IssueEventType.Body) {
        return mapBodyIssueEventDtoToBodyIssueEvent(dto as any) as any;
    } else if (dto.type === IssueEventType.Status) {
        return mapStatusIssueEventDtoToStatusIssueEvent(dto as any) as any;
    } else if (dto.type === IssueEventType.AssignedUsers) {
        return mapAssignedUsersIssueEventDtoToAssignedUsersIssueEvent(dto as any) as any;
    } else if (dto.type === IssueEventType.Labels) {
        return mapLabelsIssueEventDtoToLabelsIssueEvent(dto as any) as any;
    }

    throw new Error(`Unsupported event type: ${dto.type}`);
}

function mapIssueEventDtoToIssueEventBase<TEvent extends IssueEventType, TIssueEventDto extends IIssueEventDto<TEvent>, TIssueEvent extends IIssueEvent<TEvent>>(
    dto: TIssueEventDto | null,
    additionalProperties?: (dto: TIssueEventDto) => Omit<TIssueEvent, keyof IIssueEventDto<TEvent>>
): TIssueEvent | null {
    if (dto === null) {
        return null;
    }

    return {
        type: dto.type,
        timestamp: new Date(dto.timestamp),
        author: dto.author ? mapUserInfoDtoToUserInfo(dto.author) : undefined,
        ...additionalProperties?.(dto)
    } as any;
}

export interface ITitleIssueEvent extends IIssueEvent<IssueEventType.Title> {}

export interface ITitleIssueEventDto extends IIssueEventDto<IssueEventType.Title> {}

export function mapTitleIssueEventDtoToTitleIssueEvent(event: null): null;
export function mapTitleIssueEventDtoToTitleIssueEvent(event: ITitleIssueEventDto): ITitleIssueEvent;
export function mapTitleIssueEventDtoToTitleIssueEvent(event: ITitleIssueEventDto | null): ITitleIssueEvent | null;
export function mapTitleIssueEventDtoToTitleIssueEvent(event: ITitleIssueEventDto | null): ITitleIssueEvent | null {
    return mapIssueEventDtoToIssueEventBase(event);
}

export interface IBodyIssueEvent extends IIssueEvent<IssueEventType.Body> {}

export interface IBodyIssueEventDto extends IIssueEventDto<IssueEventType.Body> {}

export function mapBodyIssueEventDtoToBodyIssueEvent(event: null): null;
export function mapBodyIssueEventDtoToBodyIssueEvent(event: IBodyIssueEventDto): IBodyIssueEvent;
export function mapBodyIssueEventDtoToBodyIssueEvent(event: IBodyIssueEventDto | null): IBodyIssueEvent | null;
export function mapBodyIssueEventDtoToBodyIssueEvent(event: IBodyIssueEventDto | null): IBodyIssueEvent | null {
    return mapIssueEventDtoToIssueEventBase(event);
}

export interface IStatusIssueEvent extends IIssueEvent<IssueEventType.Status> {
    status: IssueStatus;
}

export interface IStatusIssueEventDto extends IIssueEventDto<IssueEventType.Status> {
    status: IssueStatus;
}

export function mapStatusIssueEventDtoToStatusIssueEvent(event: null): null;
export function mapStatusIssueEventDtoToStatusIssueEvent(event: IStatusIssueEventDto): IStatusIssueEvent;
export function mapStatusIssueEventDtoToStatusIssueEvent(event: IStatusIssueEventDto | null): IStatusIssueEvent | null;
export function mapStatusIssueEventDtoToStatusIssueEvent(event: IStatusIssueEventDto | null): IStatusIssueEvent | null {
    return mapIssueEventDtoToIssueEventBase(event, event => ({
        status: event.status
    }));
}

export interface IAssignedUsersIssueEvent extends IIssueEvent<IssueEventType.AssignedUsers> {
    assignedUsers: IUserInfo[];
}

export interface IAssignedUsersIssueEventDto extends IIssueEventDto<IssueEventType.AssignedUsers> {
    assignedUsers: IUserInfoDto[];
}

export function mapAssignedUsersIssueEventDtoToAssignedUsersIssueEvent(event: null): null;
export function mapAssignedUsersIssueEventDtoToAssignedUsersIssueEvent(event: IAssignedUsersIssueEventDto): IAssignedUsersIssueEvent;
export function mapAssignedUsersIssueEventDtoToAssignedUsersIssueEvent(event: IAssignedUsersIssueEventDto | null): IAssignedUsersIssueEvent | null;
export function mapAssignedUsersIssueEventDtoToAssignedUsersIssueEvent(event: IAssignedUsersIssueEventDto | null): IAssignedUsersIssueEvent | null {
    return mapIssueEventDtoToIssueEventBase(event, event => ({
        assignedUsers: event.assignedUsers.map(user => mapUserInfoDtoToUserInfo(user))
    }));
}

export interface ILabelsIssueEvent extends IIssueEvent<IssueEventType.Labels> {
    labels: ILabel[];
}

export interface ILabelsIssueEventDto extends IIssueEventDto<IssueEventType.Labels> {
    labels: ILabelDto[];
}

export function mapLabelsIssueEventDtoToLabelsIssueEvent(event: null): null;
export function mapLabelsIssueEventDtoToLabelsIssueEvent(event: ILabelsIssueEventDto): ILabelsIssueEvent;
export function mapLabelsIssueEventDtoToLabelsIssueEvent(event: ILabelsIssueEventDto | null): ILabelsIssueEvent | null;
export function mapLabelsIssueEventDtoToLabelsIssueEvent(event: ILabelsIssueEventDto | null): ILabelsIssueEvent | null {
    return mapIssueEventDtoToIssueEventBase(event, event => ({
        labels: event.labels.map(label => mapLabelDtoToLabel(label))
    }));
}
