import { IUserInfo, IUserInfoDto, mapUserInfoDtoToUserInfo } from '@/model/IUserInfo';

export interface IProjectMember {
    isAdministrator: boolean;
    user: IUserInfo;
}

export interface IProjectMemberDto {
    isAdministrator: boolean;
    user: IUserInfoDto;
}

export function mapProjectMemberDtoToProjectMember(projectMemberDto: IProjectMemberDto): IProjectMember;
export function mapProjectMemberDtoToProjectMember(projectMemberDto: null): null;
export function mapProjectMemberDtoToProjectMember(projectMemberDto: IProjectMemberDto | null): IProjectMember | null;
export function mapProjectMemberDtoToProjectMember(projectMemberDto: IProjectMemberDto | null): IProjectMember | null {
    if (!projectMemberDto) {
        return null;
    }

    return {
        isAdministrator: projectMemberDto.isAdministrator,
        user: mapUserInfoDtoToUserInfo(projectMemberDto.user)
    };
}
