export class Result<T = undefined> {
    static readonly InternalErrorCode = 'internal-error';

    readonly value: T;

    readonly errorCode?: string;

    get hasError(): boolean {
        return typeof this.errorCode !== 'undefined';
    }

    protected constructor(value?: T, errorCode?: string) {
        this.value = value as T;
        this.errorCode = errorCode;
    }

    static success(): Result;
    static success<T>(value: T): Result<T>;
    static success<T>(value?: T): Result<T> {
        return new Result<T>(value);
    }

    static error<T>(errorCode: string = Result.InternalErrorCode): Result<T> {
        return new Result<T>(undefined, errorCode);
    }
}
