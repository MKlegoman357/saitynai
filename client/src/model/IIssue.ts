import { IUserInfo, IUserInfoDto, mapUserInfoDtoToUserInfo } from '@/model/IUserInfo';
import { ILabel, ILabelDto, mapLabelDtoToLabel } from '@/model/ILabel';

export enum IssueStatus {
    Open = 'open',
    Closed = 'closed'
}

export interface IIssue {
    id: string;
    title: string;
    body: string;
    status: IssueStatus;
    dateCreated: Date;
    dateModified: Date;

    author: IUserInfo;
    assignedUsers: IUserInfo[];
    labels: ILabel[];
}

export interface IIssueDto {
    id: string;
    title: string;
    body: string;
    status: string;
    dateCreated: string;
    dateModified: string;

    author: IUserInfoDto;
    assignedUsers: IUserInfoDto[];
    labels: ILabelDto[];
}

export function mapIssueDtoToIssue(issueDto: IIssueDto): IIssue;
export function mapIssueDtoToIssue(issueDto: null): null;
export function mapIssueDtoToIssue(issueDto: IIssueDto | null): IIssue | null;
export function mapIssueDtoToIssue(issueDto: IIssueDto | null): IIssue | null {
    if (!issueDto) {
        return null;
    }

    return {
        id: issueDto.id,
        title: issueDto.title,
        body: issueDto.body,
        status: issueDto.status as IssueStatus,
        dateCreated: new Date(issueDto.dateCreated),
        dateModified: new Date(issueDto.dateModified),

        author: mapUserInfoDtoToUserInfo(issueDto.author),
        assignedUsers: issueDto.assignedUsers.map(user => mapUserInfoDtoToUserInfo(user)),
        labels: issueDto.labels.map(label => mapLabelDtoToLabel(label))
    };
}
