import { IUserInfo, IUserInfoDto, mapUserInfoDtoToUserInfo } from '@/model/IUserInfo';

export interface IComment {
    id: string;
    body: string;
    author: IUserInfo;

    dateCreated: Date;
    dateModified: Date;
}

export interface ICommentDto {
    id: string;
    body: string;
    author: IUserInfoDto;

    dateCreated: string;
    dateModified: string;
}

export function mapCommentDtoToComment(commentDto: ICommentDto): IComment;
export function mapCommentDtoToComment(commentDto: null): null;
export function mapCommentDtoToComment(commentDto: ICommentDto | null): IComment | null;
export function mapCommentDtoToComment(commentDto: ICommentDto | null): IComment | null {
    if (!commentDto) {
        return null;
    }

    return {
        id: commentDto.id,
        body: commentDto.body,
        author: mapUserInfoDtoToUserInfo(commentDto.author),
        dateCreated: new Date(commentDto.dateCreated),
        dateModified: new Date(commentDto.dateModified)
    };
}
