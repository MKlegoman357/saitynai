export enum ProjectVisibility {
    Public = 'public',
    Private = 'private'
}

export interface IProject {
    id: string;
    name: string;
    visibility: ProjectVisibility;
    dateCreated: Date;
}

export interface IProjectDto {
    id: string;
    name: string;
    visibility: string;
    dateCreated: string;
}

export function mapProjectDtoToProject(projectDto: null): null;
export function mapProjectDtoToProject(projectDto: IProjectDto): IProject;
export function mapProjectDtoToProject(projectDto: IProjectDto | null): IProject | null;
export function mapProjectDtoToProject(projectDto: IProjectDto | null): IProject | null {
    if (!projectDto) {
        return null;
    }

    return {
        id: projectDto.id,
        name: projectDto.name,
        visibility: projectDto.visibility as ProjectVisibility,
        dateCreated: new Date(projectDto.dateCreated)
    };
}
