<template>
  <h2 class="settings-title">Project members</h2>

  <div class="alert alert-danger" v-if="membersError">
    {{ membersError }}
  </div>
  <template v-else-if="members">
    <div class="alert alert-danger mb-3" v-if="updatingError">
      {{ updatingError }}
    </div>

    <div class="alert alert-danger mb-3" v-if="usersError">
      {{ usersError }}
    </div>
    <div class="mb-3" v-else-if="users">
      <div class="alert alert-danger mb-3" v-if="addMemberError">
        {{ addMemberError }}
      </div>

      <div class="d-flex align-items-center">
        <searchable-dropdown :items="filteredUsers"
                             class="mr-2"
                             display-value="username"
                             :filter="(user, filter) => user.username.includes(filter) || user.id.includes(filter)"
                             value-placeholder="Select user..."
                             v-model="selectedUser"
                             :disabled="addingMember"/>

        <button class="btn btn-primary mr-2" type="button" :disabled="!selectedUser || addingMember" @click="onAddMember">
          <i class="fas fa-user-plus"></i>

          Add member
        </button>

        <div class="spinner-border text-accent" v-if="addingMember"></div>
      </div>
    </div>
    <div class="mb-3" v-else>
      <span class="spinner-border text-accent"></span> Loading users...
    </div>

    <div class="list-group">
      <div class="list-group-item d-flex align-items-center" v-for="member of members" :key="member.user.id">
        <div>
          {{ member.user.username }}

          <span class="badge badge-accent" v-if="member.isAdministrator">admin</span>
        </div>
        <div class="ml-auto">
          <button class="btn btn-sm btn-primary mr-2"
                  type="button"
                  v-if="!member.isAdministrator"
                  :disabled="updatingMembers"
                  @click="updateRole(member, true)">
            <i class="fas fa-user-tie mr-1"></i>

            Promote to admin
          </button>

          <button class="btn btn-sm btn-accent mr-2"
                  type="button"
                  v-if="member.isAdministrator"
                  :disabled="hasSingleAdmin || updatingMembers"
                  @click="updateRole(member, false)">
            <i class="fas fa-user-slash mr-1"></i>

            Demote from admin
          </button>

          <button class="btn btn-sm btn-danger"
                  type="button"
                  data-toggle="modal"
                  :data-target="'#' + id('remove-member-modal')"
                  :disabled="member.isAdministrator && hasSingleAdmin || updatingMembers"
                  @click="memberToRemove = member">
            <i class="fas fa-trash mr-1"></i>

            Remove
          </button>
        </div>
      </div>

      <div :id="id('remove-member-modal')" class="modal fade" tabindex="-1" ref="removeMemberModalRef">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Remove member</h5>
              <button type="button" class="close" data-dismiss="modal">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              Are you sure you want to remove <i class="font-weight-bold">{{ memberToRemove?.user.username }}</i> from project members?
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
              <div class="d-flex align-items-center ml-auto">
                <div class="spinner-border mr-2 text-accent" v-if="isUpdating"></div>
                <button type="button" class="btn btn-outline-danger ml-auto" @click="onRemoveMember" :disabled="isUpdating">
                  Remove member
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </template>
  <div v-else>
    <p>
      Loading members...
    </p>

    <div class="progress">
      <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary w-100"></div>
    </div>
  </div>
</template>

<script lang="ts">
import { computed, defineComponent, PropType, ref, toRefs, watch } from 'vue';
import { useUniqueId } from '@/utils/uniqueId';
import { IProject } from '@/model/IProject';
import { IProjectMember } from '@/model/IProjectMember';
import { membersService } from '@/services/MembersService';
import { localizeErrorCode } from '@/utils/localization';
import SearchableDropdown from '@/components/SearchableDropdown.vue';
import { IUserInfo } from '@/model/IUserInfo';
import { userInfosService } from '@/services/UserInfosService';
import $ from 'jquery';
import { authService } from '@/services/AuthService';
import { useRouter } from 'vue-router';
import { triggerProjectAccess } from '@/utils/projectAccess';

export default defineComponent({
  components: {SearchableDropdown},
  props: {
    project: {
      type: Object as PropType<IProject>,
      required: true
    },
    isUpdating: {
      type: Boolean as PropType<boolean>,
      required: true
    },
    updateProject: {
      type: Function as PropType<<T>(func: () => Promise<T>) => Promise<T>>,
      required: true
    }
  },
  setup(props: { project: IProject }) {
    const id = useUniqueId('members-settings-tab');
    const router = useRouter();

    const {project} = toRefs(props);

    const members = ref<IProjectMember[]>();
    const membersError = ref<string>();
    const getMembers = () => {
      membersError.value = undefined;

      membersService.getAll(project.value.id).then(result => {
        if (result.hasError) {
          membersError.value = localizeErrorCode(result.errorCode!);
        } else {
          members.value = result.value;
        }
      });
    };

    watch(project, () => {
      getMembers();
    }, {immediate: true});

    const hasSingleAdmin = computed(() => members.value ? members.value.filter(m => m.isAdministrator).length <= 1 : false);

    const users = ref<IUserInfo[]>();
    const usersError = ref<string>();
    const getUsers = () => {
      usersError.value = undefined;

      userInfosService.getAll().then(result => {
        if (result.hasError) {
          usersError.value = localizeErrorCode(result.errorCode!);
        } else {
          users.value = result.value.sort((a, b) => a.username.localeCompare(b.username));
        }
      });
    };

    getUsers();

    const filteredUsers = computed<IUserInfo[]>(() => users.value ? (members.value ? users.value.filter(u => !members.value!.some(m => m.user.id === u.id)) : users.value) : []);

    const selectedUser = ref<IUserInfo | null>(null);
    const addMemberError = ref<string>();
    const addingMember = ref(false);
    const onAddMember = () => {
      if (selectedUser.value === null) return;

      addMemberError.value = undefined;
      addingMember.value = true;

      membersService.create(project.value.id, selectedUser.value.id, {isAdministrator: false}).then(result => {
        if (result.hasError) {
          addMemberError.value = localizeErrorCode(result.errorCode!);
        } else {
          selectedUser.value = null;
          getMembers()
        }
      }).finally(() => {
        addingMember.value = false;
      });
    };

    const updatingError = ref<string>();
    const updatingMembers = ref(false);

    const updateRole = (member: IProjectMember, isAdministrator: boolean) => {
      updatingMembers.value = true;
      updatingError.value = undefined;

      membersService.update(project.value.id, member.user.id, {isAdministrator}).then(async result => {
        if (result.hasError) {
          updatingError.value = localizeErrorCode(result.errorCode!);
        } else {
          member.isAdministrator = isAdministrator;

          if (member.user.id === authService.claims.value?.userId && !isAdministrator) {
            triggerProjectAccess();

            await router.push({name: 'project', params: {projectId: project.value.id}});
          }
        }
      }).finally(() => {
        updatingMembers.value = false;
      });
    };

    const removeMemberModalRef = ref<HTMLElement>();
    const memberToRemove = ref<IProjectMember | null>(null);
    const onRemoveMember = () => {
      if (memberToRemove.value === null) return;

      updatingMembers.value = true;
      updatingError.value = undefined;

      $(removeMemberModalRef.value!).modal('hide');

      membersService.delete(project.value.id, memberToRemove.value.user.id).then(result => {
        if (result.hasError) {
          updatingError.value = localizeErrorCode(result.errorCode!);
        } else {
          members.value = members.value?.filter(m => m.user.id !== memberToRemove.value!.user.id);
          memberToRemove.value = null;
        }
      }).finally(() => {
        updatingMembers.value = false;
      });
    };

    return {
      id,

      members,
      membersError,

      hasSingleAdmin,

      users,
      usersError,

      filteredUsers,

      selectedUser,
      addMemberError,
      addingMember,
      onAddMember,

      updatingError,
      updatingMembers,

      updateRole,

      removeMemberModalRef,
      memberToRemove,
      onRemoveMember
    };
  }
});
</script>
