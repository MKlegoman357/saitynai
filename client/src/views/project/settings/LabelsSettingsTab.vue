<template>
  <h2 class="settings-title">Project labels</h2>

  <div class="alert alert-danger" v-if="loadingError">
    {{ loadingError }}
  </div>
  <div v-else-if="labels">
    <div class="alert alert-danger mb-3" v-if="addLabelError">
      {{ addLabelError }}
    </div>

    <form class="d-flex flex-column flex-md-row" @submit.prevent="onAddLabel">
      <div class="mr-2">
        <label :for="id('new-label')" class="mr-1">New label:</label>
        <input :id="id('new-label')" class="form-control d-block d-md-inline-block w-auto" type="text" v-model="newLabel" required :readonly="addingLabel">
      </div>

      <div class="d-flex align-items-start mt-2 mt-md-0">
        <button class="btn btn-primary" type="submit" :disabled="addingLabel">
          Add label
        </button>

        <span class="spinner-border text-accent ml-2" v-if="addingLabel"></span>
      </div>
    </form>

    <ul class="list-group mt-3" v-if="labels.length > 0">
      <label-list-item v-for="label of labels" :key="label.name" :project="project" :label="label" :refresh-labels="getLabels"/>
    </ul>
    <p class="mt-3" v-else>
      There are no labels in this project.
    </p>
  </div>
  <div v-else>
    <p>
      Loading labels...
    </p>

    <div class="progress">
      <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary w-100"></div>
    </div>
  </div>
</template>

<script lang="tsx">
import { defineComponent, PropType, ref, toRefs, watch } from 'vue';
import { useUniqueId } from '@/utils/uniqueId';
import { IProject } from '@/model/IProject';
import { ILabel } from '@/model/ILabel';
import { labelsService } from '@/services/LabelsService';
import { localizeErrorCode } from '@/utils/localization';

const labelListItem = defineComponent({
  props: {
    project: {
      type: Object as PropType<IProject>,
      required: true
    },
    label: {
      type: Object as PropType<ILabel>,
      required: true
    },
    refreshLabels: {
      type: Function as PropType<() => void>,
      required: true
    }
  },
  setup(props: { project: IProject, label: ILabel, refreshLabels: () => void }) {
    const {project, label, refreshLabels} = toRefs(props);

    const error = ref<string>();

    const deleting = ref(false);
    const onDelete = () => {
      deleting.value = true;
      error.value = undefined;

      labelsService.delete(project.value.id, label.value.name).then(result => {
        if (result.hasError) {
          error.value = localizeErrorCode(result.errorCode!);
        } else {
          refreshLabels.value();
        }
      }).finally(() => {
        deleting.value = false;
      });
    };

    const editing = ref(false);

    const newName = ref(label.value.name);
    const saving = ref(false);
    const onSave = () => {
      saving.value = true;
      error.value = undefined;

      labelsService.update(project.value.id, label.value.name, {name: newName.value}).then(result => {
        if (result.hasError) {
          error.value = localizeErrorCode(result.errorCode!);
        } else {
          editing.value = false;
          label.value.name = newName.value;
        }
      }).finally(() => {
        saving.value = false;
      });
    };

    watch(editing, (editing, oldEditing) => {
      if (editing !== oldEditing) {
        error.value = undefined;

        if (editing) {
          newName.value = label.value.name;
        }
      }
    });

    return () => (
        <li class="list-group-item">
          {error.value && <div class="alert alert-danger">
            {error.value}
          </div>}
          {editing.value ? <div class="d-flex align-items-center">
            <input class="form-control w-auto mr-auto" type="text" v-model={newName.value}/>

            <button class="btn btn-primary ml-1" type="button" onClick={onSave} disabled={saving.value}>
              Save
            </button>

            <button class="btn btn-outline-danger ml-1" type="button" onClick={() => editing.value = false} disabled={saving.value}>
              Cancel
            </button>

            {saving.value && <div class="spinner-border text-accent ml-2"/>}
          </div> : <div class="d-flex align-items-center">
            <div>
              <span class="badge badge-pill badge-accent" style="font-size: 1rem">{label.value.name}</span>
            </div>
            <div class="ml-auto">
              <button class="btn btn-sm btn-primary mr-2" type="button" onClick={() => editing.value = true} disabled={deleting.value}>
                <i class="fas fa-edit mr-1"/>

                Edit
              </button>

              <button class="btn btn-sm btn-danger" type="button" onClick={onDelete} disabled={deleting.value}>
                <i class="fas fa-trash mr-1"/>

                Delete
              </button>
            </div>
          </div>}
        </li>
    );
  }
});

export default defineComponent({
  components: {
    labelListItem
  },
  props: {
    project: {
      type: Object as PropType<IProject>,
      required: true
    },
    isUpdating: {
      type: Boolean as PropType<boolean>,
      required: true
    },
    updateProject: {
      type: Function as PropType<<T>(func: () => Promise<T>) => Promise<T>>,
      required: true
    }
  },
  setup(props: { project: IProject }) {
    const id = useUniqueId('labels-settings-tab');

    const {project} = toRefs(props);

    const labels = ref<ILabel[]>();
    const loadingError = ref<string>();

    const getLabels = () => {
      loadingError.value = undefined;

      labelsService.getAll(project.value.id).then(result => {
        if (result.hasError) {
          loadingError.value = localizeErrorCode(result.errorCode!);
        } else {
          labels.value = result.value;
        }
      });
    };

    watch(project, () => {
      getLabels();
    }, {immediate: true});

    const newLabel = ref('');
    const addLabelError = ref<string>();
    const addingLabel = ref(false);
    const onAddLabel = () => {
      addLabelError.value = undefined;
      addingLabel.value = true;

      labelsService.create(project.value.id, {name: newLabel.value}).then(result => {
        if (result.hasError) {
          addLabelError.value = localizeErrorCode(result.errorCode!);
        } else {
          newLabel.value = '';
          getLabels();
        }
      }).finally(() => {
        addingLabel.value = false;
      });
    };

    return {
      id,

      labels,
      loadingError,
      getLabels,

      newLabel,
      addLabelError,
      addingLabel,
      onAddLabel
    };
  }
});
</script>
