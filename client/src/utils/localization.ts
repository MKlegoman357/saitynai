export function localizeErrorCode(errorCode: string, errorCodeMessages: Record<string, string> = {}, showErrorCode: boolean = true): string {
    if (errorCode in errorCodeMessages)
        return errorCodeMessages[errorCode];

    if (showErrorCode)
        return `Unexpected error occurred: ${errorCode}.`;

    return 'Unexpected error occurred.';
}
