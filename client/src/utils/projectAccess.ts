import { ref, Ref, watch, WatchStopHandle } from 'vue';
import { authService, IAccessTokenClaims } from '@/services/AuthService';
import { membersService } from '@/services/MembersService';
import { projectsService } from '@/services/ProjectsService';
import { IProject } from '@/model/IProject';
import { Role } from '@/model/Role';

export interface IProjectAccess {
    canAccess: boolean;
    isMember: boolean;
    isAdministrator: boolean;
    canModify: boolean;
}

const projectAccessTrigger = ref({});

export function triggerProjectAccess(): void {
    projectAccessTrigger.value = {};
}

export async function getProjectAccess(project: IProject | string | null | undefined, user: IAccessTokenClaims | null | undefined): Promise<IProjectAccess> {
    const projectAccess: IProjectAccess = {
        canAccess: false,
        isMember: false,
        isAdministrator: false,
        canModify: false
    };

    let projectPromise: Promise<void> | null = null;
    let memberPromise: Promise<void> | null = null;

    if (user?.role === Role.Admin) {
        projectAccess.canAccess = true;
        projectAccess.canModify = true;
    }

    if (project) {
        const projectId = project && typeof project === 'object' ? project.id : project;

        if (typeof project === 'object') {
            projectAccess.canAccess = true;
        } else {
            projectPromise = projectsService.get(projectId).then(result => {
                if (!result.hasError && result.value) {
                    projectAccess.canAccess = true;
                }
            });
        }

        if (user) {
            memberPromise = membersService.get(projectId, user.userId).then(result => {
                if (!result.hasError) {
                    projectAccess.canAccess = true;
                    projectAccess.isMember = true;

                    if (result.value?.isAdministrator) {
                        projectAccess.isAdministrator = true;
                        projectAccess.canModify = true;
                    }
                }
            });
        }
    }

    await Promise.all([projectPromise, memberPromise]);

    return projectAccess;
}

function useProjectAccessInternal(project: Ref<IProject | string | null | undefined>): [WatchStopHandle, Ref<IProjectAccess>] {
    const projectAccess = ref<IProjectAccess>({canAccess: false, isMember: false, isAdministrator: false, canModify: false});

    const updateAccess = (project: IProject | string | null | undefined, claims: IAccessTokenClaims | undefined) => {
        projectAccess.value = {canAccess: false, isMember: false, isAdministrator: false, canModify: false};

        getProjectAccess(project, claims).then(access => projectAccess.value = access);
    };

    const stopWatch = watch(
        [project, authService.claims, projectAccessTrigger] as const,
        ([project, claims]) => updateAccess(project, claims),
        {immediate: true}
    );

    return [stopWatch, projectAccess];
}

export function useProjectAccess(project: Ref<IProject | string | null | undefined>): Ref<IProjectAccess> {
    return useProjectAccessInternal(project)[1];
}
