import moment, { unitOfTime } from 'moment';

const DateFormat = 'YYYY-MM-DD';
const TimeFormat = 'HH:mm';
const TimeWithSeconds = `${TimeFormat}:ss`
const DateTimeFormat = `${DateFormat} ${TimeFormat}`;
const DateTimeWithSecondsFormat = `${DateFormat} ${TimeWithSeconds}`;
const DateTimeWithTimezoneFormat = `${DateTimeFormat} Z`;
const LocaleDateTimeFormat = 'llll';

export interface IDateFormatter<TOptions = void> {
    format(date: Date, options?: TOptions): string;
}

export interface IRelativeDateFormatterOptions {
    unitOfTime: unitOfTime.Diff;
    threshold: number;

    recentFormat: string;
    oldFormat: string;
}

const relativeDateTimeFormatter: IDateFormatter<Partial<IRelativeDateFormatterOptions>> = {
    format(date: Date, options: Partial<IRelativeDateFormatterOptions> = {}): string {
        const formatterOptions: IRelativeDateFormatterOptions = {
            unitOfTime: 'days',
            threshold: 1,

            recentFormat: DateTimeFormat,
            oldFormat: DateFormat,
            ...options
        };

        const momentDate = moment(date);
        const currentMomentDate = moment();

        if (currentMomentDate.diff(momentDate, formatterOptions.unitOfTime, true) < formatterOptions.threshold) {
            return formatDate(date, formatterOptions.recentFormat);
        } else {
            return formatDate(date, formatterOptions.oldFormat);
        }
    }
}

export const DateFormats = {
    Date: createDateFormatter(DateFormat),
    Time: createDateFormatter(TimeFormat),
    TimeWithSeconds: createDateFormatter(TimeWithSeconds),
    DateTime: createDateFormatter(DateTimeFormat),
    DateTimeWithSeconds: createDateFormatter(DateTimeWithSecondsFormat),
    DateTimeWithTimezone: createDateFormatter(DateTimeWithTimezoneFormat),
    LocaleDateTime: createDateFormatter(LocaleDateTimeFormat),

    RelativeDateTime: relativeDateTimeFormatter
};

export function formatDate(date: Date, format: string = DateTimeFormat): string {
    return moment(date).format(format);
}

function createDateFormatter(format: string): { formatString: string } & IDateFormatter {
    return {
        formatString: format,
        format(date: Date): string {
            return formatDate(date, format);
        }
    };
}
