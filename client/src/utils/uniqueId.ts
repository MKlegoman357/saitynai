let nextId = 0;

export function getNextId(prefix?: string): string {
    prefix = prefix ? prefix + '_' : '';

    return prefix + nextId++;
}

export function useUniqueId(prefix?: string): (suffix: string) => string {
    prefix = getNextId(prefix);

    return suffix => prefix + '_' + suffix;
}
