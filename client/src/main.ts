import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import 'bootstrap';
import '@/styles/main.scss';
import { authService } from '@/services/AuthService';

const app = createApp(App)
    .use(router);

authService.init();

app.mount('#app');
