import { createRouter, createWebHistory, NavigationGuard, RouteLocationRaw, RouteRecordRaw } from 'vue-router';
import { authService } from '@/services/AuthService';
import { getProjectAccess, IProjectAccess } from '@/utils/projectAccess';
import { Role } from '@/model/Role';

function getProjectAccessGuard(canAccess: (projectAccess: IProjectAccess) => boolean | Promise<boolean>): NavigationGuard {
    return async (to) => {
        const projectId = to.params.projectId as string | undefined;

        const projectAccess = await getProjectAccess(projectId, authService.claims.value);

        if (!await canAccess(projectAccess)) {
            return {
                name: 'access-denied',
                query: {
                    url: to.fullPath
                }
            };
        }

        return true;
    };
}

const routes: RouteRecordRaw[] = [
    {
        meta: {
            authenticate: false
        },
        name: 'home',
        path: '/',
        redirect: {name: 'projects'}
    },
    {
        meta: {
            authenticate: false
        },
        name: 'projects',
        path: '/projects',
        component: () => import('@/views/projects/ProjectsView.vue')
    },
    {
        meta: {
            authenticate: false
        },
        name: 'project',
        path: '/project/:projectId',
        redirect: route => ({name: 'project-issues', params: {projectId: route.params.projectId}}),
        component: () => import('@/views/project/ProjectView.vue'),
        children: [
            {
                meta: {
                    authenticate: false
                },
                name: 'project-issues',
                path: 'issues',
                component: () => import('@/views/project/IssuesTab.vue')
            },
            {
                meta: {
                    authenticate: true
                },
                beforeEnter: [getProjectAccessGuard(a => a.canModify)],
                name: 'project-settings',
                path: 'settings',
                component: () => import('@/views/project/SettingsTab.vue')
            },
            {
                meta: {
                    authenticate: false
                },
                name: 'issue',
                path: 'issue/:issueId',
                component: () => import('@/views/issue/IssueView.vue')
            }
        ]
    },
    {
        meta: {
            authenticate: false,
        },
        name: 'profile',
        path: '/profile/:userId',
        component: () => import('@/views/ProfileView.vue')
    },
    {
        meta: {
            authenticate: false
        },
        name: 'login',
        path: '/login',
        component: () => import('@/views/LoginView.vue')
    },
    {
        meta: {
            authenticate: false
        },
        name: 'register',
        path: '/register',
        component: () => import('@/views/RegisterView.vue')
    },
    {
        meta: {
            authenticate: Role.Admin
        },
        name: 'administration',
        path: '/administration',
        redirect: {name: 'administration-users'},
        component: () => import('@/views/administration/Administration.vue'),
        children: [
            {
                meta: {
                    authenticate: Role.Admin
                },
                name: 'administration-users',
                path: 'users',
                component: () => import('@/views/administration/UsersTab.vue')
            }
        ]
    },
    {
        meta: {
            authenticate: false
        },
        name: 'access-denied',
        path: '/access-denied',
        component: {
            template: '<div class="card"><div class="card-body">You don\'t have permissions to access the requested page.</div></div>'
        }
    },
    {
        meta: {
            authenticate: false
        },
        name: '404',
        path: '/:path(.*)*',
        component: {
            template: '<div class="card"><div class="card-body">404 page not found :(</div></div>'
        }
    }
];

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
});

router.beforeEach((to, from, next) => {
    const authenticate = to.meta.authenticate !== false ? to.meta.authenticate as (true | Role) : false;

    if (authenticate && !authService.isLoggedIn.value) {
        next({
            name: 'login',
            query: {
                returnTo: to.fullPath
            }
        });
    } else if (typeof authenticate === 'string' && authService.claims.value?.role !== authenticate) {
        next({
            name: 'access-denied',
            query: {
                url: to.fullPath
            }
        });
    } else {
        next();
    }
});

export function routeExists(location: RouteLocationRaw): boolean {
    const resolved = router.resolve(location);

    return resolved.name !== '404';
}

export default router;
