import { requestService, RequestService } from '@/services/RequestService';
import { Result } from '@/model/Result';
import { IssuesService } from '@/services/IssuesService';
import { IIssueEvent, IIssueEventDto, mapIssueEventDtoToIssueEvent } from '@/model/IIssueEvent';

export class IssueEventsService {

    static readonly BASE_PATH = 'event';

    constructor(private requestService: RequestService) {}

    async getAll(projectId: string, issueId: string): Promise<Result<IIssueEvent[]>> {
        return await this.requestService.get<IIssueEventDto[], IIssueEvent[]>(
            IssueEventsService.getPath(projectId, issueId),
            value => value.map(event => mapIssueEventDtoToIssueEvent(event))
        );
    }

    static getPath(projectId: string, issueId: string): string {
        return IssuesService.getPath(projectId, issueId) + '/' + IssueEventsService.BASE_PATH;
    }

}

export const issueEventsService = new IssueEventsService(requestService);
