import { Result } from '@/model/Result';
import { requestService, RequestService } from '@/services/RequestService';
import { ProjectsService } from '@/services/ProjectsService';
import { IIssue, IIssueDto, IssueStatus, mapIssueDtoToIssue } from '@/model/IIssue';

export interface IIssuesFilter {
    labels?: string[];
    status?: IssueStatus;
    authors?: string[];
    title?: string;
}

export interface INewIssue {
    title: string;
    body: string;
    authorId: string;

    labels: string[];
    assignedUsers: string[];
}

export interface IIssueUpdates {
    title: string;
    body: string;
    status: IssueStatus;

    labels: string[];
    assignedUsers: string[];
}

export class IssuesService {

    static readonly BASE_PATH = 'issue';

    constructor(private requestService: RequestService) {}

    async getAll(projectId: string, filter: IIssuesFilter = {}): Promise<Result<IIssue[]>> {
        const params = {
            title: filter.title,
            status: filter.status,
            authors: filter.authors?.join(','),
            labels: filter.labels?.join(',')
        };

        return await this.requestService.get<IIssueDto[], IIssue[]>(
            IssuesService.getPath(projectId),
            value => value.map(issue => mapIssueDtoToIssue(issue)),
            {params}
        );
    }

    async get(projectId: string, issueId: string): Promise<Result<IIssue | null>> {
        return await this.requestService.get<IIssueDto, IIssue | null>(
            IssuesService.getPath(projectId, issueId),
            value => mapIssueDtoToIssue(value),
            undefined,
            true
        );
    }

    async create(projectId: string, newIssue: INewIssue): Promise<Result<IIssue>> {
        return await this.requestService.post<IIssueDto, IIssue>(
            IssuesService.getPath(projectId),
            {
                ...newIssue
            },
            value => mapIssueDtoToIssue(value)
        );
    }

    async update(projectId: string, issueId: string, updates: Partial<IIssueUpdates>): Promise<Result> {
        return await this.requestService.patch(
            IssuesService.getPath(projectId, issueId),
            {
                ...updates
            }
        );
    }

    async delete(projectId: string, issueId: string): Promise<Result> {
        return await this.requestService.delete(
            IssuesService.getPath(projectId, issueId)
        );
    }

    static getPath(projectId: string, issueId?: string): string {
        return ProjectsService.BASE_PATH + '/' + projectId + '/' + IssuesService.BASE_PATH + (issueId ? '/' + issueId : '');
    }

}

export const issuesService = new IssuesService(requestService);
