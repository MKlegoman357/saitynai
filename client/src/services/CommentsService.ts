import { Result } from '@/model/Result';
import { requestService, RequestService } from '@/services/RequestService';
import { IComment, ICommentDto, mapCommentDtoToComment } from '@/model/IComment';
import { IssuesService } from '@/services/IssuesService';

export interface INewComment {
    body: string;
    author: string;
}

export interface ICommentUpdates {
    body: string;
}

export class CommentsService {

    static readonly BASE_PATH = 'comment';

    constructor(private requestService: RequestService) {}

    async getAll(projectId: string, issueId: string): Promise<Result<IComment[]>> {
        return await this.requestService.get<ICommentDto[], IComment[]>(
            CommentsService.getPath(projectId, issueId),
            value => value.map(comment => mapCommentDtoToComment(comment))
        );
    }

    async get(projectId: string, issueId: string, commentId: string): Promise<Result<IComment | null>> {
        return await this.requestService.get<ICommentDto, IComment | null>(
            CommentsService.getPath(projectId, issueId, commentId),
            value => mapCommentDtoToComment(value),
            undefined,
            true
        );
    }

    async create(projectId: string, issueId: string, newComment: INewComment): Promise<Result<IComment>> {
        return await this.requestService.post<ICommentDto, IComment>(
            CommentsService.getPath(projectId, issueId),
            {
                ...newComment
            },
            value => mapCommentDtoToComment(value)
        );
    }

    async update(projectId: string, issueId: string, commentId: string, updates: Partial<ICommentUpdates>): Promise<Result> {
        return await this.requestService.patch(
            CommentsService.getPath(projectId, issueId, commentId),
            {
                ...updates
            }
        );
    }

    async delete(projectId: string, issueId: string, commentId: string): Promise<Result> {
        return await this.requestService.delete(
            CommentsService.getPath(projectId, issueId, commentId)
        );
    }

    static getPath(projectId: string, issueId: string, commentId?: string): string {
        return IssuesService.getPath(projectId, issueId) + '/' + CommentsService.BASE_PATH + (commentId ? '/' + commentId : '');
    }

}

export const commentsService = new CommentsService(requestService);
