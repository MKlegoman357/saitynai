import { Result } from '@/model/Result';
import Axios, { AxiosInstance } from 'axios';
import { API_BASE_URL } from '@/config';
import { computed, ref, watch } from 'vue';
import jwtDecode from 'jwt-decode';
import { IResponse } from '@/model/IResponse';
import { Role } from '@/model/Role';

export interface ILoginSuccessResponse {
    accessToken: string;
}

export enum AuthErrorCodes {
    InvalidCredentials = 'invalid-credentials',
    EmailTaken = 'email-taken',
    UsernameTaken = 'username-taken',
    Unauthorized = 'unauthorized'
}

export interface IAccessTokenClaimsRaw {
    sub: string;
    email: string;
    username: string;
    role: string;

    iat: number;
    exp: number;
}

export interface IAccessTokenClaims {
    userId: string;
    username: string;
    role: Role;

    issuedAt: Date;
    expiresAt: Date;
}

export class AuthService {

    static readonly AccessTokenStorageKey = 'access_token';

    static readonly expiresInvalidationOffset = 10 * 1000; // 10 seconds

    accessToken = ref<string>();

    claims = computed(() => {
        if (this.accessToken.value) {
            const claims = jwtDecode<IAccessTokenClaimsRaw>(this.accessToken.value);

            return {
                userId: claims.sub,
                username: claims.username,
                role: claims.role as Role,

                issuedAt: new Date(claims.iat * 1000),
                expiresAt: new Date(claims.exp * 1000)
            } as IAccessTokenClaims;
        }

        return undefined;
    });

    isLoggedIn = computed(
        () => typeof this.accessToken.value !== 'undefined'
    );

    constructor() {
        watch(
            this.accessToken,
            accessToken => {
                if (accessToken) {
                    localStorage.setItem(AuthService.AccessTokenStorageKey, accessToken);
                } else {
                    localStorage.removeItem(AuthService.AccessTokenStorageKey)
                }
            }
        );

        watch(
            () => this.claims.value?.expiresAt,
            (expiresAt, oldExpiresAt, onInvalidate) => {
                if (expiresAt) {
                    const id = setTimeout(() => {
                        this.accessToken.value = undefined;
                    }, Math.max(expiresAt.getTime() - Date.now() - AuthService.expiresInvalidationOffset, 0));

                    onInvalidate(() => clearTimeout(id));
                }
            }
        );
    }

    init(): void {
        const token = localStorage.getItem(AuthService.AccessTokenStorageKey);

        if (token) {
            this.accessToken.value = token;
        }
    }

    withAccessToken(axios: AxiosInstance = Axios.create()): AxiosInstance {
        if (this.accessToken.value) {
            axios.defaults.headers['Authorization'] = `Bearer ${this.accessToken.value}`;
        }

        return axios;
    }

    async login(usernameOrEmail: string, password: string): Promise<Result> {
        const response = await Axios.post<IResponse<ILoginSuccessResponse>>(
            API_BASE_URL + 'auth/login',
            {username: usernameOrEmail, password},
            {
                validateStatus: status => status >= 200 && status < 300 || status === 400
            }
        );

        if (response.status === 400 || !response.data?.ok) {
            return Result.error(AuthErrorCodes.InvalidCredentials);
        }

        this.accessToken.value = response.data.data.accessToken;

        return Result.success();
    }

    async logout(): Promise<Result> {
        this.accessToken.value = undefined;

        return Result.success();
    }

    async register(email: string, username: string, password: string): Promise<Result> {
        const response = await Axios.post<IResponse<ILoginSuccessResponse>>(
            API_BASE_URL + 'auth/register',
            {email, username, password},
            {
                validateStatus: status => status >= 200 && status < 300 || status === 400
            }
        );

        const jsonResponse = response.data;

        if (!jsonResponse.ok) {
            return Result.error(jsonResponse.errorCode);
        }

        return Result.success();
    }

}

export const authService = new AuthService();
