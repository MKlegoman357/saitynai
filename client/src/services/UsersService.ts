import { Result } from '@/model/Result';
import { requestService, RequestService } from '@/services/RequestService';
import { Role } from '@/model/Role';
import { IUser, IUserDto, mapUserDtoToUser } from '@/model/IUser';

export interface INewUser {
    email: string;
    username: string;
    password: string;
    role: Role;
}

export class UsersService {

    static readonly BASE_PATH = 'user';

    constructor(private requestService: RequestService) {}

    async getAll(): Promise<Result<IUser[]>> {
        return await this.requestService.get<IUserDto[], IUser[]>(
            UsersService.getPath(),
            value => value.map(user => mapUserDtoToUser(user))
        );
    }

    async get(userId: string): Promise<Result<IUser | null>> {
        return await this.requestService.get<IUserDto, IUser | null>(
            UsersService.getPath(userId),
            value => mapUserDtoToUser(value),
            undefined,
            true
        );
    }

    async create(newUser: INewUser): Promise<Result<IUser>> {
        return await this.requestService.post<IUserDto, IUser>(
            UsersService.getPath(),
            {
                ...newUser
            },
            value => mapUserDtoToUser(value)
        );
    }

    async delete(userId: string): Promise<Result> {
        return await this.requestService.delete(
            UsersService.getPath(userId)
        );
    }

    static getPath(userId?: string): string {
        return UsersService.BASE_PATH + (typeof userId === 'undefined' ? '' : '/' + userId);
    }

}

export const usersService = new UsersService(requestService);
