import { Result } from '@/model/Result';
import { requestService, RequestService } from '@/services/RequestService';
import { IUserInfo, IUserInfoDto, mapUserInfoDtoToUserInfo } from '@/model/IUserInfo';

export class UserInfosService {

    static readonly BASE_PATH = 'user-info';

    constructor(private requestService: RequestService) {}

    async getAll(): Promise<Result<IUserInfo[]>> {
        return await this.requestService.get<IUserInfoDto[], IUserInfo[]>(
            UserInfosService.getPath(),
            value => value.map(user => mapUserInfoDtoToUserInfo(user))
        );
    }

    async get(userId: string): Promise<Result<IUserInfo | null>> {
        return await this.requestService.get<IUserInfoDto, IUserInfo | null>(
            UserInfosService.getPath(userId),
            value => mapUserInfoDtoToUserInfo(value),
            undefined,
            true
        );
    }

    static getPath(userId?: string): string {
        return UserInfosService.BASE_PATH + (typeof userId === 'undefined' ? '' : '/' + userId);
    }

}

export const userInfosService = new UserInfosService(requestService);
