import { authService, AuthService } from '@/services/AuthService';
import { Result } from '@/model/Result';
import { API_BASE_URL } from '@/config';
import { IResponse } from '@/model/IResponse';
import { AxiosRequestConfig, Method } from 'axios';

export class RequestService {

    constructor(private authService: AuthService) {}

    async request<T = undefined, TResult = T>(path: string, method: Method, transform?: (value: T) => TResult, config?: AxiosRequestConfig, returnNullOn404?: false): Promise<Result<TResult>>;
    async request<T = undefined, TResult = T>(path: string, method: Method, transform: ((value: T | null) => TResult) | undefined, config: AxiosRequestConfig | undefined, returnNullOn404: true): Promise<Result<TResult | null>>;
    async request<T = undefined, TResult = T>(path: string, method: Method, transform: (value: T | null) => TResult = value => value as any, config?: AxiosRequestConfig, returnNullOn404: boolean = false): Promise<Result<TResult | null>> {
        const axios = this.authService.withAccessToken();

        const response = await axios.request<IResponse<T> | undefined>(
            {
                url: API_BASE_URL + path,
                method,
                validateStatus: status => status >= 200 && status < 300
                    || status === 400 || status === 401 || status === 403 || status === 404,
                ...config
            }
        );

        if (returnNullOn404 && response.status === 404) {
            return Result.success(transform(null));
        }

        const jsonResponse = response.data;

        if (!jsonResponse) {
            return Result.error();
        }

        if (!jsonResponse.ok) {
            return Result.error(jsonResponse.errorCode);
        }

        return Result.success(transform(jsonResponse.data));
    }

    async get<T, TResult = T>(path: string, transform?: (value: T) => TResult, config?: AxiosRequestConfig, returnNullOn404?: false): Promise<Result<TResult>>;
    async get<T, TResult = T>(path: string, transform: ((value: T | null) => TResult) | undefined, config: AxiosRequestConfig | undefined, returnNullOn404: true): Promise<Result<TResult | null>>;
    async get<T, TResult = T>(path: string, transform?: (value: T | null) => TResult, config?: AxiosRequestConfig, returnNullOn404: boolean = false): Promise<Result<TResult | null>> {
        return this.request(path, 'GET', transform, config, returnNullOn404 as any);
    }

    async post<T, TResult = T>(path: string, data: any, transform?: (value: T) => TResult, config?: AxiosRequestConfig): Promise<Result<TResult>> {
        return this.request(path, 'POST', transform, {
            data,
            ...config
        });
    }

    async patch(path: string, data: any, config?: AxiosRequestConfig): Promise<Result> {
        return this.request(path, 'PATCH', undefined, {
            data,
            ...config
        });
    }

    async delete(path: string, config?: AxiosRequestConfig): Promise<Result> {
        return this.request(path, 'DELETE', undefined, config);
    }

}

export const requestService = new RequestService(authService);
