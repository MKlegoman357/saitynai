import { Result } from '@/model/Result';
import { requestService, RequestService } from '@/services/RequestService';
import { IProject, IProjectDto, mapProjectDtoToProject, ProjectVisibility } from '@/model/IProject';

export interface IProjectsFilter {
    userId?: string;
    query?: string;
    visibility?: ProjectVisibility;
}

export interface INewProject {
    id: string;
    name: string;
    visibility: ProjectVisibility;

    administrators: string[];
    otherMembers: string[];
}

export interface IProjectUpdates {
    name: string;
    visibility: ProjectVisibility;
}

export class ProjectsService {

    static readonly BASE_PATH = 'project';

    constructor(private requestService: RequestService) {}

    async getAll(filter: IProjectsFilter = {}): Promise<Result<IProject[]>> {
        const params = {
            userId: filter.userId,
            query: filter.query,
            visibility: filter.visibility
        };

        return await this.requestService.get<IProjectDto[], IProject[]>(
            ProjectsService.getPath(),
            value => value.map(project => mapProjectDtoToProject(project)),
            {params}
        );
    }

    async get(projectId: string): Promise<Result<IProject | null>> {
        return await this.requestService.get<IProjectDto, IProject | null>(
            ProjectsService.getPath(projectId),
            value => mapProjectDtoToProject(value),
            undefined,
            true
        );
    }

    async create(newProject: INewProject): Promise<Result<IProject>> {
        return await this.requestService.post<IProjectDto, IProject>(
            ProjectsService.getPath(newProject.id),
            {
                ...newProject,
                id: undefined
            },
            value => mapProjectDtoToProject(value)
        );
    }

    async update(projectId: string, updates: Partial<IProjectUpdates>): Promise<Result> {
        return await this.requestService.patch(
            ProjectsService.getPath(projectId),
            {
                ...updates
            }
        );
    }

    async delete(projectId: string): Promise<Result> {
        return await this.requestService.delete(
            ProjectsService.getPath(projectId)
        );
    }

    static getPath(projectId?: string): string {
        return ProjectsService.BASE_PATH + (typeof projectId === 'undefined' ? '' : '/' + projectId);
    }

}

export const projectsService = new ProjectsService(requestService);
