import { Result } from '@/model/Result';
import { requestService, RequestService } from '@/services/RequestService';
import { ILabel, ILabelDto, mapLabelDtoToLabel } from '@/model/ILabel';
import { ProjectsService } from '@/services/ProjectsService';

export interface INewLabel {
    name: string;
}

export interface ILabelUpdates {
    name: string;
}

export class LabelsService {

    static readonly BASE_PATH = 'label';

    constructor(private requestService: RequestService) {}

    async getAll(projectId: string): Promise<Result<ILabel[]>> {
        return await this.requestService.get<ILabelDto[], ILabel[]>(
            LabelsService.getPath(projectId),
            value => value.map(label => mapLabelDtoToLabel(label))
        );
    }

    async get(projectId: string, labelName: string): Promise<Result<ILabel | null>> {
        return await this.requestService.get<ILabelDto, ILabel | null>(
            LabelsService.getPath(projectId, labelName),
            value => mapLabelDtoToLabel(value),
            undefined,
            true
        );
    }

    async create(projectId: string, newLabel: INewLabel): Promise<Result<ILabel>> {
        return await this.requestService.post<ILabelDto, ILabel>(
            LabelsService.getPath(projectId, newLabel.name),
            undefined,
            value => mapLabelDtoToLabel(value)
        );
    }

    async update(projectId: string, labelName: string, updates: Partial<ILabelUpdates>): Promise<Result> {
        return await this.requestService.patch(
            LabelsService.getPath(projectId, labelName),
            {
                ...updates
            }
        );
    }

    async delete(projectId: string, labelName: string): Promise<Result> {
        return await this.requestService.delete(
            LabelsService.getPath(projectId, labelName)
        );
    }

    static getPath(projectId: string, labelName?: string): string {
        return ProjectsService.BASE_PATH + '/' + projectId + '/' + LabelsService.BASE_PATH + (labelName ? '/' + labelName : '');
    }

}

export const labelsService = new LabelsService(requestService);
