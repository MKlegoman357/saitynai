import { Result } from '@/model/Result';
import { requestService, RequestService } from '@/services/RequestService';
import { ProjectsService } from '@/services/ProjectsService';
import { IProjectMember, IProjectMemberDto, mapProjectMemberDtoToProjectMember } from '@/model/IProjectMember';

export interface INewMember {
    isAdministrator: boolean;
}

export interface IUpdateMember {
    isAdministrator: boolean;
}

export class MembersService {

    static readonly BASE_PATH = 'member';

    constructor(private requestService: RequestService) {}

    async getAll(projectId: string): Promise<Result<IProjectMember[]>> {
        return await this.requestService.get<IProjectMemberDto[], IProjectMember[]>(
            MembersService.getPath(projectId),
            value => value.map(member => mapProjectMemberDtoToProjectMember(member))
        );
    }

    async get(projectId: string, memberId: string): Promise<Result<IProjectMember | null>> {
        return await this.requestService.get<IProjectMemberDto, IProjectMember | null>(
            MembersService.getPath(projectId, memberId),
            value => mapProjectMemberDtoToProjectMember(value),
            undefined,
            true
        );
    }

    async create(projectId: string, userId: string, newMember: INewMember): Promise<Result<IProjectMember>> {
        return await this.requestService.post<IProjectMemberDto, IProjectMember>(
            MembersService.getPath(projectId, userId),
            newMember,
            value => mapProjectMemberDtoToProjectMember(value)
        );
    }

    async update(projectId: string, userId: string, updates: Partial<IUpdateMember>): Promise<Result> {
        return await this.requestService.patch(
            MembersService.getPath(projectId, userId),
            {
                ...updates
            }
        );
    }

    async delete(projectId: string, userId: string): Promise<Result> {
        return await this.requestService.delete(
            MembersService.getPath(projectId, userId)
        );
    }

    static getPath(projectId: string, userId?: string): string {
        return ProjectsService.BASE_PATH + '/' + projectId + '/' + MembersService.BASE_PATH + (userId ? '/' + userId : '');
    }

}

export const membersService = new MembersService(requestService);
