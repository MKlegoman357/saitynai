import { Document, FilterQuery } from 'mongoose';
import { QuerySelector } from 'mongodb';
import { escapeRegExp } from './string';

export function conjunction<T extends Document>(conditions: FilterQuery<T>[]): FilterQuery<T> {
  if (conditions.length <= 0) return {};

  return { $and: conditions as any };
}

export function disjunction<T extends Document>(conditions: FilterQuery<T>[]): FilterQuery<T> {
  if (conditions.length <= 0) return {};

  return { $or: conditions as any };
}

export function not<T extends Document>(filterQuery: FilterQuery<T>): FilterQuery<T> {
  return { $nor: [filterQuery] as any };
}

export function stringContains<T extends string>(substring: string): QuerySelector<T> {
  return { $regex: escapeRegExp(substring), $options: 'i' } as QuerySelector<T>;
}
