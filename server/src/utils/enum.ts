export function getEnumValues<T>(enumeration: Record<string, T>): T[] {
  return Object.keys(enumeration)
    .filter(key => enumeration.hasOwnProperty(key))
    .map(key => enumeration[key]);
}
