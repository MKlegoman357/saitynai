export function randomInt(max: number): number
export function randomInt(min: number, max?: number): number {
  if (typeof max === 'undefined') {
    max = min;
    min = 0;
  }

  return min + Math.round(Math.random() * (max - min));
}

export async function generateRandomString(length: number = 4, validate: (str: string) => Promise<boolean> = () => Promise.resolve(true), attempts: number = 3): Promise<string> {
  const chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  let str: string;

  do {
    attempts--;

    if (attempts < 0) {
      throw new Error('failed to generate a random string');
    }

    str = '';

    for (let i = 0; i < length; i++) {
      str += chars[randomInt(chars.length)];
    }
  } while (!await validate(str));

  return str;
}
