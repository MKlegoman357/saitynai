import { Test, TestingModule } from '@nestjs/testing';
import { LabelService } from './label.service';
import { ProjectService } from '../project.service';
import { TestingDatabaseModule } from '../../../common/tests/testing-database.module';

describe('LabelService', () => {
  let module: TestingModule;
  let service: LabelService;

  const projectServiceMock = {};

  beforeEach(async () => {
    module = await Test.createTestingModule({
      imports: [TestingDatabaseModule],
      providers: [
        LabelService,
        ProjectService
      ]
    }).overrideProvider(ProjectService).useValue(projectServiceMock)
      .compile();

    service = module.get(LabelService);
  });

  afterEach(async () => {
    await module.close();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
