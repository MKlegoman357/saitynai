import { Injectable } from '@nestjs/common';
import { canModifyProject, ProjectService } from '../project.service';
import { Label } from '../../../models/Label';
import { ProjectDependentService } from '../ProjectDependentService';
import { LabelNotFoundError } from '../../common/errors/LabelNotFoundError';
import { LabelAlreadyExistsError } from '../../common/errors/LabelAlreadyExistsError';
import { InjectModel } from '@nestjs/mongoose';
import { Project, ProjectDocument } from '../../../models/Project';
import { Model } from 'mongoose';
import { IAuthenticatedUser } from '../../auth/auth.service';
import { PermissionDeniedError } from '../../common/errors/PermissionDeniedError';

export function canModifyLabels(project: Project, user: IAuthenticatedUser | null): boolean {
  return canModifyProject(project, user);
}

@Injectable()
export class LabelService extends ProjectDependentService {

  constructor(@InjectModel(Project.name) projectModel: Model<ProjectDocument>, projectService: ProjectService) {
    super({ labels: true }, projectModel, projectService);
  }

  async getAll(projectId: string, currentUser: IAuthenticatedUser | null): Promise<Label[]> {
    const project = await this.getProject(projectId, currentUser);

    return project.labels.map(label => label);
  }

  async get(projectId: string, name: string, currentUser: IAuthenticatedUser | null): Promise<Label | null> {
    const project = await this.getProject(projectId, currentUser);

    return project.labels.find(label => label.name === name) ?? null;
  }

  async add(projectId: string, name: string, currentUser: IAuthenticatedUser | null): Promise<Label> {
    const project = await this.getProject(projectId, currentUser, { administrators: { id: true } });

    if (!canModifyLabels(project, currentUser)) {
      throw new PermissionDeniedError(currentUser?.id);
    }

    if (project.labels.some(label => label.name === name)) {
      throw new LabelAlreadyExistsError(name);
    }

    const label = new Label(name);

    project.labels.push(label);

    await project.save({ validateModifiedOnly: true });

    return label;
  }

  async update(projectId: string, name: string, newName: string, currentUser: IAuthenticatedUser | null): Promise<void> {
    const project = await this.getProject(projectId, currentUser, {
      issues: { labels: true },
      administrators: { id: true }
    });

    if (!canModifyLabels(project, currentUser)) {
      throw new PermissionDeniedError(currentUser?.id);
    }

    const label = project.labels.find(label => label.name === name) ?? null;

    if (!label) {
      throw new LabelNotFoundError(name);
    }

    label.name = newName;

    for (const issue of project.issues) {
      for (const issueLabel of issue.labels) {
        if (issueLabel.name === name) {
          issueLabel.name = newName;
        }
      }
    }

    await project.save({ validateModifiedOnly: true });
  }

  async remove(projectId: string, name: string, currentUser: IAuthenticatedUser | null): Promise<void> {
    const project = await this.getProject(projectId, currentUser, {
      issues: { labels: true },
      administrators: { id: true }
    });

    if (!canModifyLabels(project, currentUser)) {
      throw new PermissionDeniedError(currentUser?.id);
    }

    let found = false;
    project.labels = project.labels.filter(label => label.name !== name || !(found = true));

    if (!found) {
      throw new LabelNotFoundError(name);
    }

    for (const issue of project.issues) {
      issue.labels = issue.labels.filter(label => label.name !== name);
    }

    await project.save({ validateModifiedOnly: true });
  }

  async removeAll(projectId: string, currentUser: IAuthenticatedUser | null): Promise<void> {
    const project = await this.getProject(projectId, currentUser, {
      issues: {},
      administrators: { id: true }
    });

    if (!canModifyLabels(project, currentUser)) {
      throw new PermissionDeniedError(currentUser?.id);
    }

    project.labels = [];

    for (const issue of project.issues) {
      issue.labels = [];
    }

    await project.save({ validateModifiedOnly: true });
  }

}
