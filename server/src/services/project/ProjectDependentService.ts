import { ProjectService } from './project.service';
import { ProjectDocument } from '../../models/Project';
import { ProjectNotFoundError } from '../common/errors/ProjectNotFoundError';
import { Model } from 'mongoose';
import { IAuthenticatedUser } from '../auth/auth.service';

export abstract class ProjectDependentService {
  protected constructor(
    protected projectProjection: any,
    protected projectModel: Model<ProjectDocument>,
    protected projectService: ProjectService
  ) {}

  protected async getProject(id: string, currentUser: IAuthenticatedUser | null, projection?: any): Promise<ProjectDocument> {
    const project = await this.projectService.get(id, currentUser, { ...this.projectProjection, ...projection });

    if (!project) {
      throw new ProjectNotFoundError(id);
    }

    return project;
  }
}
