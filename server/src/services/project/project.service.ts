import { Injectable } from '@nestjs/common';
import { Project, ProjectDocument } from '../../models/Project';
import { ProjectVisibility } from '../../models/ProjectVisibility';
import { UserService } from '../user/user.service';
import { NoProjectAdministratorError } from '../common/errors/NoProjectAdministratorError';
import { ProjectIdTakenError } from '../common/errors/ProjectIdTakenError';
import { ProjectNotFoundError } from '../common/errors/ProjectNotFoundError';
import { FilterQuery, Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { mapUserToUserInfo } from '../../models/UserInfo';
import { IAuthenticatedUser } from '../auth/auth.service';
import { Role } from '../../models/Role';
import { conjunction, not, stringContains } from '../../utils/mongoose';
import { PermissionDeniedError } from '../common/errors/PermissionDeniedError';

export interface IProjectsFilter {
  userId?: string;
  query?: string;
  visibility?: ProjectVisibility;
}

export interface INewProject {
  name: string;
  visibility: ProjectVisibility;
  administrators: string[];
  otherMembers: string[];
}

export interface IProjectUpdates {
  name: string;
  visibility: ProjectVisibility;
}

export function projectQueryFilter(query: string): FilterQuery<ProjectDocument> {
  return {
    $or: [
      {
        name: stringContains(query)
      },
      {
        id: query
      }
    ]
  };
}

export function isProjectMemberFilter(userId: string): FilterQuery<ProjectDocument> {
  return {
    $or: [
      {
        administrators: {
          $elemMatch: {
            id: userId
          }
        }
      },
      {
        otherMembers: {
          $elemMatch: {
            id: userId
          }
        }
      }
    ]
  };
}

export function canAccessProjectFilter(user: IAuthenticatedUser | null): FilterQuery<ProjectDocument> {
  if (!user) {
    return {
      visibility: ProjectVisibility.Public
    };
  } else if (user.role !== Role.Admin) {
    return {
      $or: [
        {
          visibility: ProjectVisibility.Public
        },
        isProjectMemberFilter(user.id)
      ]
    };
  }

  return {};
}

export function isProjectAdmin(project: Project, userId: string): boolean {
  return project.administrators.some(a => a.id === userId);
}

export function isProjectMember(project: Project, userId: string): boolean {
  return [...project.administrators, ...project.otherMembers].some(a => a.id === userId);
}

export function canModifyProject(project: Project, user: IAuthenticatedUser | null): boolean {
  return !!user && (user.role === Role.Admin || isProjectAdmin(project, user.id));
}

export function canAccessProject(project: Project, user: IAuthenticatedUser | null): boolean {
  return project.visibility === ProjectVisibility.Public || !!user && (user.role === Role.Admin || isProjectMember(project, user.id));
}

@Injectable()
export class ProjectService {

  constructor(@InjectModel(Project.name) private projectModel: Model<ProjectDocument>, private userService: UserService) {}

  async getAll(currentUser: IAuthenticatedUser | null, projection?: any, filter: IProjectsFilter = {}): Promise<ProjectDocument[]> {
    const queryFilters: FilterQuery<ProjectDocument>[] = [
      canAccessProjectFilter(currentUser)
    ];

    if (filter.visibility)
      queryFilters.push({ visibility: filter.visibility });

    if (filter.query)
      queryFilters.push(projectQueryFilter(filter.query));

    if (filter.userId)
      queryFilters.push(filter.userId[0] === '!' ? not(isProjectMemberFilter(filter.userId.substr(1))) : isProjectMemberFilter(filter.userId));

    return await this.projectModel.find(conjunction(queryFilters))
      .select({ id: true, name: true, visibility: true, dateCreated: true, ...projection })
      .exec();
  }

  async get(id: string, currentUser: IAuthenticatedUser | null, projection?: any): Promise<ProjectDocument | null> {
    return await this.projectModel.findOne(conjunction([{ id }, canAccessProjectFilter(currentUser)]))
      .select({ id: true, name: true, visibility: true, dateCreated: true, ...projection })
      .exec();
  }

  async create(id: string, newProject: INewProject): Promise<ProjectDocument> {
    if (newProject.administrators.length === 0) {
      throw new NoProjectAdministratorError();
    }

    if (await this.projectModel.exists({ id })) {
      throw new ProjectIdTakenError(id);
    }

    const administrators = (await this.userService.getAllByIds(newProject.administrators)).map(mapUserToUserInfo);
    const otherMembers = (await this.userService.getAllByIds(newProject.otherMembers)).map(mapUserToUserInfo);

    const project = new Project(
      id,
      newProject.name,
      newProject.visibility,
      new Date(),
      administrators,
      otherMembers,
      [],
      []
    );

    const projectModel = new this.projectModel(project);

    return await projectModel.save();
  }

  async update(id: string, updatedProperties: Partial<IProjectUpdates>, currentUser: IAuthenticatedUser | null): Promise<ProjectDocument> {
    const project = await this.get(id, currentUser, { administrators: { id: true } });

    if (!project) {
      throw new ProjectNotFoundError(id);
    }

    if (!currentUser || !canModifyProject(project, currentUser)) {
      throw new PermissionDeniedError(currentUser?.id);
    }

    if (typeof updatedProperties.name !== 'undefined') {
      project.name = updatedProperties.name;
    }

    if (typeof updatedProperties.visibility !== 'undefined') {
      project.visibility = updatedProperties.visibility;
    }

    return await project.save({ validateModifiedOnly: true });
  }

  async delete(id: string, currentUser: IAuthenticatedUser | null): Promise<void> {
    const project = await this.get(id, currentUser, { administrators: { id: true } });

    if (!project) {
      throw new ProjectNotFoundError(id);
    }

    if (!currentUser || !canModifyProject(project, currentUser)) {
      throw new PermissionDeniedError(currentUser?.id);
    }

    const result = await this.projectModel.deleteOne({ id });

    if (result.ok !== 1 || result.deletedCount !== 1) {
      throw new ProjectNotFoundError(id);
    }
  }
}
