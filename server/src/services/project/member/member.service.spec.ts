import { Test, TestingModule } from '@nestjs/testing';
import { MemberService } from './member.service';
import { ProjectService } from '../project.service';
import { UserService } from '../../user/user.service';
import { TestingDatabaseModule } from '../../../common/tests/testing-database.module';

describe('MemberService', () => {
  let module: TestingModule;
  let service: MemberService;

  const projectServiceMock = {};
  const userServiceMock = {};

  beforeEach(async () => {
    module = await Test.createTestingModule({
      imports: [TestingDatabaseModule],
      providers: [
        MemberService,
        ProjectService,
        UserService
      ]
    }).overrideProvider(ProjectService).useValue(projectServiceMock)
      .overrideProvider(UserService).useValue(userServiceMock)
      .compile();

    service = module.get(MemberService);
  });

  afterEach(async () => {
    await module.close();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
