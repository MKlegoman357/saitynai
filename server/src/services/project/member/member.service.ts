import { Injectable } from '@nestjs/common';
import { ProjectDependentService } from '../ProjectDependentService';
import { canModifyProject, ProjectService } from '../project.service';
import { User } from '../../../models/User';
import { UserService } from '../../user/user.service';
import { UserNotFoundError } from '../../common/errors/UserNotFoundError';
import { NoProjectAdministratorError } from '../../common/errors/NoProjectAdministratorError';
import { UserAlreadyAMemberError } from '../../common/errors/UserAlreadyAMemberError';
import { ProjectMemberNotFoundError } from '../../common/errors/ProjectMemberNotFoundError';
import { mapUserToUserInfo, UserInfo } from '../../../models/UserInfo';
import { Model } from 'mongoose';
import { Project, ProjectDocument } from '../../../models/Project';
import { InjectModel } from '@nestjs/mongoose';
import { IAuthenticatedUser } from '../../auth/auth.service';
import { PermissionDeniedError } from '../../common/errors/PermissionDeniedError';

export interface IProjectMember {
  isAdministrator: boolean;
  user: UserInfo;
}

export function canModifyMembers(project: Project, user: IAuthenticatedUser | null): boolean {
  return canModifyProject(project, user);
}

@Injectable()
export class MemberService extends ProjectDependentService {

  constructor(@InjectModel(Project.name) projectModel: Model<ProjectDocument>, projectService: ProjectService, private userService: UserService) {
    super({ administrators: true, otherMembers: true }, projectModel, projectService);
  }

  async getAll(projectId: string, currentUser: IAuthenticatedUser | null): Promise<IProjectMember[]> {
    const project = await this.getProject(projectId, currentUser);

    return [
      ...project.administrators.map(user => ({ isAdministrator: true, user } as IProjectMember)),
      ...project.otherMembers.map(user => ({ isAdministrator: false, user } as IProjectMember))
    ];
  }

  async get(projectId: string, userId: string, currentUser: IAuthenticatedUser | null): Promise<IProjectMember | null> {
    const project = await this.getProject(projectId, currentUser);

    const adminUser = project.administrators.find(otherUser => otherUser.id === userId);

    if (adminUser) {
      return { isAdministrator: true, user: adminUser };
    }

    const memberUser = project.otherMembers.find(otherUser => otherUser.id === userId);

    if (memberUser) {
      return { isAdministrator: false, user: memberUser };
    }

    return null;
  }

  async add(projectId: string, userId: string, isAdministrator: boolean, currentUser: IAuthenticatedUser | null): Promise<IProjectMember> {
    const project = await this.getProject(projectId, currentUser);

    if (!canModifyMembers(project, currentUser)) {
      throw new PermissionDeniedError(currentUser?.id);
    }

    const user = await this.getUser(userId);

    if ([...project.administrators, ...project.otherMembers].some(user => user.id === userId)) {
      throw new UserAlreadyAMemberError(userId);
    }

    const userInfo = mapUserToUserInfo(user);

    if (isAdministrator) {
      project.administrators.push(userInfo);
    } else {
      project.otherMembers.push(userInfo);
    }

    await project.save({ validateModifiedOnly: true });

    return { isAdministrator, user: userInfo };
  }

  async update(projectId: string, userId: string, isAdministrator: boolean, currentUser: IAuthenticatedUser | null): Promise<void> {
    const project = await this.getProject(projectId, currentUser);

    if (!canModifyMembers(project, currentUser)) {
      throw new PermissionDeniedError(currentUser?.id);
    }

    for (const user of project.administrators) {
      if (user.id === userId) {
        if (!isAdministrator) {
          if (project.administrators.length === 1) {
            throw new NoProjectAdministratorError();
          }

          project.administrators = project.administrators.filter(user => user.id !== userId);
          project.otherMembers.push(user);
        }

        await project.save({ validateModifiedOnly: true });

        return;
      }
    }

    for (const user of project.otherMembers) {
      if (user.id === userId) {
        if (isAdministrator) {
          project.otherMembers = project.otherMembers.filter(user => user.id !== userId);
          project.administrators.push(user);
        }

        await project.save({ validateModifiedOnly: true });

        return;
      }
    }

    throw new ProjectMemberNotFoundError(userId);
  }

  async remove(projectId: string, userId: string, currentUser: IAuthenticatedUser | null): Promise<void> {
    const project = await this.getProject(projectId, currentUser);

    if (!canModifyMembers(project, currentUser)) {
      throw new PermissionDeniedError(currentUser?.id);
    }

    if (project.administrators.length === 1 && project.administrators[0].id === userId) {
      throw new NoProjectAdministratorError();
    }

    let found = false;
    project.administrators = project.administrators.filter(otherUser => otherUser.id !== userId || !(found = true));

    if (!found) {
      project.otherMembers = project.otherMembers.filter(otherUser => otherUser.id !== userId || !(found = true));
    }

    if (!found) {
      throw new ProjectMemberNotFoundError(userId);
    }

    await project.save({ validateModifiedOnly: true });
  }

  private async getUser(id: string): Promise<User> {
    const user = await this.userService.get(id);

    if (!user) {
      throw new UserNotFoundError(id);
    }

    return user;
  }
}
