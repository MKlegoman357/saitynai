import { Test, TestingModule } from '@nestjs/testing';
import { ProjectService } from './project.service';
import { UserService } from '../user/user.service';
import { TestingDatabaseModule } from '../../common/tests/testing-database.module';

describe('ProjectService', () => {
  let module: TestingModule;
  let service: ProjectService;

  const userServiceMock = {};

  beforeEach(async () => {
    module = await Test.createTestingModule({
      imports: [TestingDatabaseModule],
      providers: [
        ProjectService,
        UserService
      ]
    }).overrideProvider(UserService).useValue(userServiceMock)
      .compile();

    service = module.get(ProjectService);
  });

  afterEach(async () => {
    await module.close();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
