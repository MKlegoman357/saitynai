import { Test, TestingModule } from '@nestjs/testing';
import { CommentService } from './comment.service';
import { ProjectService } from '../../project.service';
import { UserService } from '../../../user/user.service';
import { TestingDatabaseModule } from '../../../../common/tests/testing-database.module';

describe('CommentService', () => {
  let module: TestingModule;
  let service: CommentService;

  const projectServiceMock = {};
  const userServiceMock = {};

  beforeEach(async () => {
    module = await Test.createTestingModule({
      imports: [TestingDatabaseModule],
      providers: [
        CommentService,
        ProjectService,
        UserService
      ]
    }).overrideProvider(ProjectService).useValue(projectServiceMock)
      .overrideProvider(UserService).useValue(userServiceMock)
      .compile();

    service = module.get(CommentService);
  });

  afterEach(async () => {
    await module.close();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
