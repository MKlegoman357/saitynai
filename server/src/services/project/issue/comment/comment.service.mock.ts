import { IMockedService } from '../../../../common/tests/mocks';
import { CommentService, ICommentUpdates, INewComment } from './comment.service';
import { Comment } from '../../../../models/Comment';

interface ICommentServiceMockMethods {
  getAll: jest.Mock<Promise<Comment[]>, [string, string]>;
  get: jest.Mock<Promise<Comment | null>, [string, string, string]>;
  add: jest.Mock<Promise<Comment>, [string, string, INewComment, string | undefined]>;
  update: jest.Mock<Promise<void>, [string, string, string, Partial<ICommentUpdates>]>;
  remove: jest.Mock<Promise<void>, [string, string, string]>;
}

export type CommentServiceMock = ICommentServiceMockMethods & IMockedService & CommentService;

export function createCommentServiceMock(): CommentServiceMock {
  const service: ICommentServiceMockMethods & IMockedService = {
    getAll: jest.fn(),
    get: jest.fn(),
    add: jest.fn(),
    update: jest.fn(),
    remove: jest.fn(),
    resetMock() {
      service.getAll.mockReset();
      service.get.mockReset();
      service.add.mockReset();
      service.update.mockReset();
      service.remove.mockReset();
    }
  };

  return service as CommentServiceMock;
}
