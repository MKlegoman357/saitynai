import { Injectable } from '@nestjs/common';
import { IssueDependentService } from '../IssueDependentService';
import { canAccessProject, canModifyProject, ProjectService } from '../../project.service';
import { Comment } from '../../../../models/Comment';
import { UserService } from '../../../user/user.service';
import { generateRandomString } from '../../../../utils/random';
import { UserNotFoundError } from '../../../common/errors/UserNotFoundError';
import { CommentNotFoundError } from '../../../common/errors/CommentNotFoundError';
import { InjectModel } from '@nestjs/mongoose';
import { Project, ProjectDocument } from '../../../../models/Project';
import { Model } from 'mongoose';
import { mapUserToUserInfo } from '../../../../models/UserInfo';
import { IAuthenticatedUser } from '../../../auth/auth.service';
import { Issue } from '../../../../models/Issue';
import { IssueStatus } from '../../../../models/IssueStatus';
import { PermissionDeniedError } from '../../../common/errors/PermissionDeniedError';

export interface INewComment {
  body: string;
  author: string;
}

export interface ICommentUpdates {
  body: string;
}

export function canCreateComment(project: Project, issue: Issue, user: IAuthenticatedUser | null): boolean {
  return !!user && (issue.status === IssueStatus.Open && canAccessProject(project, user));
}

export function canModifyComment(project: Project, comment: Comment, user: IAuthenticatedUser | null): boolean {
  return !!user && (comment.author.id === user.id || canModifyProject(project, user));
}

@Injectable()
export class CommentService extends IssueDependentService {

  constructor(
    @InjectModel(Project.name) projectModel: Model<ProjectDocument>,
    projectService: ProjectService,
    private userService: UserService
  ) {
    super(projectModel, projectService);
  }

  async getAll(projectId: string, issueId: string, currentUser: IAuthenticatedUser | null): Promise<Comment[]> {
    const [issue] = await this.getIssue(projectId, issueId, currentUser);

    return issue.comments.map(comment => comment);
  }

  async get(projectId: string, issueId: string, commentId: string, currentUser: IAuthenticatedUser | null): Promise<Comment | null> {
    const [issue] = await this.getIssue(projectId, issueId, currentUser);

    return issue.comments.find(comment => comment.id === commentId) ?? null;
  }

  async add(projectId: string, issueId: string, newComment: INewComment, currentUser: IAuthenticatedUser | null, id?: string): Promise<Comment> {
    const [issue, project] = await this.getIssue(projectId, issueId, currentUser, {
      administrators: { id: true },
      otherMembers: { id: true }
    });

    if (!canCreateComment(project, issue, currentUser)) {
      throw new PermissionDeniedError(currentUser?.id);
    }

    const author = await this.userService.get(newComment.author);

    if (!author) {
      throw new UserNotFoundError(newComment.author);
    }

    const authorInfo = mapUserToUserInfo(author);
    const commentId = id ?? await generateRandomString(4, async id => issue.comments.every(comment => comment.id !== id));
    const dateCreated = new Date();
    const comment = new Comment(
      commentId,
      newComment.body,
      authorInfo,
      dateCreated,
      dateCreated
    );

    issue.comments.push(comment);

    await project.save({ validateModifiedOnly: true });

    return comment;
  }

  async update(projectId: string, issueId: string, commentId: string, bodyUpdates: Partial<ICommentUpdates>, currentUser: IAuthenticatedUser | null): Promise<void> {
    const [issue, project] = await this.getIssue(projectId, issueId, currentUser, { administrators: { id: true } });
    const comment = issue.comments.find(comment => comment.id === commentId) ?? null;

    if (!comment) {
      throw new CommentNotFoundError(commentId);
    }

    if (!canModifyComment(project, comment, currentUser)) {
      throw new PermissionDeniedError(currentUser?.id);
    }

    if (typeof bodyUpdates.body !== 'undefined') {
      comment.body = bodyUpdates.body;
    }

    comment.dateModified = new Date();

    await project.save({ validateModifiedOnly: true });
  }

  async remove(projectId: string, issueId: string, commentId: string, currentUser: IAuthenticatedUser | null): Promise<void> {
    const [issue, project] = await this.getIssue(projectId, issueId, currentUser, { administrators: { id: true } });
    const comment = issue.comments.find(comment => comment.id === commentId) ?? null;

    if (!comment) {
      throw new CommentNotFoundError(commentId);
    }

    if (!canModifyComment(project, comment, currentUser)) {
      throw new PermissionDeniedError(currentUser?.id);
    }

    issue.comments = issue.comments.filter(comment => comment.id !== commentId);

    await project.save({ validateModifiedOnly: true });
  }
}
