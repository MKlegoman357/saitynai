import { Injectable } from '@nestjs/common';
import { ProjectDependentService } from '../ProjectDependentService';
import { canModifyProject, ProjectService } from '../project.service';
import { Issue } from '../../../models/Issue';
import { IssueStatus } from '../../../models/IssueStatus';
import { UserService } from '../../user/user.service';
import { UserNotFoundError } from '../../common/errors/UserNotFoundError';
import { IssueNotFoundError } from '../../common/errors/IssueNotFoundError';
import { InjectModel } from '@nestjs/mongoose';
import { Project, ProjectDocument } from '../../../models/Project';
import { Model } from 'mongoose';
import { mapLabelToLabelInfo } from '../../../models/LabelInfo';
import { mapUserToUserInfo, UserInfo } from '../../../models/UserInfo';
import { IAuthenticatedUser } from '../../auth/auth.service';
import { PermissionDeniedError } from '../../common/errors/PermissionDeniedError';
import {
  AssignedUsersIssueEvent,
  BodyIssueEvent,
  IssueEvent,
  LabelsIssueEvent,
  StatusIssueEvent,
  TitleIssueEvent
} from '../../../models/IssueEvent';

export interface IIssuesFilter {
  labels?: string[];
  status?: IssueStatus;
  authors?: string[];
  title?: string;
}

export interface INewIssue {
  title: string;
  body: string;
  labels: string[];
  assignedUsers: string[];
  authorId: string;
}

export interface IIssueUpdates {
  title: string;
  body: string;
  status: IssueStatus;
  labels: string[];
  assignedUsers: string[];
}

export function canCreateIssue(project: Project, user: IAuthenticatedUser | null): boolean {
  return canModifyProject(project, user);
}

export function canModifyIssue(project: Project, issue: Issue, user: IAuthenticatedUser | null): boolean {
  return !!user && (issue.author.id === user.id || issue.assignedUsers.some(u => u.id === user.id) || canModifyProject(project, user));
}

@Injectable()
export class IssueService extends ProjectDependentService {

  constructor(
    @InjectModel(Project.name) projectModel: Model<ProjectDocument>,
    projectService: ProjectService,
    private userService: UserService
  ) {
    super({ issues: true }, projectModel, projectService);
  }

  async getAll(projectId: string, currentUser: IAuthenticatedUser | null, filter: IIssuesFilter = {}): Promise<Issue[]> {
    const project = await this.getProject(projectId, currentUser);

    return project.issues
      .filter(issue =>
        (!filter.status || issue.status === filter.status)
        && (!filter.title || issue.title.toLowerCase().includes(filter.title.toLowerCase()))
        && (!filter.authors || filter.authors.some(author => author === issue.author.id || author.toLowerCase() === issue.author.username.toLowerCase()))
        && (!filter.labels || issue.labels.some(label => filter.labels!.includes(label.name)))
      ).sort((a, b) => b.dateModified.getTime() - a.dateModified.getTime());
  }

  async get(projectId: string, issueId: string, currentUser: IAuthenticatedUser | null): Promise<Issue | null> {
    const project = await this.getProject(projectId, currentUser);

    return project.issues.find(issue => issue.id === issueId) ?? null;
  }

  async create(projectId: string, newIssue: INewIssue, currentUser: IAuthenticatedUser | null): Promise<Issue> {
    const project = await this.getProject(projectId, currentUser, { labels: true, administrators: { id: true } });

    if (!canCreateIssue(project, currentUser)) {
      throw new PermissionDeniedError(currentUser?.id);
    }

    const author = await this.userService.get(newIssue.authorId);

    if (!author) {
      throw new UserNotFoundError(newIssue.authorId);
    }

    const authorInfo = mapUserToUserInfo(author);
    const assignedUsers = (await this.userService.getAllByIds(newIssue.assignedUsers))
      .map(mapUserToUserInfo);
    const labels = project.labels
      .filter(label => newIssue.labels.includes(label.name))
      .map(mapLabelToLabelInfo);

    const issueId = this.generateNewIssueId(project.issues);
    const dateCreated = new Date()
    const issue = new Issue(
      issueId,
      newIssue.title,
      newIssue.body,
      IssueStatus.Open,
      dateCreated,
      dateCreated,
      authorInfo,
      assignedUsers,
      labels,
      [],
      []
    );

    project.issues.push(issue);

    await project.save({ validateModifiedOnly: true });

    return issue;
  }

  async update(projectId: string, issueId: string, updates: Partial<IIssueUpdates>, currentUser: IAuthenticatedUser | null): Promise<void> {
    const project = await this.getProject(projectId, currentUser, { labels: true, administrators: { id: true } });
    const issue = project.issues.find(issue => issue.id === issueId) ?? null;

    if (!issue) {
      throw new IssueNotFoundError(issueId);
    }

    if (!canModifyIssue(project, issue, currentUser)) {
      throw new PermissionDeniedError(currentUser?.id);
    }

    const updatedLabels = typeof updates.labels !== 'undefined'
      ? project.labels
        .filter(label => updates.labels!.includes(label.name))
        .map(mapLabelToLabelInfo)
      : null;

    const updatedAssignedUsers = typeof updates.assignedUsers !== 'undefined'
      ? (await this.userService.getAllByIds(updates.assignedUsers))
        .map(mapUserToUserInfo)
      : null;

    const events: IssueEvent[] = [];
    const eventTimestamp = new Date();
    const eventAuthor = new UserInfo(currentUser!.id, currentUser!.username);

    if (typeof updates.title !== 'undefined' && issue.title !== updates.title) {
      issue.title = updates.title;
      events.push(new TitleIssueEvent(eventTimestamp, eventAuthor));
    }

    if (typeof updates.body !== 'undefined' && issue.body !== updates.body) {
      issue.body = updates.body;
      events.push(new BodyIssueEvent(eventTimestamp, eventAuthor));
    }

    if (typeof updates.status !== 'undefined' && issue.status !== updates.status) {
      issue.status = updates.status;
      events.push(new StatusIssueEvent(eventTimestamp, issue.status, eventAuthor));
    }

    if (updatedLabels !== null && (updatedLabels.length !== issue.labels.length || !updatedLabels.every(updatedLabel => issue.labels.some(label => label.name === updatedLabel.name)))) {
      issue.labels = updatedLabels;
      events.push(new LabelsIssueEvent(eventTimestamp, issue.labels.map(label => label), eventAuthor));
    }

    if (updatedAssignedUsers !== null && (issue.assignedUsers.length !== updatedAssignedUsers.length || !updatedAssignedUsers.every(updatedUser => issue.assignedUsers.some(user => user.id === updatedUser.id)))) {
      issue.assignedUsers = updatedAssignedUsers;
      events.push(new AssignedUsersIssueEvent(eventTimestamp, issue.assignedUsers.map(user => user), eventAuthor));
    }

    for (const event of events) {
      issue.events.push(event);
    }

    if (events.length > 0) {
      issue.dateModified = eventTimestamp;
    }

    await project.save({ validateModifiedOnly: true });
  }

  async delete(projectId: string, issueId: string, currentUser: IAuthenticatedUser | null): Promise<void> {
    const project = await this.getProject(projectId, currentUser, { administrators: { id: true } });
    const issue = project.issues.find(issue => issue.id === issueId) ?? null;

    if (!issue) {
      throw new IssueNotFoundError(issueId);
    }

    if (!canModifyIssue(project, issue, currentUser)) {
      throw new PermissionDeniedError(currentUser?.id);
    }

    project.issues = project.issues.filter(issue => issue.id !== issueId);

    await project.save({ validateModifiedOnly: true });
  }

  private generateNewIssueId(issues: Issue[]): string {
    let lastNumericIssueId = 0;

    for (const issue of issues) {
      const numericId = Number(issue.id);

      if (!isNaN(numericId) && numericId > lastNumericIssueId) {
        lastNumericIssueId = numericId;
      }
    }

    return (lastNumericIssueId + 1).toString();
  }
}
