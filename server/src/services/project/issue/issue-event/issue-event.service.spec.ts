import { Test, TestingModule } from '@nestjs/testing';
import { IssueEventService } from './issue-event.service';
import { TestingDatabaseModule } from '../../../../common/tests/testing-database.module';
import { ProjectService } from '../../project.service';

describe('IssueEventService', () => {
  let service: IssueEventService;

  const projectServiceMock = {};

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [TestingDatabaseModule],
      providers: [IssueEventService, ProjectService],
    }).overrideProvider(ProjectService).useValue(projectServiceMock)
      .compile();

    service = module.get<IssueEventService>(IssueEventService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
