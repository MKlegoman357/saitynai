import { Injectable } from '@nestjs/common';
import { IssueDependentService } from '../IssueDependentService';
import { InjectModel } from '@nestjs/mongoose';
import { Project, ProjectDocument } from '../../../../models/Project';
import { Model } from 'mongoose';
import { ProjectService } from '../../project.service';
import { IAuthenticatedUser } from '../../../auth/auth.service';
import { IssueEvent } from '../../../../models/IssueEvent';

@Injectable()
export class IssueEventService extends IssueDependentService {

  constructor(
    @InjectModel(Project.name) projectModel: Model<ProjectDocument>,
    projectService: ProjectService
  ) {
    super(projectModel, projectService);
  }

  async getAll(projectId: string, issueId: string, currentUser: IAuthenticatedUser | null): Promise<IssueEvent[]> {
    const [issue] = await this.getIssue(projectId, issueId, currentUser);

    return issue.events
      .map(event => event)
      .sort((a, b) => a.timestamp.getTime() - b.timestamp.getTime());
  }

}
