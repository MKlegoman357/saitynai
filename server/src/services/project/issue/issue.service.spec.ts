import { Test, TestingModule } from '@nestjs/testing';
import { IssueService } from './issue.service';
import { ProjectService } from '../project.service';
import { UserService } from '../../user/user.service';
import { TestingDatabaseModule } from '../../../common/tests/testing-database.module';

describe('IssueService', () => {
  let module: TestingModule;
  let service: IssueService;

  const projectServiceMock = {};
  const userServiceMock = {};

  beforeEach(async () => {
    module = await Test.createTestingModule({
      imports: [TestingDatabaseModule],
      providers: [
        IssueService,
        ProjectService,
        UserService
      ]
    }).overrideProvider(ProjectService).useValue(projectServiceMock)
      .overrideProvider(UserService).useValue(userServiceMock)
      .compile();

    service = module.get(IssueService);
  });

  afterEach(async () => {
    await module.close();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
