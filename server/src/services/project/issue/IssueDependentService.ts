import { ProjectDependentService } from '../ProjectDependentService';
import { ProjectService } from '../project.service';
import { Issue } from '../../../models/Issue';
import { IssueNotFoundError } from '../../common/errors/IssueNotFoundError';
import { Model } from 'mongoose';
import { ProjectDocument } from '../../../models/Project';
import { IAuthenticatedUser } from '../../auth/auth.service';

export abstract class IssueDependentService extends ProjectDependentService {
  protected constructor(
    protected projectModel: Model<ProjectDocument>,
    projectService: ProjectService
  ) {
    super({ issues: true }, projectModel, projectService);
  }

  protected async getIssue(projectId: string, issueId: string, currentUser: IAuthenticatedUser | null, projectProjection?: any): Promise<[issue: Issue, project: ProjectDocument]> {
    const project = await this.getProject(projectId, currentUser, projectProjection);
    const issue = project.issues.find(issue => issue.id === issueId) ?? null;

    if (!issue) {
      throw new IssueNotFoundError(issueId);
    }

    return [issue, project];
  }
}
