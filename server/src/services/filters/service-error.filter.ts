import { ArgumentsHost, Catch, ExceptionFilter } from '@nestjs/common';
import { ServiceError } from '../common/errors/ServiceError';
import { Response as ExpressResponse } from 'express';
import { ErrorResponse } from '../../models/responses/ErrorResponse';

type ServiceErrorConstructor = {
  new(...args: any[]): ServiceError;
};

@Catch(ServiceError)
export class ServiceErrorFilter implements ExceptionFilter<ServiceError> {
  private errorCodeOverrides: Map<ServiceErrorConstructor, number> = new Map();

  constructor(errorCodeOverrides: { code: number, errors: ServiceErrorConstructor[] }[] = []) {
    for (const { code, errors } of errorCodeOverrides) {
      for (const error of errors) {
        this.errorCodeOverrides.set(error, code);
      }
    }
  }

  catch(exception: ServiceError, host: ArgumentsHost) {
    const httpHost = host.switchToHttp();
    const response = httpHost.getResponse<ExpressResponse>();

    const errorPrototype = Object.getPrototypeOf(exception) as ServiceErrorConstructor;
    const responseCode = this.errorCodeOverrides.get(errorPrototype.constructor as ServiceErrorConstructor) ?? exception.responseCode;

    response
      .status(responseCode)
      .json(new ErrorResponse(exception.errorCode, exception.data));
  }
}
