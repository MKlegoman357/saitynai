import { AuthService, IAuthenticatedUser, ILoginResponse } from './auth.service';
import { IMockedService } from '../../common/tests/mocks';

interface IAuthServiceMockMethods {
  login: jest.Mock<Promise<ILoginResponse>, [IAuthenticatedUser]>;
  validateUser: jest.Mock<IAuthenticatedUser | null, [string, string]>;
}

export type AuthServiceMock = IAuthServiceMockMethods & IMockedService & AuthService;

export function createAuthServiceMock(): AuthServiceMock {
  const service: IAuthServiceMockMethods & IMockedService = {
    login: jest.fn(),
    validateUser: jest.fn(),

    resetMock() {
      service.login.mockReset();
      service.validateUser.mockReset();
    }
  };

  return service as AuthServiceMock;
}
