import { Injectable } from '@nestjs/common';
import { INewUser, UserService } from '../user/user.service';
import { Role } from '../../models/Role';
import { JwtService } from '@nestjs/jwt';
import { ServiceError } from '../common/errors/ServiceError';

export interface IAuthenticatedUser {
  id: string;
  email: string;
  username: string;
  role: Role;
}

export interface ILoginResponse {
  accessToken: string;
}

export interface IRegisterResponse {
  success: boolean;
  errorCode?: string;
}

export interface IRegisterRequest {
  email: string;
  username: string;
  password: string;
}

@Injectable()
export class AuthService {
  constructor(private usersService: UserService, private jwtService: JwtService) {}

  async validateUser(usernameOrEmail: string, password: string): Promise<IAuthenticatedUser | null> {
    const user = await this.usersService.getByUsernameOrEmail(usernameOrEmail);

    if (user && await this.usersService.validatePassword(password, user.password)) {
      return {
        id: user.id,
        email: user.email,
        username: user.username,
        role: user.role
      };
    }

    return null;
  }

  async login(user: IAuthenticatedUser): Promise<ILoginResponse> {
    const payload = {
      sub: user.id,
      email: user.email,
      username: user.username,
      role: user.role
    };

    return {
      accessToken: this.jwtService.sign(payload)
    };
  }

  async register(request: IRegisterRequest): Promise<IRegisterResponse> {
    const newUser: INewUser = {
      email: request.email,
      username: request.username,
      password: request.password,
      role: Role.User
    };

    try {
      await this.usersService.create(newUser);

      return { success: true };
    } catch (e) {
      if (e instanceof ServiceError) {
        return { success: false, errorCode: e.errorCode };
      }

      throw e;
    }
  }
}
