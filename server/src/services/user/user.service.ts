import { Injectable } from '@nestjs/common';
import { User, UserDocument } from '../../models/User';
import { Role } from '../../models/Role';
import { UsernameTakenError } from '../common/errors/UsernameTakenError';
import { getAllByIds } from '../common/utils';
import { UserNotFoundError } from '../common/errors/UserNotFoundError';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { generateRandomString } from '../../utils/random';
import * as bcrypt from 'bcrypt';
import { ConfigService } from '@nestjs/config';
import { BcryptRoundsConfigKey } from '../../config/config';
import { EmailTakenError } from '../common/errors/EmailTakenError';

export interface INewUser {
  email: string;
  username: string;
  password: string;
  role: Role;
}

@Injectable()
export class UserService {

  private readonly bcryptRounds: number;

  constructor(@InjectModel(User.name) private userModel: Model<UserDocument>, configService: ConfigService) {
    this.bcryptRounds = Number(configService.get<string>(BcryptRoundsConfigKey)) || 10;
  }

  /**
   * @deprecated
   */
  async addUser(user: User, hashPassword: boolean = true): Promise<void> {
    if (hashPassword) {
      user.password = await bcrypt.hash(user.password, this.bcryptRounds);
    }

    const userModel = new this.userModel(user);

    await userModel.save();
  }

  async getAll(): Promise<UserDocument[]> {
    return await this.userModel.find().exec();
  }

  getAllByIds(userIds: string[], allowNulls?: false): Promise<UserDocument[]>;
  getAllByIds(userIds: string[], allowNulls: true): Promise<(UserDocument | null)[]>;
  getAllByIds(userIds: string[], allowNulls: boolean): Promise<(UserDocument | null)[]>;
  getAllByIds(userIds: string[], allowNulls: boolean = false): Promise<(UserDocument | null)[]> {
    return getAllByIds(
      userIds,
      (userId) => this.get(userId),
      allowNulls || (userId => new UserNotFoundError(userId))
    );
  }

  async get(id: string): Promise<UserDocument | null> {
    return await this.userModel.findOne({ id }).exec();
  }

  async getByUsernameOrEmail(usernameOrEmail: string): Promise<UserDocument | null> {
    return await this.userModel.findOne({ $or: [{ username: usernameOrEmail }, { email: usernameOrEmail }] }).exec();
  }

  async create(newUser: INewUser, id?: string): Promise<UserDocument> {
    const emailTaken = await this.userModel.exists({ email: newUser.email });
    const usernameTaken = await this.userModel.exists({ username: newUser.username });

    if (emailTaken) {
      throw new EmailTakenError(newUser.email);
    } else if (usernameTaken) {
      throw new UsernameTakenError(newUser.username);
    }

    const userId = id ?? await generateRandomString(4, async id => !await this.userModel.exists({ id }));
    const passwordHash = await bcrypt.hash(newUser.password, this.bcryptRounds);

    const user = new User(
      userId,
      newUser.email,
      newUser.username,
      passwordHash,
      newUser.role
    );

    const userModel = new this.userModel(user);

    return await userModel.save();
  }

  async delete(id: string): Promise<void> {
    const result = await this.userModel.deleteOne({ id });

    if (result.ok !== 1 || result.deletedCount !== 1) {
      throw new UserNotFoundError(id);
    }
  }

  async validatePassword(password: string, passwordHash: string): Promise<boolean> {
    return await bcrypt.compare(password, passwordHash);
  }
}
