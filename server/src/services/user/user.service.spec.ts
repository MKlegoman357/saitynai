import { Test, TestingModule } from '@nestjs/testing';
import { UserService } from './user.service';
import { TestingDatabaseModule } from '../../common/tests/testing-database.module';

describe('UserService', () => {
  let module: TestingModule;
  let service: UserService;

  beforeEach(async () => {
    module = await Test.createTestingModule({
      imports: [TestingDatabaseModule],
      providers: [UserService]
    }).compile();

    service = module.get(UserService);
  });

  afterEach(async () => {
    await module.close();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
