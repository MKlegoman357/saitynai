import { ServiceError } from './ServiceError';
import { SchemaObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { openApiObject, openApiString } from '../../../openapi/utils';
import { ErrorResponse } from '../../../models/responses/ErrorResponse';

export class LabelNotFoundError extends ServiceError {
  constructor(public name: string) {
    super('label-not-found', `Label with name '${name}' was not found`, { name });
  }

  static openApiResponse(): SchemaObject {
    return ErrorResponse.openApi('label-not-found', openApiObject({ name: openApiString() }));
  }
}
