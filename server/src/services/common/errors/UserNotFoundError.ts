import { ServiceError } from './ServiceError';
import { SchemaObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { openApiObject, openApiString } from '../../../openapi/utils';
import { ErrorResponse } from '../../../models/responses/ErrorResponse';

export class UserNotFoundError extends ServiceError {
  constructor(public userId: string) {
    super('user-not-found', `user with id ${userId} was not found`, { userId });
  }

  static openApiResponse(): SchemaObject {
    return ErrorResponse.openApi('user-not-found', openApiObject({ userId: openApiString() }));
  }
}
