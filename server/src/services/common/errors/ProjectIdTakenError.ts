import { ServiceError } from './ServiceError';
import { SchemaObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { ErrorResponse } from '../../../models/responses/ErrorResponse';
import { openApiObject, openApiString } from '../../../openapi/utils';

export class ProjectIdTakenError extends ServiceError {
  constructor(public projectId: string) {
    super('project-id-taken', `project id '${projectId}' is taken`, { projectId });
  }

  static openApiResponse(): SchemaObject {
    return ErrorResponse.openApi('project-id-taken', openApiObject({ projectId: openApiString() }));
  }
}
