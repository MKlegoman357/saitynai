import { ServiceError } from './ServiceError';
import { SchemaObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { ErrorResponse } from '../../../models/responses/ErrorResponse';
import { openApiObject, openApiString } from '../../../openapi/utils';

export class EmailTakenError extends ServiceError {
  constructor(public email: string) {
    super('email-taken', `email '${email}' is taken`, { email });
  }

  static openApiResponse(): SchemaObject {
    return ErrorResponse.openApi('email-taken', openApiObject({ email: openApiString() }));
  }
}
