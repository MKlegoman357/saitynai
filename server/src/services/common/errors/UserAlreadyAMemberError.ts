import { ServiceError } from './ServiceError';
import { SchemaObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { ErrorResponse } from '../../../models/responses/ErrorResponse';
import { openApiObject, openApiString } from '../../../openapi/utils';

export class UserAlreadyAMemberError extends ServiceError {
  constructor(public userId: string) {
    super('user-already-a-member', `User with id '${userId}' is already a member of this project`, { userId });
  }

  static openApiResponse(): SchemaObject {
    return ErrorResponse.openApi('user-already-a-member', openApiObject({ userId: openApiString() }));
  }
}
