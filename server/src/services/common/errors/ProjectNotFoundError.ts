import { ServiceError } from './ServiceError';
import { SchemaObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { openApiObject, openApiString } from '../../../openapi/utils';
import { ErrorResponse } from '../../../models/responses/ErrorResponse';

export class ProjectNotFoundError extends ServiceError {
  constructor(public projectId: string) {
    super('project-not-found', `project with id ${projectId} was not found`, { projectId });
  }

  static openApiResponse(): SchemaObject {
    return ErrorResponse.openApi('project-not-found', openApiObject({ projectId: openApiString() }));
  }
}
