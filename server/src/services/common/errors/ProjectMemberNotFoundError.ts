import { ServiceError } from './ServiceError';
import { SchemaObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { openApiObject, openApiString } from '../../../openapi/utils';
import { ErrorResponse } from '../../../models/responses/ErrorResponse';

export class ProjectMemberNotFoundError extends ServiceError {
  constructor(public userId: string) {
    super('project-member-not-found', `Project member with id ${userId} was not found`, { userId });
  }

  static openApiResponse(): SchemaObject {
    return ErrorResponse.openApi('project-member-not-found', openApiObject({ userId: openApiString() }));
  }
}
