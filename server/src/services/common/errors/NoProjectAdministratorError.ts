import { ServiceError } from './ServiceError';
import { SchemaObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { ErrorResponse } from '../../../models/responses/ErrorResponse';

export class NoProjectAdministratorError extends ServiceError {
  constructor() {
    super('no-project-administrator', 'no project administrators have been assigned');
  }

  static openApiResponse(): SchemaObject {
    return ErrorResponse.openApi('no-project-administrator');
  }
}
