import { ServiceError } from './ServiceError';
import { SchemaObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { ErrorResponse } from '../../../models/responses/ErrorResponse';
import { openApiObject, openApiString } from '../../../openapi/utils';

export class UsernameTakenError extends ServiceError {
  constructor(public username: string) {
    super('username-taken', `username '${username}' is taken`, { username });
  }

  static openApiResponse(): SchemaObject {
    return ErrorResponse.openApi('username-taken', openApiObject({ username: openApiString() }));
  }
}
