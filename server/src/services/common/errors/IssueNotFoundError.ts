import { ServiceError } from './ServiceError';
import { SchemaObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { ErrorResponse } from '../../../models/responses/ErrorResponse';
import { openApiObject, openApiString } from '../../../openapi/utils';

export class IssueNotFoundError extends ServiceError {
  constructor(public issueId: string) {
    super('issue-not-found', `Issue with id ${issueId} was not found`, { issueId });
  }

  static openApiResponse(): SchemaObject {
    return ErrorResponse.openApi('issue-not-found', openApiObject({ issueId: openApiString() }));
  }
}
