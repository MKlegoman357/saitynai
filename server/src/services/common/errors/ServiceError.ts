import { HttpStatus } from '@nestjs/common';

export class ServiceError extends Error {
  constructor(public errorCode: string, message?: string, public data: any | undefined = undefined, public responseCode: number = HttpStatus.BAD_REQUEST) {
    super(message);
  }
}
