import { ServiceError } from './ServiceError';
import { SchemaObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { ErrorResponse } from '../../../models/responses/ErrorResponse';
import { HttpStatus } from '@nestjs/common';
import { openApiObject, openApiString } from '../../../openapi/utils';

export class PermissionDeniedError extends ServiceError {
  constructor(public userId?: string) {
    super('permission-denied', 'user was not permitted access', { userId }, HttpStatus.FORBIDDEN);
  }

  static openApiResponse(): SchemaObject {
    return ErrorResponse.openApi('permission-denied', openApiObject({ userId: openApiString(undefined, true) }));
  }
}
