import { ServiceError } from './ServiceError';
import { SchemaObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { ErrorResponse } from '../../../models/responses/ErrorResponse';
import { openApiObject, openApiString } from '../../../openapi/utils';

export class CommentNotFoundError extends ServiceError {
  constructor(public commentId: string) {
    super('comment-not-found', `Comment with id ${commentId} was not found`, { commentId });
  }

  static openApiResponse(): SchemaObject {
    return ErrorResponse.openApi('comment-not-found', openApiObject({ commentId: openApiString() }));
  }
}
