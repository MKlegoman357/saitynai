import { ServiceError } from './ServiceError';
import { SchemaObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { ErrorResponse } from '../../../models/responses/ErrorResponse';
import { openApiObject, openApiString } from '../../../openapi/utils';

export class LabelAlreadyExistsError extends ServiceError {
  constructor(public name: string) {
    super('label-already-exists', `Label with name '${name}' already exists`, { name});
  }

  static openApiResponse(): SchemaObject {
    return ErrorResponse.openApi('label-already-exists', openApiObject({ name: openApiString() }));
  }
}
