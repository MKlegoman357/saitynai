export function getAllByIds<T>(ids: string[], get: (id: string) => Promise<T | null>, allowNulls?: true): Promise<(T | null)[]>;
export function getAllByIds<T>(ids: string[], get: (id: string) => Promise<T | null>, onNull: (id: string) => Error): Promise<T[]>;
export function getAllByIds<T>(ids: string[], get: (id: string) => Promise<T | null>, allowNullsOrOnNull: true | ((id: string) => Error)): Promise<(T | null)[]>;
export function getAllByIds<T>(ids: string[], get: (id: string) => Promise<T | null>, allowNullsOrOnNull: true | ((id: string) => Error) = true): Promise<(T | null)[]> {
  return Promise.all(ids.map(id => get(id).then(
    entity => {
      if (!entity && allowNullsOrOnNull !== true) {
        throw allowNullsOrOnNull(id);
      }

      return entity;
    }
  )));
}
