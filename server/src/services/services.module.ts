import { Module } from '@nestjs/common';
import { UserService } from './user/user.service';
import { ProjectService } from './project/project.service';
import { LabelService } from './project/label/label.service';
import { MemberService } from './project/member/member.service';
import { IssueService } from './project/issue/issue.service';
import { CommentService } from './project/issue/comment/comment.service';
import { DatabaseModule } from '../database/database.module';
import { AuthService } from './auth/auth.service';
import { JwtModule } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { JwtExpiresConfigKey, JwtSecretConfigKey } from '../config/config';
import { IssueEventService } from './project/issue/issue-event/issue-event.service';

@Module({
  imports: [
    DatabaseModule,
    JwtModule.registerAsync({
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => {
        const secret = configService.get<string>(JwtSecretConfigKey);
        const expires = configService.get<string>(JwtExpiresConfigKey);

        return { secret, signOptions: { expiresIn: expires } };
      }
    })
  ],
  providers: [
    UserService,
    ProjectService,
    LabelService,
    MemberService,
    IssueService,
    CommentService,
    AuthService,
    IssueEventService
  ],
  exports: [
    UserService,
    ProjectService,
    LabelService,
    MemberService,
    IssueService,
    CommentService,
    AuthService,
    IssueEventService
  ]
})
export class ServicesModule {}
