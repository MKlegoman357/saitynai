import { Injectable, ValidationPipe, ValidationPipeOptions } from '@nestjs/common';
import { ServiceError } from '../services/common/errors/ServiceError';
import { ValidationError } from 'class-validator';

function mapErrorsToErrorData(errors: ValidationError[]): Record<string, Record<string, string>> | undefined {
  if (process.env.NODE_ENV === 'production') return undefined;

  const data: Record<string, Record<string, string>> = {};

  for (const error of errors) {
    if (error.constraints) {
      data[error.property] = error.constraints;
    }
  }

  return data;
}

@Injectable()
export class BodyValidationPipe extends ValidationPipe {
  constructor(options?: ValidationPipeOptions) {
    super({
      transform: true,
      whitelist: true,
      forbidNonWhitelisted: true,
      enableDebugMessages: process.env.NODE_ENV === 'production',
      disableErrorMessages: process.env.NODE_ENV === 'production',
      exceptionFactory: errors => {
        throw new ServiceError('bad-request', 'Bad request', mapErrorsToErrorData(errors));
      },
      ...options
    });
  }
}
