import { BodyValidationPipe } from './body-validation.pipe';

describe('BodyValidationPipe', () => {
  it('should be defined', () => {
    expect(new BodyValidationPipe()).toBeDefined();
  });
});
