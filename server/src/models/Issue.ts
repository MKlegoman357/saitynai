import { IssueStatus } from './IssueStatus';
import { UserInfo, UserInfoSchema } from './UserInfo';
import { Comment, CommentSchema } from './Comment';
import { LabelInfo, LabelInfoSchema } from './LabelInfo';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema } from 'mongoose';
import { getEnumValues } from '../utils/enum';
import { IssueEvent, IssueEventSchema, mapIssueEventDiscriminators } from './IssueEvent';

@Schema()
export class Issue {
  @Prop({ required: true }) id: string;
  @Prop({ required: true }) title: string;
  @Prop({ required: true }) body: string;
  @Prop({ required: true, enum: getEnumValues(IssueStatus) }) status: IssueStatus;
  @Prop({ required: true }) dateCreated: Date;
  @Prop({ required: true }) dateModified: Date;

  @Prop({ required: true, type: UserInfoSchema }) author: UserInfo;
  @Prop({ required: true, type: [UserInfoSchema] }) assignedUsers: UserInfo[];
  @Prop({ required: true, type: [LabelInfoSchema] }) labels: LabelInfo[];

  @Prop({ required: true, type: [CommentSchema] }) comments: Comment[];
  @Prop({ required: true, type: [IssueEventSchema] }) events: IssueEvent[];

  constructor(id: string, title: string, body: string, status: IssueStatus, dateCreated: Date, dateModified: Date, author: UserInfo, assignedUsers: UserInfo[], labels: LabelInfo[], comments: Comment[], events: IssueEvent[]) {
    this.id = id;
    this.title = title;
    this.body = body;
    this.status = status;
    this.dateCreated = dateCreated;
    this.dateModified = dateModified;
    this.author = author;
    this.assignedUsers = assignedUsers;
    this.labels = labels;
    this.comments = comments;
    this.events = events;
  }
}

export type IssueDocument = Issue & Document;
export const IssueSchema = SchemaFactory.createForClass(Issue);

mapIssueEventDiscriminators(IssueSchema.path('events') as MongooseSchema.Types.DocumentArray);
