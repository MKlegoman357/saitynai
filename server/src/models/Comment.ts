import { UserInfo, UserInfoSchema } from './UserInfo';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type CommentDocument = Comment & Document;

@Schema()
export class Comment {
  @Prop({ required: true }) id: string;
  @Prop({ required: true }) body: string;
  @Prop({ required: true, type: UserInfoSchema }) author: UserInfo;
  @Prop({ required: true }) dateCreated: Date;
  @Prop({ required: true }) dateModified: Date;

  constructor(id: string, body: string, author: UserInfo, dateCreated: Date, dateModified: Date) {
    this.id = id;
    this.body = body;
    this.author = author;
    this.dateCreated = dateCreated;
    this.dateModified = dateModified;
  }
}

export const CommentSchema = SchemaFactory.createForClass(Comment);
