import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Label } from './Label';

export type LabelInfoDocument = LabelInfo & Document;

@Schema()
export class LabelInfo {
  @Prop({ required: true }) name: string;

  constructor(name: string) {
    this.name = name;
  }
}

export const LabelInfoSchema = SchemaFactory.createForClass(LabelInfo);

export function mapLabelToLabelInfo(label: Label): LabelInfo {
  return new LabelInfo(label.name);
}
