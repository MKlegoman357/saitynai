import { Role } from './Role';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { getEnumValues } from '../utils/enum';

export type UserDocument = User & Document;

@Schema()
export class User {
  @Prop({ required: true }) id: string;
  @Prop({ required: true }) email: string;
  @Prop({ required: true }) username: string;
  @Prop({ required: true }) password: string;
  @Prop({ required: true, enum: getEnumValues(Role) }) role: Role;

  constructor(id: string, email: string, username: string, password: string, role: Role) {
    this.id = id;
    this.email = email;
    this.username = username;
    this.password = password;
    this.role = role;
  }
}

export const UserSchema = SchemaFactory.createForClass(User);
