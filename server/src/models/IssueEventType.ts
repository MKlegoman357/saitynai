import { SchemaObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { openApiEnum } from '../openapi/utils';

export enum IssueEventType {
  Title = 'title',
  Body = 'body',
  Status = 'status',
  AssignedUsers = 'assigned-users',
  Labels = 'labels'
}

export function openApiIssueEventType(): SchemaObject {
  return openApiEnum('IssueEventType', IssueEventType);
}
