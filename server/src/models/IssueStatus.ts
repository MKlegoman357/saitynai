import { SchemaObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { openApiEnum } from '../openapi/utils';

export enum IssueStatus {
  Open = 'open',
  Closed = 'closed'
}

export function openApiIssueStatus(): SchemaObject {
  return openApiEnum('IssueStatus', IssueStatus);
}
