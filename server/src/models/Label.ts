import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type LabelDocument = Label & Document;

@Schema()
export class Label {
  @Prop({ required: true }) name: string;

  constructor(name: string) {
    this.name = name;
  }
}

export const LabelSchema = SchemaFactory.createForClass(Label);
