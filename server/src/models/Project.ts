import { ProjectVisibility } from './ProjectVisibility';
import { UserInfo, UserInfoSchema } from './UserInfo';
import { Issue, IssueSchema } from './Issue';
import { Label, LabelSchema } from './Label';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { getEnumValues } from '../utils/enum';
import { Document } from 'mongoose';

export type ProjectDocument = Project & Document;

@Schema()
export class Project {
  @Prop({ required: true }) id: string;
  @Prop({ required: true }) name: string;
  @Prop({ required: true, enum: getEnumValues(ProjectVisibility) }) visibility: ProjectVisibility;
  @Prop({ required: true }) dateCreated: Date;

  @Prop({ required: true, type: [UserInfoSchema] }) administrators: UserInfo[];
  @Prop({ required: true, type: [UserInfoSchema] }) otherMembers: UserInfo[];

  @Prop({ required: true, type: [IssueSchema] }) issues: Issue[];
  @Prop({ required: true, type: [LabelSchema] }) labels: Label[];

  constructor(id: string, name: string, visibility: ProjectVisibility, dateCreated: Date, administrators: UserInfo[], otherMembers: UserInfo[], issues: Issue[], labels: Label[]) {
    this.id = id;
    this.name = name;
    this.visibility = visibility;
    this.dateCreated = dateCreated;
    this.administrators = administrators;
    this.otherMembers = otherMembers;
    this.issues = issues;
    this.labels = labels;
  }
}

export const ProjectSchema = SchemaFactory.createForClass(Project);
