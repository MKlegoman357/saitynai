import { IssueEventType } from './IssueEventType';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { getEnumValues } from '../utils/enum';
import { AcceptsDiscriminator, Document } from 'mongoose';
import { IssueStatus } from './IssueStatus';
import { UserInfo, UserInfoSchema } from './UserInfo';
import { LabelInfo, LabelInfoSchema } from './LabelInfo';

export abstract class IssueEvent<TEvent extends IssueEventType = any> {
  type: TEvent;
  timestamp: Date;
  author?: UserInfo;

  protected constructor(type: TEvent, timestamp: Date, author?: UserInfo) {
    this.type = type;
    this.timestamp = timestamp;
    this.author = author;
  }
}

@Schema({ discriminatorKey: 'type' })
class IssueEventSchemaClass<TEvent extends IssueEventType = any> extends IssueEvent<TEvent> {
  @Prop({ required: true, enum: getEnumValues(IssueEventType) }) type!: TEvent;
  @Prop({ required: true }) timestamp!: Date;
  @Prop({ required: false, type: UserInfoSchema }) author?: UserInfo;
}

export type IssueEventDocument<TEvent extends IssueEventType> = IssueEvent<TEvent> & Document;
export const IssueEventSchema = SchemaFactory.createForClass(IssueEventSchemaClass as { new(...args: any[]): IssueEvent });

@Schema()
export class TitleIssueEvent extends IssueEvent<IssueEventType.Title> {
  constructor(timestamp: Date, author?: UserInfo) {
    super(IssueEventType.Title, timestamp, author);
  }
}

export type TitleIssueEventDocument = TitleIssueEvent & Document;
export const TitleIssueEventSchema = SchemaFactory.createForClass(TitleIssueEvent);

@Schema()
export class BodyIssueEvent extends IssueEvent<IssueEventType.Body> {
  constructor(timestamp: Date, author?: UserInfo) {
    super(IssueEventType.Body, timestamp, author);
  }
}

export type BodyIssueEventDocument = BodyIssueEvent & Document;
export const BodyIssueEventSchema = SchemaFactory.createForClass(BodyIssueEvent);

@Schema()
export class StatusIssueEvent extends IssueEvent<IssueEventType.Status> {
  @Prop({ required: true, enum: getEnumValues(IssueStatus) }) status: IssueStatus;

  constructor(timestamp: Date, status: IssueStatus, author?: UserInfo) {
    super(IssueEventType.Status, timestamp, author);
    this.status = status;
  }
}

export type StatusIssueEventDocument = StatusIssueEvent & Document;
export const StatusIssueEventSchema = SchemaFactory.createForClass(StatusIssueEvent);

@Schema()
export class AssignedUsersIssueEvent extends IssueEvent<IssueEventType.AssignedUsers> {
  @Prop({ required: true, type: [UserInfoSchema] }) assignedUsers: UserInfo[];

  constructor(timestamp: Date, assignedUsers: UserInfo[], author?: UserInfo) {
    super(IssueEventType.AssignedUsers, timestamp, author);
    this.assignedUsers = assignedUsers;
  }
}

export type AssignedUsersIssueEventDocument = AssignedUsersIssueEvent & Document;
export const AssignedUsersIssueEventSchema = SchemaFactory.createForClass(AssignedUsersIssueEvent);

@Schema()
export class LabelsIssueEvent extends IssueEvent<IssueEventType.Labels> {
  @Prop({ required: true, type: [LabelInfoSchema] }) labels: LabelInfo[];

  constructor(timestamp: Date, labels: LabelInfo[], author?: UserInfo) {
    super(IssueEventType.Labels, timestamp, author);
    this.labels = labels;
  }
}

export type LabelsIssueEventDocument = LabelsIssueEvent & Document;
export const LabelsIssueEventSchema = SchemaFactory.createForClass(LabelsIssueEvent);

export function mapIssueEventDiscriminators(issueEventPath: AcceptsDiscriminator): void {
  issueEventPath.discriminator(IssueEventType.Title, TitleIssueEventSchema);
  issueEventPath.discriminator(IssueEventType.Body, BodyIssueEventSchema);
  issueEventPath.discriminator(IssueEventType.Status, StatusIssueEventSchema);
  issueEventPath.discriminator(IssueEventType.AssignedUsers, AssignedUsersIssueEventSchema);
  issueEventPath.discriminator(IssueEventType.Labels, LabelsIssueEventSchema);
}
