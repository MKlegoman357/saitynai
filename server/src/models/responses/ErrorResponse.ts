import { Response } from './Response';
import { ApiProperty } from '@nestjs/swagger';
import { SchemaObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { openApiString } from '../../openapi/utils';

export class ErrorResponse extends Response {
  @ApiProperty() errorCode: string;
  @ApiProperty() errorData?: any;

  constructor(errorCode: string, errorData?: any) {
    super(false);

    this.errorData = errorData;
    this.errorCode = errorCode;
  }

  static openApi(errorCode: string, errorData?: SchemaObject): SchemaObject {
    const extraProperties: Record<string, SchemaObject> = {};

    if (errorData) {
      extraProperties.errorData = errorData;
    }

    return Response.openApiResponse(false, {
      errorCode: openApiString(errorCode),
      ...extraProperties
    });
  }
}
