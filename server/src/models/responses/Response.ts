import { ApiProperty } from '@nestjs/swagger';
import { SchemaObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { openApiBoolean, openApiObject } from '../../openapi/utils';

export abstract class Response {
  @ApiProperty() ok: boolean;

  protected constructor(ok: boolean) {
    this.ok = ok;
  }

  static openApiResponse(okValue: boolean, extraProperties?: Record<string, SchemaObject>): SchemaObject {
    return openApiObject({
      ok: openApiBoolean(okValue),
      ...extraProperties
    }, 'Response');
  }
}
