import { Response } from './Response';
import { ApiProperty } from '@nestjs/swagger';
import { SchemaObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';

export class SuccessResponse<T = undefined> extends Response {
  @ApiProperty() data: T;

  constructor();
  constructor(data: T);
  constructor(data?: T) {
    super(true);
    this.data = data as T;
  }

  static openApi(data?: SchemaObject): SchemaObject {
    const extraProperties: Record<string, SchemaObject> = {};

    if (data) {
      extraProperties.data = data;
    }

    return Response.openApiResponse(true, extraProperties);
  }
}
