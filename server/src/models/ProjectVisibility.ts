import { SchemaObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { openApiEnum } from '../openapi/utils';

export enum ProjectVisibility {
  Public = 'public',
  Private = 'private'
}

export function openApiProjectVisibility(): SchemaObject {
  return openApiEnum('ProjectVisibility', ProjectVisibility);
}
