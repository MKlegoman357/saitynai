import { SchemaObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { openApiEnum } from '../openapi/utils';

export enum Role {
  User = 'user',
  Admin = 'admin'
}

export function openApiRole(): SchemaObject {
  return openApiEnum('Role', Role);
}
