import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { User } from './User';

export type UserInfoDocument = UserInfo & Document;

@Schema()
export class UserInfo {
  @Prop({ required: true }) id: string;
  @Prop({ required: true }) username: string;

  constructor(id: string, username: string) {
    this.id = id;
    this.username = username;
  }
}

export const UserInfoSchema = SchemaFactory.createForClass(UserInfo);

export function mapUserToUserInfo(user: User): UserInfo {
  return new UserInfo(user.id, user.username);
}
