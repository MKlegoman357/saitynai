import { Module } from '@nestjs/common';
import { UsersModule } from './users/users.module';
import { ProjectsModule } from './projects/projects.module';
import { ProjectService } from './services/project/project.service';
import { UserService } from './services/user/user.service';
import { ServicesModule } from './services/services.module';
import { DatabaseModule } from './database/database.module';
import { ConfigModule } from '@nestjs/config';
import { AuthModule } from './auth/auth.module';
import { TestingDatabaseModule } from './common/tests/testing-database.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: ['.env.local', '.env'],
      isGlobal: true
    }),
    DatabaseModule,
    ServicesModule,
    UsersModule,
    ProjectsModule,
    AuthModule,
    TestingDatabaseModule
  ],
  providers: [ProjectService, UserService]
})
export class AppModule {}
