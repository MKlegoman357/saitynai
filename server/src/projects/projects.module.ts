import { Module } from '@nestjs/common';
import { ProjectController } from './project/project.controller';
import { ServicesModule } from '../services/services.module';
import { LabelController } from './project/label/label.controller';
import { MemberController } from './project/member/member.controller';
import { IssueController } from './project/issue/issue.controller';
import { CommentController } from './project/issue/comment/comment.controller';
import { IssueEventController } from './project/issue/issue-event/issue-event.controller';

@Module({
  imports: [ServicesModule],
  controllers: [ProjectController, LabelController, MemberController, IssueController, CommentController, IssueEventController]
})
export class ProjectsModule {}
