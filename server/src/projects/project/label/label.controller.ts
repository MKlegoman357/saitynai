import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Patch,
  Post,
  UseFilters,
  UseGuards,
  UsePipes
} from '@nestjs/common';
import { Response } from '../../../models/responses/Response';
import { LabelDto, mapLabelToLabelDto } from '../../../dtos/LabelDto';
import { SuccessResponse } from '../../../models/responses/SuccessResponse';
import { LabelService } from '../../../services/project/label/label.service';
import { ServiceErrorFilter } from '../../../services/filters/service-error.filter';
import {
  ApiBadRequestResponse,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiProperty,
  ApiTags
} from '@nestjs/swagger';
import { openApiOneOf, openApiArrayOf } from '../../../openapi/utils';
import { ProjectNotFoundError } from '../../../services/common/errors/ProjectNotFoundError';
import { LabelNotFoundError } from '../../../services/common/errors/LabelNotFoundError';
import { LabelAlreadyExistsError } from '../../../services/common/errors/LabelAlreadyExistsError';
import { JwtGuard } from '../../../auth/guards/jwt.guard';
import { IsString } from 'class-validator';
import { BodyValidationPipe } from '../../../validation/body-validation.pipe';
import { CurrentUser } from '../../../auth/decorators/current-user.decorator';
import { IAuthenticatedUser } from '../../../services/auth/auth.service';
import { AuthenticatedGuard } from '../../../auth/guards/authenticated.guard';

class UpdateLabelRequest {
  @ApiProperty()
  @IsString()
  name!: string;
}

@ApiTags('labels')
@Controller('project/:projectId/label')
@UseFilters(new ServiceErrorFilter())
@UsePipes(new BodyValidationPipe())
@UseGuards(JwtGuard)
export class LabelController {

  constructor(private labelService: LabelService) {}

  @Get()
  @HttpCode(HttpStatus.OK)
  @UseFilters(new ServiceErrorFilter([{ code: 404, errors: [ProjectNotFoundError] }]))
  @ApiOperation({ summary: 'Gets all labels' })
  @ApiOkResponse({ schema: SuccessResponse.openApi(openApiArrayOf(LabelDto.openApi())) })
  @ApiNotFoundResponse({ schema: ProjectNotFoundError.openApiResponse() })
  async getAllLabels(@CurrentUser() currentUser: IAuthenticatedUser | null, @Param('projectId') projectId: string): Promise<Response> {
    const labels = await this.labelService.getAll(projectId, currentUser);

    return new SuccessResponse(labels.map(label => mapLabelToLabelDto(label)));
  }

  @Get(':name')
  @HttpCode(HttpStatus.OK)
  @UseFilters(new ServiceErrorFilter([{ code: 404, errors: [ProjectNotFoundError, LabelNotFoundError] }]))
  @ApiOperation({ summary: 'Gets a single label' })
  @ApiOkResponse({ schema: SuccessResponse.openApi(LabelDto.openApi()) })
  @ApiNotFoundResponse({ schema: openApiOneOf(ProjectNotFoundError.openApiResponse(), LabelNotFoundError.openApiResponse()) })
  async getLabel(@CurrentUser() currentUser: IAuthenticatedUser | null, @Param('projectId') projectId: string, @Param('name') name: string): Promise<Response> {
    const label = await this.labelService.get(projectId, name, currentUser);

    if (!label) {
      throw new LabelNotFoundError(name);
    }

    return new SuccessResponse(mapLabelToLabelDto(label));
  }

  @Post(':name')
  @HttpCode(HttpStatus.CREATED)
  @UseFilters(new ServiceErrorFilter([{ code: 404, errors: [ProjectNotFoundError] }]))
  @UseGuards(new AuthenticatedGuard())
  @ApiOperation({ summary: 'Adds a label' })
  @ApiCreatedResponse({ schema: SuccessResponse.openApi(LabelDto.openApi()) })
  @ApiNotFoundResponse({ schema: ProjectNotFoundError.openApiResponse() })
  @ApiBadRequestResponse({ schema: LabelAlreadyExistsError.openApiResponse() })
  async addLabel(@CurrentUser() currentUser: IAuthenticatedUser | null, @Param('projectId') projectId: string, @Param('name') name: string): Promise<Response> {
    const label = await this.labelService.add(projectId, name, currentUser);

    return new SuccessResponse(mapLabelToLabelDto(label));
  }

  @Patch(':name')
  @HttpCode(HttpStatus.OK)
  @UseFilters(new ServiceErrorFilter([{ code: 404, errors: [ProjectNotFoundError, LabelNotFoundError] }]))
  @UseGuards(new AuthenticatedGuard())
  @ApiOperation({ summary: 'Updates a label\'s name' })
  @ApiOkResponse({ schema: SuccessResponse.openApi() })
  @ApiNotFoundResponse({ schema: openApiOneOf(ProjectNotFoundError.openApiResponse(), LabelNotFoundError.openApiResponse()) })
  async updateLabel(@CurrentUser() currentUser: IAuthenticatedUser | null, @Param('projectId') projectId: string, @Param('name') name: string, @Body() body: UpdateLabelRequest): Promise<Response> {
    await this.labelService.update(projectId, name, body.name, currentUser);

    return new SuccessResponse();
  }

  @Delete(':name')
  @HttpCode(HttpStatus.OK)
  @UseFilters(new ServiceErrorFilter([{ code: 404, errors: [ProjectNotFoundError, LabelNotFoundError] }]))
  @UseGuards(new AuthenticatedGuard())
  @ApiOperation({ summary: 'Removes a label' })
  @ApiOkResponse({ schema: SuccessResponse.openApi() })
  @ApiNotFoundResponse({ schema: openApiOneOf(ProjectNotFoundError.openApiResponse(), LabelNotFoundError.openApiResponse()) })
  async removeLabel(@CurrentUser() currentUser: IAuthenticatedUser | null, @Param('projectId') projectId: string, @Param('name') name: string): Promise<Response> {
    await this.labelService.remove(projectId, name, currentUser);

    return new SuccessResponse();
  }

  @Delete('')
  @HttpCode(HttpStatus.OK)
  @UseFilters(new ServiceErrorFilter([{ code: 404, errors: [ProjectNotFoundError] }]))
  @UseGuards(new AuthenticatedGuard())
  @ApiOperation({ summary: 'Removes all labels' })
  @ApiOkResponse({ schema: SuccessResponse.openApi() })
  @ApiNotFoundResponse({ schema: ProjectNotFoundError.openApiResponse() })
  async removeAllLabels(@CurrentUser() currentUser: IAuthenticatedUser | null, @Param('projectId') projectId: string): Promise<Response> {
    await this.labelService.removeAll(projectId, currentUser);

    return new SuccessResponse();
  }

}
