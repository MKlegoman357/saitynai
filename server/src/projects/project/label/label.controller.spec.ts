import { Test, TestingModule } from '@nestjs/testing';
import { LabelController } from './label.controller';
import { LabelService } from '../../../services/project/label/label.service';

describe('LabelController', () => {
  let controller: LabelController;

  const labelServiceMock = {};

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        LabelController,
        LabelService
      ]
    }).overrideProvider(LabelService).useValue(labelServiceMock)
      .compile();

    controller = module.get(LabelController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
