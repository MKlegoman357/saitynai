import { Test, TestingModule } from '@nestjs/testing';
import { ProjectController } from './project.controller';
import { ProjectService } from '../../services/project/project.service';

describe('ProjectController', () => {
  let controller: ProjectController;

  const projectServiceMock = {};

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ProjectController,
        ProjectService
      ]
    }).overrideProvider(ProjectService).useValue(projectServiceMock)
      .compile();

    controller = module.get(ProjectController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
