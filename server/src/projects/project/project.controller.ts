import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Patch,
  Post,
  Query,
  UseFilters,
  UseGuards,
  UsePipes
} from '@nestjs/common';
import { Response } from '../../models/responses/Response';
import { INewProject, IProjectsFilter, IProjectUpdates, ProjectService } from '../../services/project/project.service';
import { SuccessResponse } from '../../models/responses/SuccessResponse';
import { mapProjectToProjectDto, ProjectDto } from '../../dtos/ProjectDto';
import { ProjectVisibility } from '../../models/ProjectVisibility';
import { ServiceErrorFilter } from '../../services/filters/service-error.filter';
import {
  ApiBadRequestResponse,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiProperty,
  ApiPropertyOptional,
  ApiTags
} from '@nestjs/swagger';
import { openApiOneOf, openApiArrayOf } from '../../openapi/utils';
import { ProjectNotFoundError } from '../../services/common/errors/ProjectNotFoundError';
import { ProjectIdTakenError } from '../../services/common/errors/ProjectIdTakenError';
import { NoProjectAdministratorError } from '../../services/common/errors/NoProjectAdministratorError';
import { UserNotFoundError } from '../../services/common/errors/UserNotFoundError';
import { JwtGuard } from '../../auth/guards/jwt.guard';
import { IsEnum, IsOptional, IsString } from 'class-validator';
import { BodyValidationPipe } from '../../validation/body-validation.pipe';
import { IAuthenticatedUser } from '../../services/auth/auth.service';
import { CurrentUser } from '../../auth/decorators/current-user.decorator';
import { AuthenticatedGuard } from '../../auth/guards/authenticated.guard';

class GetAllProjectsQuery implements IProjectsFilter {
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  userId?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  query?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsEnum(ProjectVisibility)
  visibility?: ProjectVisibility;
}

class CreateProjectRequest implements INewProject {
  @ApiProperty()
  @IsString()
  name!: string;

  @ApiProperty({ enum: ProjectVisibility, enumName: 'ProjectVisibility' })
  @IsEnum(ProjectVisibility)
  visibility!: ProjectVisibility;

  @ApiProperty({ type: [String] })
  @IsString({ each: true })
  administrators!: string[];

  @ApiProperty({ type: [String] })
  @IsString({ each: true })
  otherMembers!: string[];
}

class UpdateProjectRequest implements Partial<IProjectUpdates> {
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  name?: string;

  @ApiPropertyOptional({ enum: ProjectVisibility, enumName: 'ProjectVisibility' })
  @IsOptional()
  @IsEnum(ProjectVisibility)
  visibility?: ProjectVisibility;
}

@ApiTags('projects')
@Controller('project')
@UseFilters(new ServiceErrorFilter())
@UsePipes(new BodyValidationPipe())
@UseGuards(JwtGuard)
export class ProjectController {

  constructor(private projectService: ProjectService) {}

  @Get()
  @HttpCode(HttpStatus.OK)
  @ApiOperation({ summary: 'Gets all projects' })
  @ApiOkResponse({ schema: SuccessResponse.openApi(openApiArrayOf(ProjectDto.openApi())) })
  async getAllProjects(@CurrentUser() currentUser: IAuthenticatedUser | null, @Query() query: GetAllProjectsQuery): Promise<Response> {
    const projects = await this.projectService.getAll(currentUser, undefined, query);

    return new SuccessResponse(projects.map(project => mapProjectToProjectDto(project)));
  }

  @Get(':projectId')
  @HttpCode(HttpStatus.OK)
  @UseFilters(new ServiceErrorFilter([{ code: 404, errors: [ProjectNotFoundError] }]))
  @ApiOperation({ summary: 'Gets a single project' })
  @ApiOkResponse({ schema: SuccessResponse.openApi(ProjectDto.openApi()) })
  @ApiNotFoundResponse({ schema: ProjectNotFoundError.openApiResponse() })
  async getProject(@CurrentUser() currentUser: IAuthenticatedUser | null, @Param('projectId') projectId: string): Promise<Response> {
    const project = await this.projectService.get(projectId, currentUser);

    if (!project) {
      throw new ProjectNotFoundError(projectId);
    }

    return new SuccessResponse(mapProjectToProjectDto(project));
  }

  @Post(':projectId')
  @HttpCode(HttpStatus.CREATED)
  @UseGuards(new AuthenticatedGuard())
  @ApiOperation({ summary: 'Creates a project' })
  @ApiCreatedResponse({ schema: SuccessResponse.openApi(ProjectDto.openApi()) })
  @ApiBadRequestResponse({ schema: openApiOneOf(ProjectIdTakenError.openApiResponse(), NoProjectAdministratorError.openApiResponse(), UserNotFoundError.openApiResponse()) })
  async createProject(@Param('projectId') projectId: string, @Body() body: CreateProjectRequest): Promise<Response> {
    const project = await this.projectService.create(projectId, body);

    return new SuccessResponse(mapProjectToProjectDto(project));
  }

  @Patch(':projectId')
  @HttpCode(HttpStatus.OK)
  @UseFilters(new ServiceErrorFilter([{ code: 404, errors: [ProjectNotFoundError] }]))
  @UseGuards(new AuthenticatedGuard())
  @ApiOperation({ summary: 'Updates a project' })
  @ApiOkResponse({ schema: SuccessResponse.openApi() })
  @ApiNotFoundResponse({ schema: ProjectNotFoundError.openApiResponse() })
  async updateProject(@CurrentUser() currentUser: IAuthenticatedUser | null, @Param('projectId') projectId: string, @Body() body: UpdateProjectRequest): Promise<Response> {
    await this.projectService.update(projectId, body, currentUser);

    return new SuccessResponse();
  }

  @Delete(':projectId')
  @HttpCode(HttpStatus.OK)
  @UseFilters(new ServiceErrorFilter([{ code: 404, errors: [ProjectNotFoundError] }]))
  @UseGuards(new AuthenticatedGuard())
  @ApiOperation({ summary: 'Deletes a project' })
  @ApiOkResponse({ schema: SuccessResponse.openApi() })
  @ApiNotFoundResponse({ schema: ProjectNotFoundError.openApiResponse() })
  async deleteProject(@CurrentUser() currentUser: IAuthenticatedUser | null, @Param('projectId') projectId: string): Promise<Response> {
    await this.projectService.delete(projectId, currentUser);

    return new SuccessResponse();
  }

}
