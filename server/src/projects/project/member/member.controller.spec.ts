import { Test, TestingModule } from '@nestjs/testing';
import { MemberController } from './member.controller';
import { MemberService } from '../../../services/project/member/member.service';

describe('MemberController', () => {
  let controller: MemberController;

  const memberServiceMock = {};

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        MemberController,
        MemberService
      ]
    }).overrideProvider(MemberService).useValue(memberServiceMock)
      .compile();

    controller = module.get(MemberController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
