import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Patch,
  Post,
  UseFilters,
  UseGuards,
  UsePipes
} from '@nestjs/common';
import { ServiceErrorFilter } from '../../../services/filters/service-error.filter';
import { Response } from '../../../models/responses/Response';
import { MemberService } from '../../../services/project/member/member.service';
import { SuccessResponse } from '../../../models/responses/SuccessResponse';
import { mapProjectMemberToProjectMemberDto, ProjectMemberDto } from '../../../dtos/ProjectMemberDto';
import {
  ApiBadRequestResponse,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiProperty,
  ApiTags
} from '@nestjs/swagger';
import { openApiOneOf, openApiArrayOf } from '../../../openapi/utils';
import { ProjectNotFoundError } from '../../../services/common/errors/ProjectNotFoundError';
import { ProjectMemberNotFoundError } from '../../../services/common/errors/ProjectMemberNotFoundError';
import { UserAlreadyAMemberError } from '../../../services/common/errors/UserAlreadyAMemberError';
import { NoProjectAdministratorError } from '../../../services/common/errors/NoProjectAdministratorError';
import { UserNotFoundError } from '../../../services/common/errors/UserNotFoundError';
import { JwtGuard } from '../../../auth/guards/jwt.guard';
import { IsBoolean } from 'class-validator';
import { BodyValidationPipe } from '../../../validation/body-validation.pipe';
import { CurrentUser } from '../../../auth/decorators/current-user.decorator';
import { IAuthenticatedUser } from '../../../services/auth/auth.service';
import { AuthenticatedGuard } from '../../../auth/guards/authenticated.guard';

class AddMemberRequest {
  @ApiProperty()
  @IsBoolean()
  isAdministrator!: boolean;
}

class UpdateMemberRequest {
  @ApiProperty()
  @IsBoolean()
  isAdministrator!: boolean;
}

@ApiTags('members')
@Controller('project/:projectId/member')
@UseFilters(new ServiceErrorFilter())
@UsePipes(new BodyValidationPipe())
@UseGuards(JwtGuard)
export class MemberController {

  constructor(private memberService: MemberService) {}

  @Get()
  @HttpCode(HttpStatus.OK)
  @UseFilters(new ServiceErrorFilter([{ code: 404, errors: [ProjectNotFoundError] }]))
  @ApiOperation({ summary: 'Gets all members' })
  @ApiOkResponse({ schema: SuccessResponse.openApi(openApiArrayOf(ProjectMemberDto.openApi())) })
  @ApiNotFoundResponse({ schema: ProjectNotFoundError.openApiResponse() })
  async getAllMembers(@CurrentUser() currentUser: IAuthenticatedUser | null, @Param('projectId') projectId: string): Promise<Response> {
    const members = await this.memberService.getAll(projectId, currentUser);

    return new SuccessResponse(members.map(member => mapProjectMemberToProjectMemberDto(member)));
  }

  @Get(':userId')
  @HttpCode(HttpStatus.OK)
  @UseFilters(new ServiceErrorFilter([{ code: 404, errors: [ProjectNotFoundError, ProjectMemberNotFoundError] }]))
  @ApiOperation({ summary: 'Gets a single member' })
  @ApiOkResponse({ schema: SuccessResponse.openApi(ProjectMemberDto.openApi()) })
  @ApiNotFoundResponse({ schema: openApiOneOf(ProjectNotFoundError.openApiResponse(), ProjectMemberNotFoundError.openApiResponse()) })
  async getMember(@CurrentUser() currentUser: IAuthenticatedUser | null, @Param('projectId') projectId: string, @Param('userId') userId: string): Promise<Response> {
    const member = await this.memberService.get(projectId, userId, currentUser);

    if (!member) {
      throw new ProjectMemberNotFoundError(userId);
    }

    return new SuccessResponse(mapProjectMemberToProjectMemberDto(member));
  }

  @Post(':userId')
  @HttpCode(HttpStatus.CREATED)
  @UseFilters(new ServiceErrorFilter([{ code: 404, errors: [ProjectNotFoundError] }]))
  @UseGuards(new AuthenticatedGuard())
  @ApiOperation({ summary: 'Adds a member' })
  @ApiCreatedResponse({ schema: SuccessResponse.openApi(ProjectMemberDto.openApi()) })
  @ApiNotFoundResponse({ schema: ProjectNotFoundError.openApiResponse() })
  @ApiBadRequestResponse({ schema: openApiOneOf(UserAlreadyAMemberError.openApiResponse(), UserNotFoundError.openApiResponse()) })
  async addMember(@CurrentUser() currentUser: IAuthenticatedUser | null, @Param('projectId') projectId: string, @Param('userId') userId: string, @Body() body: AddMemberRequest): Promise<Response> {
    const member = await this.memberService.add(projectId, userId, body.isAdministrator, currentUser);

    return new SuccessResponse(mapProjectMemberToProjectMemberDto(member));
  }

  @Patch(':userId')
  @HttpCode(HttpStatus.OK)
  @UseFilters(new ServiceErrorFilter([{ code: 404, errors: [ProjectNotFoundError, ProjectMemberNotFoundError] }]))
  @UseGuards(new AuthenticatedGuard())
  @ApiOperation({ summary: 'Changes member\'s administrator status' })
  @ApiOkResponse({ schema: SuccessResponse.openApi() })
  @ApiNotFoundResponse({ schema: openApiOneOf(ProjectNotFoundError.openApiResponse(), ProjectMemberNotFoundError.openApiResponse()) })
  async updateMember(@CurrentUser() currentUser: IAuthenticatedUser | null, @Param('projectId') projectId: string, @Param('userId') userId: string, @Body() body: UpdateMemberRequest): Promise<Response> {
    await this.memberService.update(projectId, userId, body.isAdministrator, currentUser);

    return new SuccessResponse();
  }

  @Delete(':userId')
  @HttpCode(HttpStatus.OK)
  @UseFilters(new ServiceErrorFilter([{ code: 404, errors: [ProjectNotFoundError, ProjectMemberNotFoundError] }]))
  @UseGuards(new AuthenticatedGuard())
  @ApiOperation({ summary: 'Removes a member' })
  @ApiOkResponse({ schema: SuccessResponse.openApi() })
  @ApiNotFoundResponse({ schema: openApiOneOf(ProjectNotFoundError.openApiResponse(), ProjectMemberNotFoundError.openApiResponse()) })
  @ApiBadRequestResponse({ schema: NoProjectAdministratorError.openApiResponse() })
  async removeMember(@CurrentUser() currentUser: IAuthenticatedUser | null, @Param('projectId') projectId: string, @Param('userId') userId: string): Promise<Response> {
    await this.memberService.remove(projectId, userId, currentUser);

    return new SuccessResponse();
  }

}
