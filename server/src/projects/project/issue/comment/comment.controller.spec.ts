import { Test, TestingModule } from '@nestjs/testing';
import { AddCommentRequest, CommentController, UpdateCommentRequest } from './comment.controller';
import { CommentService } from '../../../../services/project/issue/comment/comment.service';
import {
  CommentServiceMock,
  createCommentServiceMock
} from '../../../../services/project/issue/comment/comment.service.mock';
import { Comment } from '../../../../models/Comment';
import { UserInfo } from '../../../../models/UserInfo';
import { SuccessResponse } from '../../../../models/responses/SuccessResponse';
import { CommentDto } from '../../../../dtos/CommentDto';
import { UserInfoDto } from '../../../../dtos/UserInfoDto';
import { ProjectNotFoundError } from '../../../../services/common/errors/ProjectNotFoundError';
import { IssueNotFoundError } from '../../../../services/common/errors/IssueNotFoundError';
import { CommentNotFoundError } from '../../../../services/common/errors/CommentNotFoundError';
import { UserNotFoundError } from '../../../../services/common/errors/UserNotFoundError';
import { ServiceError } from '../../../../services/common/errors/ServiceError';

describe('CommentController', () => {
  let controller: CommentController;
  let commentServiceMock: CommentServiceMock;

  const date = new Date('2000-01-01 00:00');

  beforeEach(async () => {
    commentServiceMock = createCommentServiceMock();
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CommentController,
        CommentService
      ]
    }).overrideProvider(CommentService).useValue(commentServiceMock)
      .compile();

    controller = module.get(CommentController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('getAllComments', () => {
    it('should return all comments', async () => {
      const comments = [
        new Comment('id1', 'body1', new UserInfo('userId1', 'username1'), date, date),
        new Comment('id2', 'body2', new UserInfo('userId2', 'username2'), date, date)
      ];
      commentServiceMock.getAll.mockResolvedValue(comments);

      const result = await controller.getAllComments(null, 'projectId', 'issueId');

      expect(commentServiceMock.getAll).toHaveBeenCalledWith('projectId', 'issueId', null);
      expect(result).toMatchObject(new SuccessResponse([
        new CommentDto('id1', 'body1', new UserInfoDto('userId1', 'username1'), date, date),
        new CommentDto('id2', 'body2', new UserInfoDto('userId2', 'username2'), date, date)
      ]));
    });
  });

  describe('getComment', () => {
    it('should return a comment', async () => {
      const comment = new Comment('commentId', 'commentBody', new UserInfo('userId', 'username'), date, date);
      commentServiceMock.get.mockResolvedValue(comment);

      const result = await controller.getComment(null, 'projectId', 'issueId', 'commentId');

      expect(commentServiceMock.get).toHaveBeenCalledWith('projectId', 'issueId', 'commentId', null);
      expect(result).toMatchObject(
        new SuccessResponse(
          new CommentDto('commentId', 'commentBody', new UserInfoDto('userId', 'username'), date, date)
        )
      );
    });

    it('should throw CommentNotFound error', async () => {
      const error = new CommentNotFoundError('commentId');
      commentServiceMock.get.mockRejectedValue(error);

      const promise = controller.getComment(null, 'projectId', 'issueId', 'commentId');

      await expect(promise).rejects.toEqual(error);
      expect(commentServiceMock.get).toHaveBeenCalledWith('projectId', 'issueId', 'commentId', null);
    });
  });

  describe('addComment', () => {
    it('should return the added comment', async () => {
      const request = new AddCommentRequest();
      request.body = 'commentBody';
      request.author = 'userId';
      const comment = new Comment('commentId', 'commentBody', new UserInfo('userId', 'username'), date, date);
      commentServiceMock.add.mockResolvedValue(comment);

      const result = await controller.addComment(null, 'projectId', 'issueId', request);

      expect(commentServiceMock.add).toHaveBeenCalledWith('projectId', 'issueId', request, null, undefined);
      expect(result).toMatchObject(
        new SuccessResponse(
          new CommentDto('commentId', 'commentBody', new UserInfoDto('userId', 'username'), date, date)
        )
      );
    });
  });

  describe('updateComment', () => {
    it('should return successful response', async () => {
      const request = new UpdateCommentRequest();
      request.body = 'updatedBody';
      commentServiceMock.update.mockResolvedValue();

      const result = await controller.updateComment(null, 'projectId', 'issueId', 'commentId', request);

      expect(commentServiceMock.update).toHaveBeenCalledWith('projectId', 'issueId', 'commentId', request, null);
      expect(result).toMatchObject(new SuccessResponse());
    });
  });

  describe('removeComment', () => {
    it('should return successful response', async () => {
      commentServiceMock.remove.mockResolvedValue();

      const result = await controller.removeComment(null, 'projectId', 'issueId', 'commentId');

      expect(commentServiceMock.remove).toHaveBeenCalledWith('projectId', 'issueId', 'commentId', null);
      expect(result).toMatchObject(new SuccessResponse());
    });
  });

  const addCommentRequest = new AddCommentRequest();
  const updateCommentRequest = new UpdateCommentRequest();

  describe.each([
    [
      'getAllComments',
      () => controller.getAllComments.bind(controller),
      [null, 'projectId', 'issueId'],
      () => commentServiceMock.getAll,
      ['projectId', 'issueId', null],
      [new ProjectNotFoundError('projectId'), new IssueNotFoundError('issueId')]
    ],
    [
      'getComment',
      () => controller.getComment.bind(controller),
      [null, 'projectId', 'issueId', 'commentId'],
      () => commentServiceMock.get,
      ['projectId', 'issueId', 'commentId', null],
      [new ProjectNotFoundError('projectId'), new IssueNotFoundError('issueId')]
    ],
    [
      'addComment',
      () => controller.addComment.bind(controller),
      [null, 'projectId', 'issueId', addCommentRequest],
      () => commentServiceMock.add,
      ['projectId', 'issueId', addCommentRequest, null, undefined],
      [new ProjectNotFoundError('projectId'), new IssueNotFoundError('issueId'), new UserNotFoundError('userId')]
    ],
    [
      'updateComment',
      () => controller.updateComment.bind(controller),
      [null, 'projectId', 'issueId', 'commentId', updateCommentRequest],
      () => commentServiceMock.update,
      ['projectId', 'issueId', 'commentId', updateCommentRequest, null],
      [new ProjectNotFoundError('projectId'), new IssueNotFoundError('issueId'), new CommentNotFoundError('commentId')]
    ],
    [
      'removeComment',
      () => controller.removeComment.bind(controller),
      [null, 'projectId', 'issueId', 'commentId'],
      () => commentServiceMock.remove,
      ['projectId', 'issueId', 'commentId', null],
      [new ProjectNotFoundError('projectId'), new IssueNotFoundError('issueId'), new CommentNotFoundError('commentId')]
    ]
  ] as [
    controllerName: string,
    getControllerMethod: () => (...args: any[]) => Promise<any>,
    controllerArgs: any[],
    getServiceMethod: () => jest.Mock & ((...args: any[]) => Promise<any>),
    serviceArgs: any[],
    serviceErrors: ServiceError[]
  ][])('%s with invalid parameters', (_functionName, getControllerMethod, controllerArgs, getServiceMethod, serviceArgs, serviceErrors) => {
    it.each(serviceErrors.map(error => [error.constructor.name, error]))('should throw %s', async (_errorName, error) => {
      const controllerMethod = getControllerMethod();
      const serviceMethod = getServiceMethod();
      serviceMethod.mockRejectedValue(error);

      const promise = controllerMethod(...controllerArgs);

      await expect(promise).rejects.toEqual(error);
      expect(serviceMethod).toHaveBeenCalledWith(...serviceArgs);
    });
  });
});
