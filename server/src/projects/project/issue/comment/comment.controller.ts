import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Patch,
  Post,
  UseFilters,
  UseGuards,
  UsePipes
} from '@nestjs/common';
import { ServiceErrorFilter } from '../../../../services/filters/service-error.filter';
import {
  CommentService,
  ICommentUpdates,
  INewComment
} from '../../../../services/project/issue/comment/comment.service';
import { Response } from '../../../../models/responses/Response';
import { SuccessResponse } from '../../../../models/responses/SuccessResponse';
import { CommentDto, mapCommentToCommentDto } from '../../../../dtos/CommentDto';
import {
  ApiBadRequestResponse,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiProperty,
  ApiPropertyOptional,
  ApiTags
} from '@nestjs/swagger';
import { openApiOneOf, openApiArrayOf } from '../../../../openapi/utils';
import { ProjectNotFoundError } from '../../../../services/common/errors/ProjectNotFoundError';
import { IssueNotFoundError } from '../../../../services/common/errors/IssueNotFoundError';
import { CommentNotFoundError } from '../../../../services/common/errors/CommentNotFoundError';
import { UserNotFoundError } from '../../../../services/common/errors/UserNotFoundError';
import { JwtGuard } from '../../../../auth/guards/jwt.guard';
import { IsOptional, IsString } from 'class-validator';
import { BodyValidationPipe } from '../../../../validation/body-validation.pipe';
import { CurrentUser } from '../../../../auth/decorators/current-user.decorator';
import { IAuthenticatedUser } from '../../../../services/auth/auth.service';
import { AuthenticatedGuard } from '../../../../auth/guards/authenticated.guard';

export class AddCommentRequest implements INewComment {
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  id?: string;

  @ApiProperty()
  @IsString()
  body!: string;

  @ApiProperty()
  @IsString()
  author!: string;
}

export class UpdateCommentRequest implements Partial<ICommentUpdates> {
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  body?: string;
}

@ApiTags('comments')
@Controller('project/:projectId/issue/:issueId/comment')
@UseFilters(new ServiceErrorFilter())
@UsePipes(new BodyValidationPipe())
@UseGuards(JwtGuard)
export class CommentController {

  constructor(private commentService: CommentService) {}

  @Get()
  @HttpCode(HttpStatus.OK)
  @UseFilters(new ServiceErrorFilter([{ code: 404, errors: [ProjectNotFoundError, IssueNotFoundError] }]))
  @ApiOperation({ summary: 'Gets all comments' })
  @ApiOkResponse({ schema: SuccessResponse.openApi(openApiArrayOf(CommentDto.openApi())) })
  @ApiNotFoundResponse({ schema: openApiOneOf(ProjectNotFoundError.openApiResponse(), IssueNotFoundError.openApiResponse()) })
  async getAllComments(@CurrentUser() currentUser: IAuthenticatedUser | null, @Param('projectId') projectId: string, @Param('issueId') issueId: string): Promise<Response> {
    const comments = await this.commentService.getAll(projectId, issueId, currentUser);

    return new SuccessResponse(comments.map(comment => mapCommentToCommentDto(comment)));
  }

  @Get(':commentId')
  @HttpCode(HttpStatus.OK)
  @UseFilters(new ServiceErrorFilter([{
    code: 404,
    errors: [ProjectNotFoundError, IssueNotFoundError, CommentNotFoundError]
  }]))
  @ApiOperation({ summary: 'Gets a single comment' })
  @ApiOkResponse({ schema: SuccessResponse.openApi(CommentDto.openApi()) })
  @ApiNotFoundResponse({ schema: openApiOneOf(ProjectNotFoundError.openApiResponse(), IssueNotFoundError.openApiResponse(), CommentNotFoundError.openApiResponse()) })
  async getComment(@CurrentUser() currentUser: IAuthenticatedUser | null, @Param('projectId') projectId: string, @Param('issueId') issueId: string, @Param('commentId') commentId: string): Promise<Response> {
    const comment = await this.commentService.get(projectId, issueId, commentId, currentUser);

    if (!comment) {
      throw new CommentNotFoundError(commentId);
    }

    return new SuccessResponse(mapCommentToCommentDto(comment));
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  @UseFilters(new ServiceErrorFilter([{ code: 404, errors: [ProjectNotFoundError, IssueNotFoundError] }]))
  @UseGuards(new AuthenticatedGuard())
  @ApiOperation({ summary: 'Adds a new comment' })
  @ApiCreatedResponse({ schema: SuccessResponse.openApi(CommentDto.openApi()) })
  @ApiNotFoundResponse({ schema: openApiOneOf(ProjectNotFoundError.openApiResponse(), IssueNotFoundError.openApiResponse()) })
  @ApiBadRequestResponse({ schema: UserNotFoundError.openApiResponse() })
  async addComment(@CurrentUser() currentUser: IAuthenticatedUser | null, @Param('projectId') projectId: string, @Param('issueId') issueId: string, @Body() body: AddCommentRequest): Promise<Response> {
    const comment = await this.commentService.add(projectId, issueId, body, currentUser, body.id);

    return new SuccessResponse(mapCommentToCommentDto(comment));
  }

  @Patch(':commentId')
  @HttpCode(HttpStatus.OK)
  @UseFilters(new ServiceErrorFilter([{
    code: 404,
    errors: [ProjectNotFoundError, IssueNotFoundError, CommentNotFoundError]
  }]))
  @UseGuards(new AuthenticatedGuard())
  @ApiOperation({ summary: 'Updates a comment' })
  @ApiOkResponse({ schema: SuccessResponse.openApi() })
  @ApiNotFoundResponse({ schema: openApiOneOf(ProjectNotFoundError.openApiResponse(), IssueNotFoundError.openApiResponse(), CommentNotFoundError.openApiResponse()) })
  async updateComment(@CurrentUser() currentUser: IAuthenticatedUser | null, @Param('projectId') projectId: string, @Param('issueId') issueId: string, @Param('commentId') commentId: string, @Body() body: UpdateCommentRequest): Promise<Response> {
    await this.commentService.update(projectId, issueId, commentId, body, currentUser);

    return new SuccessResponse();
  }

  @Delete(':commentId')
  @HttpCode(HttpStatus.OK)
  @UseFilters(new ServiceErrorFilter([{
    code: 404,
    errors: [ProjectNotFoundError, IssueNotFoundError, CommentNotFoundError]
  }]))
  @UseGuards(new AuthenticatedGuard())
  @ApiOperation({ summary: 'Removes a comment' })
  @ApiOkResponse({ schema: SuccessResponse.openApi() })
  @ApiNotFoundResponse({ schema: openApiOneOf(ProjectNotFoundError.openApiResponse(), IssueNotFoundError.openApiResponse(), CommentNotFoundError.openApiResponse()) })
  async removeComment(@CurrentUser() currentUser: IAuthenticatedUser | null, @Param('projectId') projectId: string, @Param('issueId') issueId: string, @Param('commentId') commentId: string): Promise<Response> {
    await this.commentService.remove(projectId, issueId, commentId, currentUser);

    return new SuccessResponse();
  }
}
