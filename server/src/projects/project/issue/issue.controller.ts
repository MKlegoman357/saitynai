import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Patch,
  Post,
  Query,
  UseFilters,
  UseGuards,
  UsePipes
} from '@nestjs/common';
import { ServiceErrorFilter } from '../../../services/filters/service-error.filter';
import { IIssuesFilter, IIssueUpdates, INewIssue, IssueService } from '../../../services/project/issue/issue.service';
import { Response } from '../../../models/responses/Response';
import { IssueStatus } from '../../../models/IssueStatus';
import { SuccessResponse } from '../../../models/responses/SuccessResponse';
import { IssueDto, mapIssueToIssueDto } from '../../../dtos/IssueDto';
import {
  ApiBadRequestResponse,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiProperty,
  ApiPropertyOptional,
  ApiTags
} from '@nestjs/swagger';
import { openApiOneOf, openApiArrayOf } from '../../../openapi/utils';
import { ProjectNotFoundError } from '../../../services/common/errors/ProjectNotFoundError';
import { IssueNotFoundError } from '../../../services/common/errors/IssueNotFoundError';
import { UserNotFoundError } from '../../../services/common/errors/UserNotFoundError';
import { LabelNotFoundError } from '../../../services/common/errors/LabelNotFoundError';
import { JwtGuard } from '../../../auth/guards/jwt.guard';
import { IsEnum, IsOptional, IsString } from 'class-validator';
import { BodyValidationPipe } from '../../../validation/body-validation.pipe';
import { CurrentUser } from '../../../auth/decorators/current-user.decorator';
import { IAuthenticatedUser } from '../../../services/auth/auth.service';
import { AuthenticatedGuard } from '../../../auth/guards/authenticated.guard';
import { Expose, Transform } from 'class-transformer';

class GetAllIssuesQuery implements IIssuesFilter {
  @ApiPropertyOptional({ type: [String] })
  @IsOptional()
  @IsString({ each: true })
  @Expose()
  @Transform(({ value }) => typeof value === 'string' ? (value as string).split(',').map(tag => tag.trim()) : value)
  labels?: string[];

  @ApiPropertyOptional({ enum: IssueStatus, enumName: 'IssueStatus' })
  @IsOptional()
  @IsEnum(IssueStatus)
  @Expose()
  status?: IssueStatus;

  @ApiPropertyOptional({ type: [String] })
  @IsOptional()
  @IsString({ each: true })
  @Expose()
  @Transform(({ value }) => typeof value === 'string' ? (value as string).split(',').map(tag => tag.trim()) : value)
  authors?: string[];

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  @Expose()
  title?: string;
}

class CreateIssueRequest implements INewIssue {
  @ApiProperty()
  @IsString()
  title!: string;

  @ApiProperty()
  @IsString()
  body!: string;

  @ApiProperty()
  @IsString()
  authorId!: string;

  @ApiProperty({ type: [String] })
  @IsString({ each: true })
  labels!: string[];

  @ApiProperty({ type: [String] })
  @IsString({ each: true })
  assignedUsers!: string[];
}

class UpdateIssueRequest implements Partial<IIssueUpdates> {
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  title?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  body?: string;

  @ApiPropertyOptional({ enum: IssueStatus, enumName: 'IssueStatus' })
  @IsOptional()
  @IsEnum(IssueStatus)
  status?: IssueStatus;

  @ApiPropertyOptional({ type: [String] })
  @IsOptional()
  @IsString({ each: true })
  labels?: string[];

  @ApiPropertyOptional({ type: [String] })
  @IsOptional()
  @IsString({ each: true })
  assignedUsers?: string[];
}

@ApiTags('issues')
@Controller('project/:projectId/issue')
@UseFilters(new ServiceErrorFilter())
@UsePipes(new BodyValidationPipe())
@UseGuards(JwtGuard)
export class IssueController {

  constructor(private issueService: IssueService) {}

  @Get()
  @HttpCode(HttpStatus.OK)
  @UseFilters(new ServiceErrorFilter([{ code: 404, errors: [ProjectNotFoundError] }]))
  @ApiOperation({ summary: 'Gets all issues' })
  @ApiOkResponse({ schema: SuccessResponse.openApi(openApiArrayOf(IssueDto.openApi())) })
  @ApiNotFoundResponse({ schema: ProjectNotFoundError.openApiResponse() })
  async getAllIssues(@CurrentUser() currentUser: IAuthenticatedUser | null, @Param('projectId') projectId: string, @Query() query: GetAllIssuesQuery): Promise<Response> {
    const issues = await this.issueService.getAll(projectId, currentUser, query);

    return new SuccessResponse(issues.map(issue => mapIssueToIssueDto(issue)));
  }

  @Get(':issueId')
  @HttpCode(HttpStatus.OK)
  @UseFilters(new ServiceErrorFilter([{ code: 404, errors: [ProjectNotFoundError, IssueNotFoundError] }]))
  @ApiOperation({ summary: 'Gets a single issue' })
  @ApiOkResponse({ schema: SuccessResponse.openApi(IssueDto.openApi()) })
  @ApiNotFoundResponse({ schema: openApiOneOf(ProjectNotFoundError.openApiResponse(), IssueNotFoundError.openApiResponse()) })
  async getIssue(@CurrentUser() currentUser: IAuthenticatedUser | null, @Param('projectId') projectId: string, @Param('issueId') issueId: string): Promise<Response> {
    const issue = await this.issueService.get(projectId, issueId, currentUser);

    if (!issue) {
      throw new IssueNotFoundError(issueId);
    }

    return new SuccessResponse(mapIssueToIssueDto(issue));
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  @UseFilters(new ServiceErrorFilter([{ code: 404, errors: [ProjectNotFoundError] }]))
  @UseGuards(new AuthenticatedGuard())
  @ApiOperation({ summary: 'Creates an issue' })
  @ApiCreatedResponse({ schema: SuccessResponse.openApi(IssueDto.openApi()) })
  @ApiNotFoundResponse({ schema: ProjectNotFoundError.openApiResponse() })
  @ApiBadRequestResponse({ schema: openApiOneOf(UserNotFoundError.openApiResponse(), LabelNotFoundError.openApiResponse()) })
  async createIssue(@CurrentUser() currentUser: IAuthenticatedUser | null, @Param('projectId') projectId: string, @Body() body: CreateIssueRequest): Promise<Response> {
    const issue = await this.issueService.create(projectId, body, currentUser);

    return new SuccessResponse(mapIssueToIssueDto(issue));
  }

  @Patch(':issueId')
  @HttpCode(HttpStatus.OK)
  @UseFilters(new ServiceErrorFilter([{ code: 404, errors: [ProjectNotFoundError, IssueNotFoundError] }]))
  @UseGuards(new AuthenticatedGuard())
  @ApiOperation({ summary: 'Updates an issue' })
  @ApiOkResponse({ schema: SuccessResponse.openApi() })
  @ApiNotFoundResponse({ schema: openApiOneOf(ProjectNotFoundError.openApiResponse(), IssueNotFoundError.openApiResponse()) })
  @ApiBadRequestResponse({ schema: openApiOneOf(UserNotFoundError.openApiResponse(), LabelNotFoundError.openApiResponse()) })
  async updateIssue(@CurrentUser() currentUser: IAuthenticatedUser | null, @Param('projectId') projectId: string, @Param('issueId') issueId: string, @Body() body: UpdateIssueRequest): Promise<Response> {
    await this.issueService.update(projectId, issueId, body, currentUser);

    return new SuccessResponse();
  }

  @Delete(':issueId')
  @HttpCode(HttpStatus.OK)
  @UseFilters(new ServiceErrorFilter([{ code: 404, errors: [ProjectNotFoundError, IssueNotFoundError] }]))
  @UseGuards(new AuthenticatedGuard())
  @ApiOperation({ summary: 'Deletes an issue' })
  @ApiOkResponse({ schema: SuccessResponse.openApi() })
  @ApiNotFoundResponse({ schema: openApiOneOf(ProjectNotFoundError.openApiResponse(), IssueNotFoundError.openApiResponse()) })
  async deleteIssue(@CurrentUser() currentUser: IAuthenticatedUser | null, @Param('projectId') projectId: string, @Param('issueId') issueId: string): Promise<Response> {
    await this.issueService.delete(projectId, issueId, currentUser);

    return new SuccessResponse();
  }
}
