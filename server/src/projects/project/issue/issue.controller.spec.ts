import { Test, TestingModule } from '@nestjs/testing';
import { IssueController } from './issue.controller';
import { IssueService } from '../../../services/project/issue/issue.service';

describe('IssueController', () => {
  let controller: IssueController;

  const issueServiceMock = {};

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        IssueController,
        IssueService
      ]
    }).overrideProvider(IssueService).useValue(issueServiceMock)
      .compile();

    controller = module.get(IssueController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
