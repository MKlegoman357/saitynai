import { Controller, Get, HttpCode, HttpStatus, Param, UseFilters, UseGuards, UsePipes } from '@nestjs/common';
import { ApiNotFoundResponse, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { ServiceErrorFilter } from '../../../../services/filters/service-error.filter';
import { BodyValidationPipe } from '../../../../validation/body-validation.pipe';
import { JwtGuard } from '../../../../auth/guards/jwt.guard';
import { IssueEventService } from '../../../../services/project/issue/issue-event/issue-event.service';
import { ProjectNotFoundError } from '../../../../services/common/errors/ProjectNotFoundError';
import { IssueNotFoundError } from '../../../../services/common/errors/IssueNotFoundError';
import { SuccessResponse } from '../../../../models/responses/SuccessResponse';
import { openApiArrayOf, openApiOneOf } from '../../../../openapi/utils';
import { CurrentUser } from '../../../../auth/decorators/current-user.decorator';
import { IAuthenticatedUser } from '../../../../services/auth/auth.service';
import { Response } from '../../../../models/responses/Response';
import { IssueEventDto, mapIssueEventToIssueEventDto } from '../../../../dtos/IssueEventDto';

@ApiTags('issue-events')
@Controller('project/:projectId/issue/:issueId/event')
@UseFilters(new ServiceErrorFilter())
@UsePipes(new BodyValidationPipe())
@UseGuards(JwtGuard)
export class IssueEventController {

  constructor(private issueEventService: IssueEventService) {}

  @Get()
  @HttpCode(HttpStatus.OK)
  @UseFilters(new ServiceErrorFilter([{ code: 404, errors: [ProjectNotFoundError, IssueNotFoundError] }]))
  @ApiOperation({ summary: 'Gets all issue events' })
  @ApiOkResponse({ schema: SuccessResponse.openApi(openApiArrayOf(IssueEventDto.openApi())) })
  @ApiNotFoundResponse({ schema: openApiOneOf(ProjectNotFoundError.openApiResponse(), IssueNotFoundError.openApiResponse()) })
  async getAllEvents(@CurrentUser() currentUser: IAuthenticatedUser | null, @Param('projectId') projectId: string, @Param('issueId') issueId: string): Promise<Response> {
    const events = await this.issueEventService.getAll(projectId, issueId, currentUser);

    return new SuccessResponse(events.map(event => mapIssueEventToIssueEventDto(event)));
  }

}
