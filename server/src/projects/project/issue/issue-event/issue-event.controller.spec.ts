import { Test, TestingModule } from '@nestjs/testing';
import { IssueEventController } from './issue-event.controller';
import { IssueEventService } from '../../../../services/project/issue/issue-event/issue-event.service';

describe('IssueEventController', () => {
  let controller: IssueEventController;

  const issueEventServiceMock = {};

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [IssueEventController],
      providers: [IssueEventService],
    }).overrideProvider(IssueEventService).useValue(issueEventServiceMock)
      .compile();

    controller = module.get<IssueEventController>(IssueEventController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
