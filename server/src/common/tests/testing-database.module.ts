import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { DatabaseModule } from '../../database/database.module';
import { DatabaseConfigUriKey } from '../../config/config';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: [() => ({
        [DatabaseConfigUriKey]: process.env.MONGO_URL
      })]
    }),
    DatabaseModule
  ],
  exports: [
    ConfigModule,
    DatabaseModule
  ]
})
export class TestingDatabaseModule {}
