export const DatabaseMigrationsKeyPrefix = 'MIGRATIONS_';
export const DatabaseMigrationsUpKey = DatabaseMigrationsKeyPrefix + 'UP';
export const DatabaseMigrationsDownKey = DatabaseMigrationsKeyPrefix + 'DOWN';

export const DatabaseConfigKeyPrefix = 'DATABASE_';
export const DatabaseConfigUriKey = DatabaseConfigKeyPrefix + 'URI';
export const DatabaseConfigUserKey = DatabaseConfigKeyPrefix + 'USER';
export const DatabaseConfigPassKey = DatabaseConfigKeyPrefix + 'PASS';

export const JwtSecretConfigKey = 'JWT_SECRET';
export const JwtExpiresConfigKey = 'JWT_EXPIRES';

export const BcryptRoundsConfigKey = 'BCRYPT_ROUNDS';
