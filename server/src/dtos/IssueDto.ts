import { IssueStatus, openApiIssueStatus } from '../models/IssueStatus';
import { mapUserInfoToUserInfoDto, UserInfoDto } from './UserInfoDto';
import { LabelDto, mapLabelToLabelDto } from './LabelDto';
import { Issue } from '../models/Issue';
import { ApiProperty } from '@nestjs/swagger';
import { SchemaObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { openApiArrayOf, openApiDate, openApiObject, openApiString } from '../openapi/utils';

export class IssueDto {
  @ApiProperty() id: string;
  @ApiProperty() title: string;
  @ApiProperty() body: string;
  @ApiProperty({ enum: IssueStatus, enumName: 'IssueStatus' }) status: IssueStatus;
  @ApiProperty() dateCreated: Date;
  @ApiProperty() dateModified: Date;

  @ApiProperty() author: UserInfoDto;
  @ApiProperty({ type: [UserInfoDto] }) assignedUsers: UserInfoDto[];
  @ApiProperty({ type: [LabelDto] }) labels: LabelDto[];

  constructor(id: string, title: string, body: string, status: IssueStatus, dateCreated: Date, dateModified: Date, author: UserInfoDto, assignedUsers: UserInfoDto[], labels: LabelDto[]) {
    this.id = id;
    this.title = title;
    this.body = body;
    this.status = status;
    this.dateCreated = dateCreated;
    this.dateModified = dateModified;
    this.author = author;
    this.assignedUsers = assignedUsers;
    this.labels = labels;
  }

  static openApi(): SchemaObject {
    return openApiObject({
      id: openApiString(),
      title: openApiString(),
      body: openApiString(),
      status: openApiIssueStatus(),
      dateCreated: openApiDate(),
      dateModified: openApiDate(),

      author: UserInfoDto.openApi(),
      assignedUsers: openApiArrayOf(UserInfoDto.openApi()),
      labels: openApiArrayOf(LabelDto.openApi())
    }, 'Issue');
  }
}

export function mapIssueToIssueDto(issue: Issue): IssueDto {
  return new IssueDto(
    issue.id,
    issue.title,
    issue.body,
    issue.status,
    issue.dateCreated,
    issue.dateModified,

    mapUserInfoToUserInfoDto(issue.author),
    issue.assignedUsers.map(user => mapUserInfoToUserInfoDto(user)),
    issue.labels.map(label => mapLabelToLabelDto(label))
  );
}
