import { IssueEventType, openApiIssueEventType } from '../models/IssueEventType';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { SchemaObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { openApiArrayOf, openApiDate, openApiObject, openApiOneOf } from '../openapi/utils';
import { IssueStatus, openApiIssueStatus } from '../models/IssueStatus';
import {
  AssignedUsersIssueEvent,
  BodyIssueEvent,
  IssueEvent,
  LabelsIssueEvent,
  StatusIssueEvent,
  TitleIssueEvent
} from '../models/IssueEvent';
import { mapUserInfoToUserInfoDto, UserInfoDto } from './UserInfoDto';
import { LabelDto, mapLabelToLabelDto } from './LabelDto';
import { UserInfo } from '../models/UserInfo';

export abstract class IssueEventDto<TEvent extends IssueEventType = any> {
  @ApiProperty() type: TEvent;
  @ApiProperty() timestamp: Date;
  @ApiPropertyOptional() author?: UserInfoDto;

  protected constructor(type: TEvent, timestamp: Date, author?: UserInfoDto) {
    this.type = type;
    this.timestamp = timestamp;
    this.author = author;
  }

  static openApi(): SchemaObject {
    return openApiOneOf(
      TitleIssueEventDto.openApi(),
      BodyIssueEventDto.openApi(),
      StatusIssueEventDto.openApi(),
      AssignedUsersIssueEventDto.openApi(),
      LabelsIssueEventDto.openApi()
    );
  }

  protected static openApiBase(properties: Record<string, SchemaObject>, name: string = 'IssueEvent'): SchemaObject {
    return openApiObject({
      type: openApiIssueEventType(),
      timestamp: openApiDate(),
      author: UserInfoDto.openApi(true),
      ...properties
    }, name);
  }
}

export function mapIssueEventToIssueEventDto<TEvent extends IssueEventType>(event: IssueEvent<TEvent>): IssueEventDto<TEvent> {
  if (event.type === IssueEventType.Title) {
    return mapTitleIssueEventToTitleIssueEventDto(event as any) as any;
  } else if (event.type === IssueEventType.Body) {
    return mapBodyIssueEventToBodyIssueEventDto(event as any) as any;
  } else if (event.type === IssueEventType.Status) {
    return mapStatusIssueEventToStatusIssueEventDto(event as any) as any;
  } else if (event.type === IssueEventType.AssignedUsers) {
    return mapAssignedUsersIssueEventToAssignedUsersIssueEventDto(event as any) as any;
  } else if (event.type === IssueEventType.Labels) {
    return mapLabelsIssueEventToLabelsIssueEventDto(event as any) as any;
  }

  throw new Error(`Unsupported event type: ${event.type}.`);
}

export class TitleIssueEventDto extends IssueEventDto<IssueEventType.Title> {
  constructor(timestamp: Date, author?: UserInfo) {
    super(IssueEventType.Title, timestamp, author);
  }

  static openApi(): SchemaObject {
    return IssueEventDto.openApiBase({}, 'TitleIssueEvent');
  }
}

export function mapTitleIssueEventToTitleIssueEventDto(event: TitleIssueEvent): TitleIssueEventDto {
  return new TitleIssueEventDto(event.timestamp, event.author);
}

export class BodyIssueEventDto extends IssueEventDto<IssueEventType.Body> {
  constructor(timestamp: Date, author?: UserInfo) {
    super(IssueEventType.Body, timestamp, author);
  }

  static openApi(): SchemaObject {
    return IssueEventDto.openApiBase({}, 'BodyIssueEvent');
  }
}

export function mapBodyIssueEventToBodyIssueEventDto(event: BodyIssueEvent): BodyIssueEventDto {
  return new BodyIssueEventDto(event.timestamp, event.author);
}

export class StatusIssueEventDto extends IssueEventDto<IssueEventType.Status> {
  @ApiProperty() status: IssueStatus;

  constructor(timestamp: Date, status: IssueStatus, author?: UserInfo) {
    super(IssueEventType.Status, timestamp, author);

    this.status = status;
  }

  static openApi(): SchemaObject {
    return IssueEventDto.openApiBase({
      status: openApiIssueStatus()
    }, 'StatusIssueEvent');
  }
}

export function mapStatusIssueEventToStatusIssueEventDto(event: StatusIssueEvent): StatusIssueEventDto {
  return new StatusIssueEventDto(event.timestamp, event.status, event.author);
}

export class AssignedUsersIssueEventDto extends IssueEventDto<IssueEventType.AssignedUsers> {
  @ApiProperty({ type: [UserInfoDto] }) assignedUsers: UserInfoDto[];

  constructor(timestamp: Date, assignedUsers: UserInfoDto[], author?: UserInfo) {
    super(IssueEventType.AssignedUsers, timestamp, author);

    this.assignedUsers = assignedUsers;
  }

  static openApi(): SchemaObject {
    return IssueEventDto.openApiBase({
      assignedUsers: openApiArrayOf(UserInfoDto.openApi())
    }, 'AssignedUsersIssueEvent');
  }
}

export function mapAssignedUsersIssueEventToAssignedUsersIssueEventDto(event: AssignedUsersIssueEvent): AssignedUsersIssueEventDto {
  return new AssignedUsersIssueEventDto(event.timestamp, event.assignedUsers.map(user => mapUserInfoToUserInfoDto(user)), event.author);
}

export class LabelsIssueEventDto extends IssueEventDto<IssueEventType.Labels> {
  @ApiProperty({ type: [LabelDto] }) labels: LabelDto[];

  constructor(timestamp: Date, labels: LabelDto[], author?: UserInfo) {
    super(IssueEventType.Labels, timestamp, author);

    this.labels = labels;
  }

  static openApi(): SchemaObject {
    return IssueEventDto.openApiBase({
      labels: openApiArrayOf(LabelDto.openApi())
    }, 'LabelsIssueEvent');
  }
}

export function mapLabelsIssueEventToLabelsIssueEventDto(event: LabelsIssueEvent): LabelsIssueEventDto {
  return new LabelsIssueEventDto(event.timestamp, event.labels.map(label => mapLabelToLabelDto(label)), event.author);
}
