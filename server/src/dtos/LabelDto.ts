import { Label } from '../models/Label';
import { ApiProperty } from '@nestjs/swagger';
import { SchemaObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { openApiObject, openApiString } from '../openapi/utils';
import { LabelInfo } from '../models/LabelInfo';

export class LabelDto {
  @ApiProperty() name: string;

  constructor(name: string) {
    this.name = name;
  }

  static openApi(): SchemaObject {
    return openApiObject({
      name: openApiString()
    }, 'Label');
  }
}

export function mapLabelToLabelDto(label: Label | LabelInfo): LabelDto {
  return new LabelDto(label.name);
}
