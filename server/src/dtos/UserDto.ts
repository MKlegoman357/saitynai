import { User } from '../models/User';
import { ApiProperty } from '@nestjs/swagger';
import { SchemaObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { openApiObject, openApiString } from '../openapi/utils';
import { openApiRole, Role } from '../models/Role';

export class UserDto {
  @ApiProperty() id: string;
  @ApiProperty() email: string;
  @ApiProperty() username: string;
  @ApiProperty({ enum: Role, enumName: 'Role' }) role: Role;

  constructor(id: string, email: string, username: string, role: Role) {
    this.id = id;
    this.email = email;
    this.username = username;
    this.role = role;
  }

  static openApi(): SchemaObject {
    return openApiObject({
      id: openApiString(),
      email: openApiString(),
      username: openApiString(),
      role: openApiRole()
    }, 'User');
  }
}

export function mapUserToUserDto(user: User): UserDto {
  return new UserDto(user.id, user.email, user.username, user.role);
}
