import { Comment } from '../models/Comment';
import { mapUserInfoToUserInfoDto, UserInfoDto } from './UserInfoDto';
import { ApiProperty } from '@nestjs/swagger';
import { SchemaObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { openApiDate, openApiObject, openApiString } from '../openapi/utils';

export class CommentDto {
  @ApiProperty() id: string;
  @ApiProperty() body: string;
  @ApiProperty() author: UserInfoDto;
  @ApiProperty() dateCreated: Date;
  @ApiProperty() dateModified: Date;

  constructor(id: string, body: string, author: UserInfoDto, dateCreated: Date, dateModified: Date) {
    this.id = id;
    this.body = body;
    this.author = author;
    this.dateCreated = dateCreated;
    this.dateModified = dateModified;
  }

  static openApi(): SchemaObject {
    return openApiObject({
      id: openApiString(),
      body: openApiString(),
      author: UserInfoDto.openApi(),
      dateCreated: openApiDate(),
      dateModified: openApiDate()
    }, 'Comment');
  }
}

export function mapCommentToCommentDto(comment: Comment): CommentDto {
  return new CommentDto(comment.id, comment.body, mapUserInfoToUserInfoDto(comment.author), comment.dateCreated, comment.dateModified);
}
