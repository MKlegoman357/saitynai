import { User } from '../models/User';
import { ApiProperty } from '@nestjs/swagger';
import { SchemaObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { openApiObject, openApiString } from '../openapi/utils';
import { UserInfo } from '../models/UserInfo';

export class UserInfoDto {
  @ApiProperty() id: string;
  @ApiProperty() username: string;

  constructor(id: string, username: string) {
    this.id = id;
    this.username = username;
  }

  static openApi(nullable: boolean = false): SchemaObject {
    return openApiObject({
      id: openApiString(),
      username: openApiString()
    }, 'UserInfo', nullable);
  }
}

export function mapUserInfoToUserInfoDto(user: User | UserInfo): UserInfoDto {
  return new UserInfoDto(user.id, user.username);
}
