import { mapUserInfoToUserInfoDto, UserInfoDto } from './UserInfoDto';
import { IProjectMember } from '../services/project/member/member.service';
import { ApiProperty } from '@nestjs/swagger';
import { SchemaObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { openApiBoolean, openApiObject } from '../openapi/utils';

export class ProjectMemberDto {
  @ApiProperty() isAdministrator: boolean;
  @ApiProperty() user: UserInfoDto;

  constructor(isAdministrator: boolean, user: UserInfoDto) {
    this.isAdministrator = isAdministrator;
    this.user = user;
  }

  static openApi(): SchemaObject {
    return openApiObject({
      isAdministrator: openApiBoolean(),
      user: UserInfoDto.openApi()
    }, 'ProjectMember');
  }
}

export function mapProjectMemberToProjectMemberDto(projectMember: IProjectMember): ProjectMemberDto {
  return new ProjectMemberDto(projectMember.isAdministrator, mapUserInfoToUserInfoDto(projectMember.user));
}
