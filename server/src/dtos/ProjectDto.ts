import { openApiProjectVisibility, ProjectVisibility } from '../models/ProjectVisibility';
import { Project } from '../models/Project';
import { ApiProperty } from '@nestjs/swagger';
import { SchemaObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { openApiDate, openApiObject, openApiString } from '../openapi/utils';

export class ProjectDto {
  @ApiProperty() id: string;
  @ApiProperty() name: string;
  @ApiProperty({ enum: ProjectVisibility, enumName: 'ProjectVisibility' }) visibility: ProjectVisibility;
  @ApiProperty() dateCreated: Date;

  constructor(id: string, name: string, visibility: ProjectVisibility, dateCreated: Date) {
    this.id = id;
    this.name = name;
    this.visibility = visibility;
    this.dateCreated = dateCreated;
  }

  static openApi(): SchemaObject {
    return openApiObject({
      id: openApiString(),
      name: openApiString(),
      visibility: openApiProjectVisibility(),
      dateCreated: openApiDate()
    }, 'Project')
  }
}

export function mapProjectToProjectDto(project: Project): ProjectDto {
  return new ProjectDto(
    project.id,
    project.name,
    project.visibility,
    project.dateCreated
  );
}
