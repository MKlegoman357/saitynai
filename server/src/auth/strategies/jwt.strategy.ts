import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtSecretConfigKey } from '../../config/config';
import { Role } from '../../models/Role';
import { IAuthenticatedUser } from '../../services/auth/auth.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private configService: ConfigService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: configService.get<string>(JwtSecretConfigKey)
    });
  }

  async validate(payload: { sub: string, email: string, username: string, role: Role }): Promise<IAuthenticatedUser> {
    return {
      id: payload.sub,
      email: payload.email,
      username: payload.username,
      role: payload.role
    };
  }
}
