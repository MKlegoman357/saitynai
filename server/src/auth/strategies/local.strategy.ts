import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { BadRequestException, Injectable } from '@nestjs/common';
import { AuthService, IAuthenticatedUser } from '../../services/auth/auth.service';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private authService: AuthService) {
    super();
  }

  async validate(username: string, password: string): Promise<IAuthenticatedUser> {
    const user = await this.authService.validateUser(username, password);

    if (!user) {
      throw new BadRequestException();
    }

    return user;
  }
}
