import { Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { IAuthenticatedUser } from '../../services/auth/auth.service';

@Injectable()
export class JwtGuard extends AuthGuard('jwt') {

  handleRequest<TUser = IAuthenticatedUser | null>(err: any, user: any): TUser {
    return user || null;
  }

}
