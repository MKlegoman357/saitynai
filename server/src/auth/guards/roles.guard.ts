import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Request as ExpressRequest } from 'express';
import { IAuthenticatedUser } from '../../services/auth/auth.service';
import { Role } from '../../models/Role';

@Injectable()
export class RolesGuard implements CanActivate {
  private readonly roles: Role[];

  constructor(...roles: Role[]) {
    this.roles = roles;
  }

  canActivate(context: ExecutionContext): boolean {
    const request = context.switchToHttp().getRequest<ExpressRequest>();
    const user = request.user as IAuthenticatedUser | undefined;

    if (!user) {
      return false;
    }

    return this.roles.includes(user.role);
  }
}
