import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Request as ExpressRequest } from 'express';
import { IAuthenticatedUser } from '../../services/auth/auth.service';

@Injectable()
export class AuthenticatedGuard implements CanActivate {

  canActivate(context: ExecutionContext): boolean {
    const request = context.switchToHttp().getRequest<ExpressRequest>();
    const user = request.user as IAuthenticatedUser | undefined;

    return !!user;
  }

}
