import { Test, TestingModule } from '@nestjs/testing';
import { AuthController } from './auth.controller';
import { AuthService, IAuthenticatedUser, ILoginResponse } from '../../services/auth/auth.service';
import { Request as ExpressRequest } from 'express';
import { Role } from '../../models/Role';
import { createAuthServiceMock, AuthServiceMock } from '../../services/auth/auth.service.mock';
import { SuccessResponse } from '../../models/responses/SuccessResponse';

describe('AuthController', () => {
  let controller: AuthController;
  let authServiceMock: AuthServiceMock;

  beforeEach(async () => {
    authServiceMock = createAuthServiceMock();

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthController,
        AuthService
      ]
    }).overrideProvider(AuthService).useValue(authServiceMock)
      .compile();

    controller = module.get(AuthController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('login', () => {
    it('should return access token', async () => {
      const user = {
        id: 'userId',
        username: 'username',
        role: Role.User
      } as IAuthenticatedUser;
      const request: ExpressRequest = { user } as any;
      authServiceMock.login.mockResolvedValue({ accessToken: 'accessToken' });

      const result = await controller.login(request) as SuccessResponse<ILoginResponse>;

      expect(authServiceMock.login).toBeCalledWith(user);
      expect(result.ok).toBe(true);
      expect(result.data.accessToken).toBe('accessToken');
    });
  });
});
