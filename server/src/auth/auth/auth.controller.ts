import { Body, Controller, Post, Request, UseFilters, UseGuards } from '@nestjs/common';
import { Request as ExpressRequest } from 'express';
import { LocalGuard } from '../guards/local.guard';
import { AuthService, IAuthenticatedUser, IRegisterRequest } from '../../services/auth/auth.service';
import { ApiProperty } from '@nestjs/swagger';
import { ServiceErrorFilter } from '../../services/filters/service-error.filter';
import { ErrorResponse } from '../../models/responses/ErrorResponse';
import { Response } from '../../models/responses/Response';
import { SuccessResponse } from '../../models/responses/SuccessResponse';

class RegisterRequest implements IRegisterRequest {
  @ApiProperty() email!: string;
  @ApiProperty() username!: string;
  @ApiProperty() password!: string;
}

@UseFilters(new ServiceErrorFilter())
@Controller('auth')
export class AuthController {

  constructor(private authService: AuthService) {}

  @UseGuards(LocalGuard)
  @Post('login')
  async login(@Request() request: ExpressRequest): Promise<Response> {
    const response = await this.authService.login(request.user as IAuthenticatedUser);

    return new SuccessResponse(response);
  }

  @Post('register')
  async register(@Body() body: RegisterRequest): Promise<Response> {
    const response = await this.authService.register(body);

    if (!response.success) {
      return new ErrorResponse(response.errorCode!);
    }

    return new SuccessResponse();
  }

}
