import { Module } from '@nestjs/common';
import { ServicesModule } from '../services/services.module';
import { PassportModule } from '@nestjs/passport';
import { LocalStrategy } from './strategies/local.strategy';
import { AuthController } from './auth/auth.controller';
import { JwtStrategy } from './strategies/jwt.strategy';

@Module({
  imports: [
    ServicesModule,
    PassportModule
  ],
  providers: [LocalStrategy, JwtStrategy],
  controllers: [AuthController]
})
export class AuthModule {}
