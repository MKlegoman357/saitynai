import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import * as fs from 'fs';
import { User } from './models/User';
import { UserService } from './services/user/user.service';
import { Role } from './models/Role';
import { INestApplication } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { DatabaseMigrationsDownKey, DatabaseMigrationsUpKey } from './config/config';
import { doMigrations } from './migrations/migrations';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true });

  const documentOptions = new DocumentBuilder()
    .setTitle('Issue tracker')
    .setVersion('0.1.0')
    .build();
  const document = SwaggerModule.createDocument(app, documentOptions);
  SwaggerModule.setup('', app, document);

  await fs.promises.writeFile('openapi.json', JSON.stringify(document, undefined, '\t'));

  const config = app.get(ConfigService);

  const migrationsUp = config.get<string>(DatabaseMigrationsUpKey) === 'true';
  const migrationsDown = config.get<string>(DatabaseMigrationsDownKey) === 'true';

  if (migrationsUp || migrationsDown) {
    await doMigrations(app, migrationsUp);

    await app.close();

    return;
  }

  await app.listen(3000, async () => {
    console.log('app started on http://localhost:3000');

    await seedDatabase(app);
  });
}

bootstrap().catch(console.error);

async function seedDatabase(app: INestApplication): Promise<void> {
  const userService = app.get(UserService);

  if (!await userService.get('admin')) {
    await userService.addUser(new User('admin', 'admin@admin.local', 'admin', 'admin', Role.Admin));
    console.log('added users', await userService.getAll());
  }
}
