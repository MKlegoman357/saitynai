import { Test, TestingModule } from '@nestjs/testing';
import { UserInfoController } from './user-info.controller';
import { UserService } from '../../services/user/user.service';

describe('UserInfoController', () => {
  let controller: UserInfoController;

  const userServiceMock = {};

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UserInfoController,
        UserService
      ],
    }).overrideProvider(UserService).useValue(userServiceMock)
      .compile();

    controller = module.get<UserInfoController>(UserInfoController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
