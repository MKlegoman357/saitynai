import { Controller, Get, HttpCode, HttpStatus, Param, UseFilters } from '@nestjs/common';
import { UserService } from '../../services/user/user.service';
import { ApiNotFoundResponse, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { SuccessResponse } from '../../models/responses/SuccessResponse';
import { openApiArrayOf } from '../../openapi/utils';
import { mapUserInfoToUserInfoDto, UserInfoDto } from '../../dtos/UserInfoDto';
import { Response } from '../../models/responses/Response';
import { ServiceErrorFilter } from '../../services/filters/service-error.filter';
import { UserNotFoundError } from '../../services/common/errors/UserNotFoundError';

@ApiTags('user-info')
@Controller('user-info')
@UseFilters(new ServiceErrorFilter())
export class UserInfoController {

  constructor(private userService: UserService) {}

  @Get()
  @HttpCode(HttpStatus.OK)
  @ApiOperation({ summary: 'Gets all users' })
  @ApiOkResponse({ schema: SuccessResponse.openApi(openApiArrayOf(UserInfoDto.openApi())) })
  async getAllUsers(): Promise<Response> {
    const users = await this.userService.getAll();

    return new SuccessResponse(users.map(user => mapUserInfoToUserInfoDto(user)));
  }

  @Get(':userId')
  @HttpCode(HttpStatus.OK)
  @UseFilters(new ServiceErrorFilter([{ code: 404, errors: [UserNotFoundError] }]))
  @ApiOperation({ summary: 'Gets a single user' })
  @ApiOkResponse({ schema: SuccessResponse.openApi(UserInfoDto.openApi()) })
  @ApiNotFoundResponse({ schema: UserNotFoundError.openApiResponse() })
  async getUser(@Param('userId') userId: string): Promise<Response> {
    const user = await this.userService.get(userId);

    if (!user) {
      throw new UserNotFoundError(userId);
    }

    return new SuccessResponse(mapUserInfoToUserInfoDto(user));
  }
}
