import { Module } from '@nestjs/common';
import { UserController } from './user/user.controller';
import { ServicesModule } from '../services/services.module';
import { UserInfoController } from './user-info/user-info.controller';

@Module({
  imports: [ServicesModule],
  controllers: [UserController, UserInfoController]
})
export class UsersModule {}
