import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  UseFilters,
  UseGuards,
  UsePipes
} from '@nestjs/common';
import { INewUser, UserService } from '../../services/user/user.service';
import { Role } from '../../models/Role';
import { Response } from '../../models/responses/Response';
import { SuccessResponse } from '../../models/responses/SuccessResponse';
import { ServiceErrorFilter } from '../../services/filters/service-error.filter';
import {
  ApiBadRequestResponse,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiProperty,
  ApiPropertyOptional,
  ApiTags
} from '@nestjs/swagger';
import { openApiArrayOf } from '../../openapi/utils';
import { UserNotFoundError } from '../../services/common/errors/UserNotFoundError';
import { UsernameTakenError } from '../../services/common/errors/UsernameTakenError';
import { JwtGuard } from '../../auth/guards/jwt.guard';
import { RolesGuard } from '../../auth/guards/roles.guard';
import { mapUserToUserDto, UserDto } from '../../dtos/UserDto';
import { IsEmail, IsEnum, IsOptional, IsString } from 'class-validator';
import { BodyValidationPipe } from '../../validation/body-validation.pipe';

class CreateUserRequest implements INewUser {
  @ApiPropertyOptional()
  @IsOptional()
  @IsString()
  id?: string;

  @ApiProperty()
  @IsEmail()
  email!: string;

  @ApiProperty()
  @IsString()
  username!: string;

  @ApiProperty()
  @IsString()
  password!: string;

  @ApiProperty({ enum: Role, enumName: 'Role' })
  @IsEnum(Role)
  role!: Role;
}

@ApiTags('users')
@Controller('user')
@UseFilters(new ServiceErrorFilter())
@UsePipes(new BodyValidationPipe())
@UseGuards(JwtGuard, new RolesGuard(Role.Admin))
export class UserController {

  constructor(private userService: UserService) {}

  @Get()
  @HttpCode(HttpStatus.OK)
  @ApiOperation({ summary: 'Gets all users' })
  @ApiOkResponse({ schema: SuccessResponse.openApi(openApiArrayOf(UserDto.openApi())) })
  async getAllUsers(): Promise<Response> {
    const users = await this.userService.getAll();

    return new SuccessResponse(users.map(user => mapUserToUserDto(user)));
  }

  @Get(':userId')
  @HttpCode(HttpStatus.OK)
  @UseFilters(new ServiceErrorFilter([{ code: 404, errors: [UserNotFoundError] }]))
  @ApiOperation({ summary: 'Gets a single user' })
  @ApiOkResponse({ schema: SuccessResponse.openApi(UserDto.openApi()) })
  @ApiNotFoundResponse({ schema: UserNotFoundError.openApiResponse() })
  async getUser(@Param('userId') userId: string): Promise<Response> {
    const user = await this.userService.get(userId);

    if (!user) {
      throw new UserNotFoundError(userId);
    }

    return new SuccessResponse(mapUserToUserDto(user));
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  @ApiOperation({ summary: 'Creates a user' })
  @ApiCreatedResponse({ schema: SuccessResponse.openApi(UserDto.openApi()) })
  @ApiBadRequestResponse({ schema: UsernameTakenError.openApiResponse() })
  async createUser(@Body() body: CreateUserRequest): Promise<Response> {
    const user = await this.userService.create(body, body.id);

    return new SuccessResponse(mapUserToUserDto(user));
  }

  @Delete(':userId')
  @HttpCode(HttpStatus.OK)
  @UseFilters(new ServiceErrorFilter([{ code: 404, errors: [UserNotFoundError] }]))
  @ApiOperation({ summary: 'Deletes a user' })
  @ApiOkResponse({ schema: SuccessResponse.openApi() })
  @ApiNotFoundResponse({ schema: UserNotFoundError.openApiResponse() })
  async deleteUser(@Param('userId') userId: string): Promise<Response> {
    await this.userService.delete(userId);

    return new SuccessResponse();
  }
}
