import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { User, UserSchema } from '../models/User';
import { Project, ProjectSchema } from '../models/Project';
import { ConfigService } from '@nestjs/config';
import { DatabaseConfigPassKey, DatabaseConfigUriKey, DatabaseConfigUserKey } from '../config/config';

@Module({
  imports: [
    MongooseModule.forRootAsync({
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => {
        const uri = configService.get<string>(DatabaseConfigUriKey)!;
        const user = configService.get<string>(DatabaseConfigUserKey)!;
        const pass = configService.get<string>(DatabaseConfigPassKey)!;

        return { uri, user, pass };
      }
    }),
    MongooseModule.forFeature([
      { name: User.name, schema: UserSchema },
      { name: Project.name, schema: ProjectSchema }
    ])
  ],
  exports: [MongooseModule]
})
export class DatabaseModule {}
