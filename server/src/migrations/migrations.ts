import { INestApplication } from '@nestjs/common';
import { getModelToken } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Project, ProjectDocument } from '../models/Project';

export async function doMigrations(app: INestApplication, up: boolean): Promise<void> {
  if (up) {
    await doMigrationsUp(app);
  } else {
    await doMigrationsDown(app);
  }
}

async function doMigrationsUp(app: INestApplication): Promise<void> {
  const projectModel = app.get<Model<ProjectDocument>>(getModelToken(Project.name));

  const projects = await projectModel.find().exec();

  for (const project of projects) {
    for (const issue of project.issues) {
      for (const comment of issue.comments) {
        /*const dateCreated = new Date();

        comment.dateCreated = dateCreated;
        comment.dateModified = dateCreated;*/
      }

      /*issue.dateModified = issue.dateCreated;

      if (issue.events.length > 0) {
        issue.dateModified = issue.events[issue.events.length - 1].timestamp;
      }*/
    }

    await project.save({ validateBeforeSave: false });
  }
}

async function doMigrationsDown(app: INestApplication): Promise<void> {
  const projectModel = app.get<Model<ProjectDocument>>(getModelToken(Project.name));

  const projects = await projectModel.find().exec();

  for (const project of projects) {
    for (const issue of project.issues) {
      for (const comment of issue.comments) {
        /*comment.dateCreated = undefined as any;
        comment.dateModified = undefined as any;*/
      }
    }

    await project.save({ validateBeforeSave: false });
  }
}
