import { SchemaObject } from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { getEnumValues } from '../utils/enum';

export function openApiOneOf(...objects: SchemaObject[]): SchemaObject {
  return { oneOf: objects };
}

export function openApiString(defaultValue?: string, nullable?: boolean): SchemaObject {
  let enumValues: string[] | undefined = undefined;

  if (typeof defaultValue !== 'undefined') {
    enumValues = [defaultValue];
  }

  return {
    type: 'string',
    default: defaultValue,
    enum: enumValues,
    nullable
  };
}

export function openApiNumber(defaultValue?: number, nullable?: boolean): SchemaObject {
  return {
    type: 'number',
    default: defaultValue,
    nullable
  };
}

export function openApiBoolean(defaultValue?: boolean, nullable?: boolean): SchemaObject {
  return {
    type: 'boolean',
    default: defaultValue,
    nullable
  };
}

export function openApiDate(defaultValue?: string, nullable?: boolean): SchemaObject {
  return openApiString(defaultValue, nullable);
}

export function openApiArrayOf(item: SchemaObject, nullable?: boolean): SchemaObject {
  return {
    type: 'array',
    items: item,
    nullable
  };
}

export function openApiObject(properties: Record<string, SchemaObject>, name?: string, nullable?: boolean): SchemaObject {
  return {
    type: 'object',
    title: name,
    properties: properties,
    nullable
  };
}

export function openApiEnum(name: string, enumeration: Record<string, string>, nullable?: boolean): SchemaObject {
  return {
    type: 'string',
    title: name,
    enum: getEnumValues(enumeration),
    nullable
  };
}
